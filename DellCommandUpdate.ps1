<#
Tested working on Windows 11 DCU 5.3.0 Windows Universal Application
DCU https://www.dell.com/support/kbdoc/en-us/000177325/dell-command-update
Create DCU msi https://www.dell.com/support/kbdoc/en-us/000177292/how-to-create-a-dell-command-update-msi-installer-package
dcu-cli reference https://www.dell.com/support/manuals/en-us/command-update/dellcommandupdate_rg/dell-command-update-cli-commands?guid=guid-92619086-5f7c-4a05-bce2-0d560c15e8ed&lang=en-us

Downloading exe from Dell unreliable:
 invoke-webrequest : Access Denied
You don't have permission to access 
"http://dl.dell.com/FOLDER11563484M/1/Dell-Command-Update-Windows-Universal-Application_P83K5_WIN_5.3.0_A00.EXE" on this server.
Reference #18.36263e17.1718242385.3f23ac1 
https://errors.edgesuite.net/18.36263e17.1718242385.3f23ac1
At line:1 char:1
+ invoke-webrequest -uri $uri -OutFile $exepath
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : InvalidOperation: (System.Net.HttpWebRequest:HttpWebRequest) [Invoke-WebRequest], WebException
    + FullyQualifiedErrorId : WebCmdletWebResponseException,Microsoft.PowerShell.Commands.InvokeWebRequestCommand
#>

$dellchk=get-ciminstance -classname Win32_ComputerSystem | select-object Manufacturer
if($false -eq $dellchk.manufacturer.startswith('Dell')){
    # write-host 'Not Dell endpoint'
    exit 0
}

$appchk=@()
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*'
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*'
$dcuchk=$appchk | where-object {$_.DisplayName -like '*Dell Command*'}
$dcumsi=-join('MsiExec.exe /i ',"$env:HOMEDRIVE",'\DellCommandUpdateApp.msi /quiet /norestart')

if($dcuchk){
    # write-host 'DCU installed check version'
    $dcuvers=[Version]($dcuchk.DisplayVersion)
    if($dcuvers -lt '5.3.0'){
        # write-host 'Check DCU not running'
        $gpdcu=get-process | where-object {$_.ProcessName -eq 'DellCommandUpdate'}
        if($null -eq $gpdcu){
            # write-host 'DCU not running, Uninstall DCU'
            $us=$dcuchk.uninstallstring.replace('MsiExec.exe /X{','MsiExec.exe /qn /norestart /x {')
            cmd.exe /c $us
            # write-host 'Install DCU'
            cmd.exe /c $dcumsi
        }else{
            # write-host 'DCU running exit'
            exit 0
        }
    }
}else{
    # write-host 'DCU not installed, install it'
    cmd.exe /c $dcumsi
}

$appchk=@()
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*'
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*'
$dcuchk=$appchk | where-object {$_.DisplayName -like '*Dell Command*'}
if($null -eq $dcuchk){
    # write-host 'Installing msi above failed with "Another program is being installed. Please wait until that installation is complete, and then try installing this software again."'
    exit 0
}

$dcupath="$env:ProgramFiles\Dell\CommandUpdate\dcu-cli.exe"
$dcuconfig1=-join('"',$dcupath,'" /configure -scheduleAction=DownloadInstallAndNotify -silent -updatesNotification=enable -userConsent=disable')
$dcuconfig2=-join('"',$dcupath,'" /configure -deferralInstallCount=9 -deferralInstallInterval=24 -deferralRestartCount=9 -deferralRestartInterval=24 -installationDeferral=enable -silent')
# -scheduleAction=DownloadAndNotify did notify via Windows Powershell session. However, subsequently running below /ApplyUpdates redownloaded all updates.
$dculock=-join('"',$dcupath,'" /configure -locksettings=enable -silent')
$dcuscan=-join('"',$dcupath,'" /scan -silent')
$unlockdcu=-join('"',$dcupath,'" /configure -locksettings=disable -silent')

$gpdcu=get-process | where-object {$_.ProcessName -eq 'DellCommandUpdate'}
if($null -eq $gpdcu){
    # write-host 'DCU not running, lock UI, configure, scan for updates, unlock UI'
    cmd.exe /c $dculock
    cmd.exe /c $dcuconfig1
    cmd.exe /c $dcuconfig2
    cmd.exe /c $dcuscan
    cmd.exe /c $unlockdcu
}else{
    # write-host 'DCU running, try again later'
    exit 0
}

$gpdcu=get-process | where-object {$_.ProcessName -eq 'DellCommandUpdate'}
if($null -eq $gpdcu){
    # write-host 'DCU not running'
    $batchk=(get-ciminstance -classname Win32_Battery).BatteryStatus
    if($null -eq $batchk -or $batchk -eq 2){
        # write-host 'Either a desktop, or laptop connected to power. Check for RDP / console session'
        $qwinstaOutput=qwinsta
        # Convert array object to array string
        $lines=$qwinstaOutput -split "`n"

        # Skip the first line headers
        $lines=$lines[1..($lines.Length - 1)]

        # Create array to hold session objects
        $sessions=@()
        foreach($line in $lines){
            # Create string array by removing extra spaces and split by space
            $columns=($line -replace '\s+', ',') -split ','
            # Create a custom object for each session
            $session=[PSCustomObject]@{
                'SessionName'=$columns[0]
                'Username'=$columns[1]
                'Id'=$columns[2]
                'State'=$columns[3]
                'IdleTime'=$columns[4]
                'LogonTime'=$columns[5]
            }
            $sessions+=$session
        }

        $conchk=$null
        $rdpchk=$null
        $dischk=$null
        foreach($item in $sessions){
            if(($item.SessionName.startswith('>console') -or $item.SessionName.startswith('console')) -and $item.State -eq 'Active'){
                # console Active = Locked
                # >console Active = Unlocked
                # write-host 'found active console session'
                $conchk=$true
                break
            }
            if($item.SessionName.startswith('>rdp-tcp#')){
                # write-host 'found active rdp session'
                $rdpchk=$true
                break
            }
            if($item.SessionName -eq '>' -and $item.Username.length -ne 0 -and $item.State -eq 'Disc'){
                # write-host 'found disconnected session'
                $dischk=$true
                break
            }
        }

        if($null -eq $conchk -and $null -eq $rdpchk -and $null -eq $dischk){
            # write-host 'no active RDP / console session and no disconnected session, proceed with all updates'
            $updatedcu=-join('"',$dcupath,'" /ApplyUpdates -autoSuspendBitlocker=enable -reboot=enable -silent -updateDeviceCategory=audio,chipset,input,others,network,storage,video -updateSeverity=critical,recommended,security -updateType=application,bios,driver,firmware,others')
            cmd.exe /c $dculock
            cmd.exe /c $updatedcu
            cmd.exe /c $unlockdcu
        }else{
            # write-host 'Session detected, non-disruptive updates only'
            # While logged in active console session ran below and got: "Installation for these updates were interrupted, will get installed after UserActivity completes." Few minutes later, while still logged in update applied and notified via Windows "Notifications"
            $updatemindcu=-join('"',$dcupath,'" /ApplyUpdates -autoSuspendBitlocker=enable -reboot=disable -silent -updateDeviceCategory=chipset,others,storage -updateSeverity=critical,security -updateType=application,driver,others')
            cmd.exe /c $dculock
            cmd.exe /c $updatemindcu
            cmd.exe /c $unlockdcu
        }
    }
}