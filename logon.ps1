<#
Windows PowerShell cannot run as SYSTEM due to add-appxpackage

$tt=new-scheduledtasktrigger -atlogon
$ta=new-scheduledtaskaction -execute 'powershell.exe' -argument '-nologo -windowstyle hidden -ExecutionPolicy Bypass -command ". $env:homedrive\scripts\logon.ps1; exit $LASTEXITCODE"' -workingdirectory "$env:homedrive\scripts"
$principal=New-ScheduledTaskPrincipal -groupid "BUILTIN\Administrators" -RunLevel Highest
$tle=new-timespan -minutes 5
$tlr=new-timespan -minutes 1
# https://learn.microsoft.com/en-us/powershell/module/scheduledtasks/new-scheduledtasksettingsset?view=windowsserver2022-ps
$ts=new-scheduledtasksettingsSet -AllowStartIfOnBatteries -compatibility 'Win8' -DontStopIfGoingOnBatteries -DontStopOnIdleEnd -executionTimelimit $tle -hidden -restartcount 1 -restartinterval $tlr
register-scheduledtask 'custom\logon.ps1' -action $ta -settings $ts -trigger $tt -principal $principal

If trigger on startup required:
Trigger on event with XML:
<QueryList>
  <Query Id="0" Path="Microsoft-Windows-TaskScheduler/Operational">
    <Select Path="Microsoft-Windows-TaskScheduler/Operational">
      *[System[(Level=4) and (EventID=201) and Provider[@Name='Microsoft-Windows-TaskScheduler'] and TimeCreated[timediff(@SystemTime) &lt;= 1800000]]]
      and 
      *[EventData[Data[@Name='TaskName']='\custom\startup.ps1']]
    </Select>
  </Query>
</QueryList>

WINDOWS DEFENDER
ENVIRONMENT VARIABLES
WINDOWS FIREWALL
REGISTRY HKCU
VSC
JUNKWARE UNINSTALL
WINDOWS POWERSHELL COLORS
SHORTCUTS
POWERSHELLGET
PSREADLINE https://github.com/PowerShell/PSReadLine/issues/3902
WINGET

#>
$dt=get-date -f 'yyyyMMddHHmmss'
$ScriptInvocation = (Get-Variable MyInvocation -Scope Script).Value
$ScriptName = $ScriptInvocation.MyCommand.Name
$fn="$env:homedrive\scripts\LOGS\$dt-$ScriptName.txt"
start-transcript -path $fn -IncludeInvocationHeader

# https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_preference_variables
$ErrorActionPreference='Continue'
$ProgressPreference='Continue'
$warningPreference='Continue'
<# DEBUG
$ErrorActionPreference='Stop'
#>

# WINDOWS DEFENDER START

Set-MpPreference -DisableRealtimeMonitoring $false

add-mppreference -exclusionpath "$env:systemdrive\Users\$env:username\AppData\Local\thinkorswim"
add-mppreference -exclusionprocess "$env:systemdrive\Users\$env:username\AppData\Local\thinkorswim\thinkorswim.exe"

Set-MpPreference -DisableRealtimeMonitoring $true
# WINDOWS DEFENDER END

# ENVIRONMENT VARIABLES START

# PWSH Telemetry Disable https://github.com/powershell/powershell
[Environment]::SetEnvironmentVariable('POWERSHELL_TELEMETRY_OPTOUT','1','User')

# .NET Telemetry Disable https://docs.microsoft.com/en-us/dotnet/core/tools/telemetry#collected-options
[Environment]::SetEnvironmentVariable('DOTNET_CLI_TELEMETRY_OPTOUT','1','User')

# ENVIRONMENT VARIABLES END

# WINDOWS FIREWALLS START

$gnfw=get-netfirewallrule
if($gnfw.displayname.Contains('THINKORSWIM') -eq $false){
    $fwp=-join('%SYSTEMDRIVE%',"\Users\$env:username\AppData\Local\thinkorswim\thinkorswim.exe")
    new-netfirewallrule -action allow -displayname 'THINKORSWIM' -direction outbound -group 'TRADING' -program $fwp -profile Any
}

# WINDOWS FIREWALLS END

# REGISTRY HKCU START
<#
# TODO Settings > Display > Nightlight
Set hours > Turn on 9pm > Turn off 8pm > Strength Middle
#>

#Settings > System > Display > Graphics
# 1 is Power Save (i.e. iGPU) and 2 is Performance but iGPU Intel UHD 770 is better than NVIDIA NVS 510. Could do logic to check cards https://learn.microsoft.com/en-us/windows/win32/api/dxgi1_6/ne-dxgi1_6-dxgi_gpu_preference

$gpunum='1'
$gpupref=-join('SwapEffectUpgradeEnable=0;GpuPreference=',$gpunum,';')
$gpuprefgame=-join('SwapEffectUpgradeEnable=1;GpuPreference=',$gpunum,';')

# CMEG+DAS
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name "$env:systemdrive\Traders Elite Pro CMEG_x64\DasTrader64.exe" -Value $gpupref -type 'string'

# Desktop Windows Manager
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name "$env:windir\System32\dwm.exe" -Value $gpupref -type 'string'

# Steam
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name "${env:ProgramFiles(x86)}\Steamwebhelper.exe" -Value $gpuprefgame -type 'string'
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name "${env:ProgramFiles(x86)}\gameoverlayui.exe" -Value $gpuprefgame -type 'string'

<# Steam Baldur's Gate 3
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name "${env:ProgramFiles(x86)}\Steam\steamapps\common\Baldurs Gate 3\bin\bg3.exe" -Value $gpuprefgame -type 'string'
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name "${env:ProgramFiles(x86)}\Steam\steamapps\common\Baldurs Gate 3\bin\bg3_dx11.exe" -Value $gpuprefgame -type 'string'
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name "${env:ProgramFiles(x86)}\Steam\steamapps\common\Baldurs Gate 3\Launcher\LariLauncher.exe" -Value $gpuprefgame -type 'string'
#>

# Steam Marvel Snap
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name "${env:ProgramFiles(x86)}\Steam\steamapps\common\MARVEL SNAP\SNAP.exe" -Value $gpuprefgame -type 'string'

# Steam Rogue Fable IV
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name "${env:ProgramFiles(x86)}\Steam\steamapps\common\Rogue Fable IV\RogueFableIV.exe" -Value $gpuprefgame -type 'string'
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name "${env:ProgramFiles(x86)}\Steam\steamapps\common\Rogue Fable IV\nwjc.exe" -Value $gpuprefgame -type 'string'

<# Steam Vermintide 2 > Game appears to require disabling Memory Integrity for anti-cheat (i.e. not happening)
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name "${env:ProgramFiles(x86)}\Steam\steamapps\common\Warhammer Vermintide 2\binaries\vermintide2.exe" -Value $gpuprefgame -type 'string'
#>

# thinkorswim
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name "$env:systemdrive\Users\$env:username\AppData\Local\thinkorswim\thinkorswim.exe" -Value $gpupref -type 'string'
$tospath="$env:systemdrive\Users\$env:username\AppData\Local\thinkorswim\"
$tosqa=Get-ChildItem -Path $tospath -Recurse -Filter 'chromium.exe'
if($null -ne $tosqa){
    $toscapp=-join($tosqa.DirectoryName,'\chromium.exe')
    Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name $toscapp -Value $gpupref -type 'string'
}

# Task Manager > Startup apps

remove-itemproperty -path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run' -name 'Steam' -ea 0

# sysdm.cpl > Advanced > Settings > Visual Effects "Adjust for best performance"
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\VisualEffects' -Name VisualFXSetting -Value 00000002

# Region Day of Week in Taskbar
Set-ItemProperty -Path 'HKCU:\Control Panel\International' -Name sShortDate -Value 'ddd M/d/yyyy'

<# Settings > Personalization > Background
# Personalize your background Solid Color
# Desktop Background Black
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Wallpapers' -Name BackgroundType -Type DWORD -Value 1
Set-ItemProperty -Path 'HKCU:\Control Panel\Desktop' -Name WallPaper -Value '' -type 'string'
Set-ItemProperty -Path 'HKCU:\Control Panel\Colors' -Name Background -Value '255 255 255'
$job3 = start-job -scriptblock {
add-type -typedefinition "using System;`n using System.Runtime.InteropServices;`n public class PInvoke { [DllImport(`"user32.dll`")] public static extern bool SetSysColors(int cElements, int[] lpaElements, int[] lpaRgbValues); }"
[PInvoke]::SetSysColors(1, @(1), @(0x000000))
}

# Settings > Personalization > Color > Color Mode Dark
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name AppsUseLightTheme -Value 0
#>

# https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-experience#allowspotlightcollection
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\CloudContent' -Name AllowSpotlightCollection -Value 0
# https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-experience#allowtailoredexperienceswithdiagnosticdata
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\CloudContent' -Name AllowTailoredExperiencesWithDiagnosticData -Value 0
# https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-experience#allowspotlightcollection
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\CloudContent' -Name DisableSpotlightCollectionOnDesktop -Value 0
# https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-experience#allowtailoredexperienceswithdiagnosticdata
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\CloudContent' -Name DisableTailoredExperiencesWithDiagnosticData -Value 0
# Windows Spotlight https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-experience#allowwindowsspotlight
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\CloudContent' -Name DisableWindowsSpotlightFeatures -Value 0



# Settings > System > Multitasking
# "Snap windows" off (requires restart to show in GUI as off)
Set-ItemProperty -Path 'HKCU:\Control Panel\Desktop' -Name WindowArrangementActive -Value 0

# "Snap windows" > "Show snap layouts when I hover over a window's maximize button" uncheck
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name EnableSnapAssistFlyout -Value 0
# JointResize DWORD
# "When I snap window, show what I can snap next to it" disabled
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name SnapAssist -Value 0
# SnapFill DWORD

# Settings > Accessibility > Keyboard
# "Use the Print screen key to open screen capture" Off
Set-ItemProperty -Path 'HKCU:\Control Panel\Keyboard' -Name PrintScreenKeyForSnippingEnabled -Value 0

# Settings > Time & language > Typing
# Touch keyboard
# Play key sounds as I type Off
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\TabletTip\1.7' -name EnableKeyAudioFeedback -Value 0
# Autocorrect misspelled words Off
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\TabletTip\1.7' -name EnableAutocorrection -Value 0
# Highlight misspelled words Off
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\TabletTip\1.7' -name EnableSpellchecking -Value 0

# Typing Insights Off
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\input\Settings' -Name InsightsEnabled -Value 0

# Inking & Typing https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-admx-globalization#implicitdatacollectionoff_1
if((test-path 'HKCU:\Software\Policies\Microsoft\InputPersonalization') -eq $false){
    New-Item -Path 'HKCU:\Software\Policies\Microsoft\InputPersonalization'
}
Set-ItemProperty -Path 'HKCU:\Software\Policies\Microsoft\InputPersonalization' -Name RestrictImplicitInkCollection -Value 1
Set-ItemProperty -Path 'HKCU:\Software\Policies\Microsoft\InputPersonalization' -Name RestrictImplicitTextCollection -Value 1

# Settings > Privacy > Radios > Access to control radios on this device > Off
# BingSearchEnabled
if((test-path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\Explorer') -eq $false){
    New-Item -Path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\Explorer'
}
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\Explorer' -Name DisableSearchBoxSuggestions -Value 1
Set-ItemProperty -Path 'HKCU:\Control Panel\Accessibility' -Name DynamicScrollbars -Value 0

# Settings > Privacy & security > Inking & typing personalization
# "Custom inking and typing dictionary" > Off
$ipath='HKCU:\Software\Microsoft\Windows\CurrentVersion\CPSS\Store\InkingAndTypingPersonalization'
if((test-path $ipath) -eq $false){
    new-item -path $ipath
}
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\CPSS\Store\InkingAndTypingPersonalization' -name 'Value' -value 0
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Personalization\Settings' -name 'AcceptedPrivacyPolicy' -value 0
#https://learn.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\InputPersonalization' -name 'RestrictImplicitInkCollection' -value 1
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\InputPersonalization' -name 'RestrictImplicitTextCollection' -value 1

# Copilot Disable https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-windowsai
if((test-path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\WindowsCopilot') -eq $false){
    New-Item -Path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\WindowsCopilot'
}
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\WindowsCopilot' -Name TurnOffWindowsCopilot -Value 1

# Defender SmartScreen for Microsoft Store Apps disable
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AppHost' -Name EnableWebContentEvaluation -Value 0
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AppHost' -Name PreventOverride -Value 0

# DisallowShaking
if((test-path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced') -eq $false){
    New-Item -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
}
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -name DisallowShaking -value 1

# Speech, inking, and typing / Disable updates to speech recognition and speech synthesis models
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\InputPersonalization\TrainedDataStore' -Name HarvestContacts -Value 0

# Game Mode
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\GameBar' -Name AutoGameModeEnabled -Value 0

# ms-gamingoverlay disable
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\GameDVR' -Name AppCaptureEnabled -Value 0
Set-ItemProperty -Path 'HKCU:\System\GameConfigStore' -Name GameDVR_Enabled -Value 0

# Inking & Personalization https://learn.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services#bkmk-priv-ink
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\InputPersonalization' -Name 'RestrictImplicitInkCollection' -Value 1
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\InputPersonalization' -Name 'RestrictImplicitTextCollection' -Value 1

# Settings > Personalization > Dynamic Lighting
# Use Dynamic Lightting on my devices Off
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Lighting' -Name 'AmbientLightingEnabled' -Value 0
# Compatible apps in the foreground always control lighting Off
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Lighting' -Name 'ControlledByForegroundApp' -Value 0

# Let websites provide locally relevant content by accessing my language list disable
Set-ItemProperty -Path 'HKCU:\Control Panel\International\User Profile' -Name HttpAcceptLanguageOptOut -Value 1

# Let Windows track app launches to improve Start and search results disable
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name Start_TrackProgs -Value 0

# Live Tiles disable
if((test-path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications') -eq $false){
    New-Item -Path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications'
}
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications' -Name NoCloudApplicationNotification -Value 1

# Messaging cloud sync https://docs.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services#bkmk-syncsettings
if((test-path 'HKCU:\SOFTWARE\Microsoft\Messaging') -eq $false){
    New-Item -Path 'HKCU:\SOFTWARE\Microsoft\Messaging'
}
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Messaging' -Name CloudServiceSyncEnabled -Value 0

# Disable process video automatically to enhance it (depends on your device hardware)
if((test-path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\VideoSettings') -eq $false){
    new-item -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\VideoSettings'
}
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\VideoSettings' -Name EnableAutoEnhanceDuringPlayback -Value 0

# Control Panel > Sound > Sounds tab > No Sounds
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes' -Name '(Default)' -Value '.None'
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\.Default\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\CriticalBatteryAlarm\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\DeviceConnect\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\DeviceDisconnect\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\DeviceFail\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\FaxBeep\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\LowBatteryAlarm\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\MailBeep\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\MessageNudge\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\Notification.Default\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\Notification.IM\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\Notification.Mail\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\Notification.Proximity\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\Notification.Reminder\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\Notification.SMS\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\ProximityConnection\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\SystemAsterisk\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\SystemExclamation\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\SystemExclamation\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\SystemHand\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\SystemNotification\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\.Default\WindowsUAC\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\sapisvr\DisNumbersSound\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\sapisvr\HubOffSound\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\sapisvr\HubOnSound\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\sapisvr\HubSleepSound\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\sapisvr\MisrecoSound\.Current' -Name '(Default)' -Value ''
Set-ItemProperty -Path 'HKCU:\AppEvents\Schemes\Apps\sapisvr\PanelSound\.Current' -Name '(Default)' -Value ''


# Mangifier starts minimiized
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\ScreenMagnifier' -Name MagnifierUIWindowMinimized -Value 1

# Speech
if((test-path 'HKCU:\Software\Microsoft\Speech_OneCore\Settings\') -eq $false){
    new-item -path 'HKCU:\Software\Microsoft\Speech_OneCore\Settings\'
}
if((test-path 'HKCU:\Software\Microsoft\Speech_OneCore\Settings\OnlineSpeechPrivacy') -eq $false){
    new-item -path 'HKCU:\Software\Microsoft\Speech_OneCore\Settings\OnlineSpeechPrivacy'
}
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Speech_OneCore\Settings\OnlineSpeechPrivacy' -Name HasAccepted -Value 0

# Stop minimizing all other windows when dragging a window with mouse https://answers.microsoft.com/en-us/windows/forum/windows8_1-desktop/disable-aero-shake/a1f2e81f-beb2-4541-9398-43af0c505912
if((test-path 'HKCU:\Software\Policies\Microsoft\Windows\Explorer') -eq $false){
    New-Item -Path 'HKCU:\Software\Policies\Microsoft\Windows\Explorer'
}
Set-ItemProperty -Path 'HKCU:\Software\Policies\Microsoft\Windows\Explorer' -Name NoWindowMinimizingShortcuts -Value 1

Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\AdvertisingInfo' -Name 'Enabled' -Value '0' -Type 'dword'

Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager' -Name 'RotatingLockScreenOverlayEnabled' -Value '0' -Type 'dword'
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager' -Name 'SubscribedContent-338387Enabled' -Value '0' -Type 'dword'
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager' -Name 'SubscribedContent-338393Enabled' -Value '0' -Type 'dword'
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager' -Name 'SubscribedContent-353694Enabled' -Value '0' -Type 'dword'
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager' -Name 'SubscribedContent-353696Enabled' -Value '0' -Type 'dword'
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager' -Name 'SubscribedContent-338389Enabled' -Value '0' -Type 'dword'
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager' -Name 'SubscribedContent-310093Enabled' -Value '0' -Type 'dword'
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Privacy' -Name 'TailoredExperiencesWithDiagnosticDataEnabled' -Value '0' -Type 'dword'

if((test-path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\UserProfileEngagement') -eq $false){
    New-Item -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\UserProfileEngagement'
}
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\UserProfileEngagement' -Name 'ScoobeSystemSettingEnabled' -Value '0' -Type 'dword'

# Settings > Personalization > Taskbar
# Search > Hide
set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Search' -name 'SearchboxTaskbarMode' -value 0

# Copilot (preview) > Off
set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -name 'ShowCopilotButton' -value 0
# Task view > Off
set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -name 'ShowTaskViewButton' -value 0
# Widgets > Off
set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -name 'TaskbarDa' -value 0 -type 'dword'
# Taskbar alignment Left
set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -name 'TaskbarAl' -value 0
# show seconds in system tray clock (users more power)
set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -name 'ShowSecondsInSystemClock' -value 1
# Wow new taskbar is annoying. Prefer old style of vertical alignment to show all windows. "Always" is "best of the worst" option
# Combine taskbar buttons and hide labels > Always
set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -name 'TaskbarGlomLevel' -value 0
# Combine taskbar buttons and hide labels  on other taskbars > Always
set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -name 'MMTaskbarGlomLevel' -value 0

Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name "ShowSyncProviderNotifications" -Value '0' -Type 'dword'

Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name 'Start_IrisRecommendations' -Value '0' -Type 'dword'

# Use Small taskbar buttons
set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -name 'TaskbarSmallIcons' -value 1

# Show news and interests on the taskbar Off
set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Feeds' -name 'ShellFeedsTaskbarViewMode' -value 2

# Transparency Effects disable
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name EnableTransparency -Value 0

# Videos Library access disable

# Window preview when hovering mouse over taskbar icons disable
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name ExtendedUIHoverTime -Value 196608

# REGISTRY END

# VSC START
$vscpath_1="$env:APPDATA\VSCodium\User\settings.json"
# Establish file base
$ef="{`n}"
$sfinal_1=$null
$sfinal_1=$ef.insert(1,"`n    `"chat.commandCenter.enabled`": false")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"editor.minimap.enabled`": false")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"editor.stickyScroll.enabled`": false")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"editor.tabCompletion`": `"on`"")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"editor.wordWrap`": `"on`"")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"telemetry.telemetryLevel`": `"off`"")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"terminal.integrated.enablePersistentSessions`": false")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"terminal.integrated.scrollback`": 10000000")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"window.autoDetectColorScheme`": false")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"window.autoDetectHighContrast`": false")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"workbench.colorTheme`": `"Default Dark Modern`"")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"workbench.commandPalette.experimental.enableNaturalLanguageSearch`": false")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"workbench.enableExperiments`": false")
# $sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"workbench.preferredLightColortheme`": `"Default Dark Modern`"")
# Natural Language Search https://code.visualstudio.com/docs/getstarted/telemetry#_managing-online-services
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"workbench.settings.enableNaturalLanguageSearch`": false")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"workbench.startupEditor`": `"newUntitledFile`"")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"workbench.welcomePage.extraAnnouncements`": false")
$sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"workbench.welcomePage.walkthroughs.openOnInstall`": false")

# PowerShell Extension
$userhomepath_1=$env:userprofile
$pwshextpath_1="$userhomepath_1\.vscode-oss\extensions\"
$pwsextchk_1=(get-childitem -path $pwshextpath_1 | Where-Object {$_.Name -like 'ms-vscode.powershell-*'})
if($pwsextchk_1.count -gt 0){
    $sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"powershell.codeFormatting.newLineAfterCloseBrace`": false")
    $sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"powershell.developer.editorServicesLogLevel`": `"None`"")
    $sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"powershell.integratedConsole.forceClearScrollbackBuffer`": false")
    $sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"powershell.integratedConsole.suppressStartupBanner`": true")
}

# GitHub Copilot Extension
$userhomepath_1=$env:userprofile
$gcoextpath_1="$userhomepath_1\.vscode-oss\extensions\"
$gcoextchk_1=(get-childitem -path $gcoextpath_1 | Where-Object {$_.Name -like 'ms-vscode.*copilot*'})
if($gcoextchk_1.count -gt 0){
    $sfinal_1=$sfinal_1.insert($sfinal_1.length-2,",`n    `"github.copilot.enable`": {`n        `"*`": false`n    }")
}

set-content -path $vscpath_1 -value $sfinal_1 -nonewline
# VSC END

# JUNKWARE UNINSTALL START

<# Fidelity Active Trader Pro
20250304 As per Fidelity, in reaction to SEC 15c3-5 they have implemented policies that restrict their retail customers from trading stocks either in blocks (e.g. 10,000 shares) and/or making trades on securities that exceed an average trading volume. If a customer attempts to do so, the customer will get an error (e.g. MA1501) that requires them to call in, wait on hold, and over the course of an hour maybe get a one time temporary exception for that day to make the trade. There are no exceptions to this as per MGMT. Given they don't have real trading hotkeys, their macOS Apple Silicon platform is buggy requiring frequent reinstalls, and silly Windows directory structure, this software is not for active traders. Other brokers don't have these problems.

# WINDOWS FIREWALL
# $fatpcef=-join($fatpqa.DirectoryName,'\CefSharpBrowserSubprocess.exe')
# auto requested inbound... why?! Program works when blocked anyways.
# new-netfirewallrule -action allow -displayname 'FATP' -direction inbound -group 'TRADING' -program $fatpcef -profile Any
$fatpqapath="$env:systemdrive\Users\$env:username\AppData\Local\Apps"
$fatpqa=Get-ChildItem -Path $fatpqapath -Recurse -Filter 'ActiveTraderPro.exe'
if($null -ne $fatpqa){
    if($gnfw.displayname.Contains('FATP') -eq $false){
        $fatpapp=-join($fatpqa.DirectoryName,'\ActiveTraderPro.exe')
        new-netfirewallrule -action allow -displayname 'FATP' -direction outbound -group 'TRADING' -program $fatpapp -profile Any
    }
    foreach($rule in $gnfw){
        if(
            ($rule.DisplayName -eq 'cefsharp.browsersubprocess.exe') -and
            ($rule.name -match [regex]::Escape($fatpqa.DirectoryName))
        ){
            set-netfirewallrule $rule.name -action block
        }
    }
}

# REGISTRY HKCU
$fatpqapath="$env:systemdrive\Users\$env:username\AppData\Local\Apps"
$fatpqa=Get-ChildItem -Path $fatpqapath -Recurse -Filter 'ActiveTraderPro.exe'
if($null -ne $fatpqa){
    $fatpapp=-join($fatpqa.DirectoryName,'\ActiveTraderPro.exe')
    Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\DirectX\UserGpuPreferences' -Name $fatpapp -Value $gpupref -type 'string'
}

# WINDOWS DEFENDER
$fatpqa=Get-ChildItem -Path "$env:systemdrive\Users\$env:username\AppData\Local\Apps" -Recurse -Filter 'ActiveTraderPro.exe'
if($null -ne $fatpqa){
    add-mppreference -exclusionpath $fatpqa.DirectoryName
    $fatpapp=-join($fatpqa.DirectoryName,'\ActiveTraderPro.exe')
    add-mppreference -exclusionprocess $fatpapp
    $fatpcef=-join($fatpqa.DirectoryName,'\CefSharpBrowserSubprocess.exe')
    add-mppreference -exclusionprocess $fatpcef
}
#>
$fadpath="$env:systemdrive\Users\$env:username\AppData\Local\ActiveTraderPro"
if(test-path $fadpath){
    remove-item -path $fadpath -recurse -force
}

$fadpath2="$env:systemdrive\Users\$env:username\AppData\Local\Apps\"
$fadpaths=get-childitem -path $fadpath2
if($null -ne $fadpaths){
    foreach($fitem in $fadpaths){
        $fdir=-join("$env:systemdrive\Users\$env:username\AppData\Local\Apps\",$fitem)
        $fchk=Get-ChildItem -Path $fdir -Recurse -Filter 'ActiveTraderPro*'
        if($null -ne $fchk){
            remove-item -path $fdir -recurse -force
        }
    }
}
remove-item -path 'HKCU:\Software\Fidelity Investments' -force -recurse

# OneDrive
stop-process -name 'OneDrive.exe' -force -ea 0
winget uninstall --id 'microsoft.onedrive' --silent
if(test-path "$env:windir\syswow64\onedrivesetup.exe"){
    start-process "$env:windir\syswow64\onedrivesetup.exe" -argumentlist '/uninstall'
}

$usrchk+=(get-childitem -path "$env:SYSTEMDRIVE\Users").name
foreach($usr in $usrchk){
    $upath1="$env:SYSTEMDRIVE\Users\$usr\AppData\Local\Microsoft\OneDrive"
    if(test-path $upath1){
        remove-item -path $upath1 -recurse -force
    }
    $upath2="$env:SYSTEMDRIVE\Users\$usr\OneDrive"
    if(test-path $upath2){
        remove-item -path $upath2 -recurse -force
    }
}

$gipod=Get-ItemProperty -Path 'HKCU:\Environment'
if($null -ne $gipod.OneDrive){
    remove-itemProperty -path 'HKCU:\Environment' -Name 'OneDrive'
}

<# Uninstall Office 365 / OneDrive
Does not actually respect silent as force shows GUI to click button to uninstall, and can fail to uninstall. Use uninstaller as "one and done" WORKAROUND
https://support.microsoft.com/en-us/office/uninstall-office-from-a-pc-9dd49b83-264a-477a-8fcc-2fdf5dbf61d8?ui=en-us&rs=en-us&ad=us#OfficeVersion=Click-to-Run_or_MSI
winget uninstall --name "Microsoft 365 - en-us" --silent --accept-source-agreements
winget uninstall --name "Microsoft 365 - es-es" --silent --accept-source-agreements
winget uninstall --name "Microsoft 365 - fr-fr" --silent --accept-source-agreements
winget uninstall --name "Microsoft OneDrive - en-us" --silent --accept-source-agreements
winget uninstall --name "Microsoft OneDrive - es-es" --silent --accept-source-agreements
winget uninstall --name "Microsoft OneDrive - fr-fr" --silent --accept-source-agreements
#>

# https://learn.microsoft.com/en-us/windows/package-manager/winget/
<# Cannot use WINGET for anything further until feature complete (e.g. unavoidable forced reboots issue is resolved https://github.com/microsoft/winget-cli/issues/3988 )
$lowg=winget list --id TheDocumentFoundation.LibreOffice
if($lowg -match '^No installed package found matching input criteria.'){
    # Install
    $null=winget install --id TheDocumentFoundation.LibreOffice --accept-package-agreements --silent
    # DEBUG winget uninstall --id TheDocumentFoundation.LibreOffice --silent
}
#>

# GLANCE
$gnpath='HKCU:\Software\GlanceNetworks'
if((test-path $gnpath) -eq $true){
    remove-item -path 'HKCU:\Software\GlanceNetworks' -recurse
}
remove-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\ApplicationAssociationToasts' -Name 'glanceguest_glanceguest' -ea 0
remove-itemproperty -path 'HKCU:\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Compatibility Assistant\Store' -Name 'C:\Program Files (x86)\GlanceGuest\GProtocolHandler.exe' -ea 0
remove-itemproperty -path 'HKCU:\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Compatibility Assistant\Store' -Name 'C:\Program Files (x86)\GlanceGuest\unins000.exe' -ea 0
$gigs=(get-item -path 'HKCU:\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Compatibility Assistant\Store').Property
foreach($gig in $gigs){
    if($gig -like '*GlanceGuestSetup*'){
        remove-itemproperty -path 'HKCU:\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Compatibility Assistant\Store' -name $gig
    }
}
$gglist=@()
$gglist+=get-itemproperty -path 'HKCU:\Software\Microsoft\Internet Explorer\LowRegistry\Audio\PolicyConfig\PropertyStore\*'
foreach($ggitem in $gglist){
    if($ggitem -like '*GlanceGuest\Glance.exe*'){
        remove-item -path $ggitem.PSPath -recurse
    }
}
if((test-path "$env:LOCALAPPDATA\Glance") -eq $true){
    remove-item -path "$env:LOCALAPPDATA\Glance" -recurse
}

# JUNKWARE UNINSTALL END

# WINDOWS POWERSHELL COLORS START

<#
https://en.wikipedia.org/wiki/ANSI_escape_code
DEFAULTS
get-psreadlineoption
EditMode                               : Windows
AddToHistoryHandler                    : System.Func`2[System.String,System.Object]
HistoryNoDuplicates                    : True
HistorySavePath                        : C:\Users\USERNAME\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadLine\ConsoleH
                                         ost_history.txt
HistorySaveStyle                       : SaveIncrementally
HistorySearchCaseSensitive             : False
HistorySearchCursorMovesToEnd          : False
MaximumHistoryCount                    : 4096
ContinuationPrompt                     : >>
ExtraPromptLineCount                   : 0
PromptText                             : {> }
BellStyle                              : Audible
DingDuration                           : 50
DingTone                               : 1221
CommandsToValidateScriptBlockArguments : {ForEach-Object, %, Invoke-Command, icm...}
CommandValidationHandler               :
CompletionQueryItems                   : 100
MaximumKillRingCount                   : 10
ShowToolTips                           : True
ViModeIndicator                        : None
WordDelimiters                         : ;:,.[]{}()/\|!?^&*-=+'"–—―
AnsiEscapeTimeout                      : 100
PredictionSource                       : History
PredictionViewStyle                    : InlineView
TerminateOrphanedConsoleApps           : False
CommandColor                           : "$([char]0x1b)[93m"
CommentColor                           : "$([char]0x1b)[32m"
ContinuationPromptColor                : "$([char]0x1b)[37m"
DefaultTokenColor                      : "$([char]0x1b)[37m"
EmphasisColor                          : "$([char]0x1b)[96m"
ErrorColor                             : "$([char]0x1b)[91m"
InlinePredictionColor                  : "$([char]0x1b)[97;2;3m"
KeywordColor                           : "$([char]0x1b)[92m"
ListPredictionColor                    : "$([char]0x1b)[33m"
ListPredictionSelectedColor            : "$([char]0x1b)[48;5;238m"
ListPredictionTooltipColor             : "$([char]0x1b)[97;2;3m"
MemberColor                            : "$([char]0x1b)[37m"
NumberColor                            : "$([char]0x1b)[97m"
OperatorColor                          : "$([char]0x1b)[90m"
ParameterColor                         : "$([char]0x1b)[90m"
SelectionColor                         : "$([char]0x1b)[30;47m"
StringColor                            : "$([char]0x1b)[36m"
TypeColor                              : "$([char]0x1b)[37m"
VariableColor                          : "$([char]0x1b)[92m"

#REFERENCE
$VARIABLECOLOR.MEMBERCOLOR="STRINGCOLOR"
$psro.ErrorColor="$([char]0x1b)[38;5;51m"

$host.ui.rawui
ForegroundColor       : Gray
BackgroundColor       : Black
CursorPosition        : 0,6
WindowPosition        : 0,0
CursorSize            : 25
BufferSize            : 120,9001
WindowSize            : 120,30
MaxWindowSize         : 120,115
MaxPhysicalWindowSize : 135,115
KeyAvailable          : False
WindowTitle           : Administrator: Windows PowerShell

DEFAULT
$host.PrivateData
ErrorForegroundColor    : Red
ErrorBackgroundColor    : Black
WarningForegroundColor  : Yellow
WarningBackgroundColor  : Black
DebugForegroundColor    : Yellow
DebugBackgroundColor    : Black
VerboseForegroundColor  : Yellow
VerboseBackgroundColor  : Black
ProgressForegroundColor : Yellow
ProgressBackgroundColor : DarkCyan

# REFERENCE

$host.PrivateData.WarningForegroundColor='Blue'
$host.PrivateData.WarningBackgroundColor='White'
$host.PrivateData.DebugForegroundColor='Blue'
$host.PrivateData.DebugBackgroundColor='White'
$host.PrivateData.VerboseForegroundColor='Blue'
$host.PrivateData.VerboseBackgroundColor='White'
#>

$csb={
$psro=get-psreadlineoption
if(
    $null -ne (get-process -name magnify -ea 0) -and
    (get-itemproperty -path 'HKCU:\Software\Microsoft\ScreenMagnifier' -Name Invert).Invert -eq 1
){
    #https://en.wikipedia.org/wiki/ANSI_escape_code
    $psro.CommandColor="$([char]0x1b)[107m"
    $psro.CommentColor="$([char]0x1b)[38;5;207m"
    $psro.ContinuationPromptColor="$([char]0x1b)[90m"
    $psro.DefaultTokenColor="$([char]0x1b)[90m"
    $psro.EmphasisColor="$([char]0x1b)[95m"
    $psro.ErrorColor="$([char]0x1b)[38;5;51m"
    $psro.InlinePredictionColor="$([char]0x1b)[97;2;3m"
    $psro.KeywordColor="$([char]0x1b)[92m"
    $psro.ListPredictionColor="$([char]0x1b)[33m"
    $psro.ListPredictionSelectedColor="$([char]0x1b)[48;5;235m"
    $psro.ListPredictionTooltipColor="$([char]0x1b)[95m"
    $psro.MemberColor="$([char]0x1b)[90m"
    $psro.NumberColor="$([char]0x1b)[38;5;6m"
    $psro.OperatorColor="$([char]0x1b)[90m"
    $psro.ParameterColor="$([char]0x1b)[90m"
    $psro.SelectionColor="$([char]0x1b)[30;101m"
    $psro.StringColor="$([char]0x1b)[38;5;208m"
    $psro.TypeColor="$([char]0x1b)[107m"
    $psro.VariableColor="$([char]0x1b)[38;5;21m"
    $host.PrivateData.ErrorForegroundColor='Cyan'
}else{
    $psro.CommandColor="$([char]0x1b)[37m"
    $psro.CommentColor="$([char]0x1b)[32m"
    $psro.ContinuationPromptColor="$([char]0x1b)[90m"
    $psro.DefaultTokenColor="$([char]0x1b)[90m"
    $psro.EmphasisColor="$([char]0x1b)[38;5;46m"
    $psro.ErrorColor="$([char]0x1b)[91m"
    $psro.InlinePredictionColor="$([char]0x1b)[97;2;3m"
    $psro.keywordColor="$([char]0x1b)[38;5;165m"
    $psro.ListPredictionColor="$([char]0x1b)[38;5;68m"
    $psro.ListPredictionSelectedColor="$([char]0x1b)[48;5;252m"
    $psro.ListPredictionTooltipColor="$([char]0x1b)[38;5;46m"
    $psro.MemberColor="$([char]0x1b)[90m"
    $psro.NumberColor="$([char]0x1b)[38;5;166m"
    $psro.OperatorColor="$([char]0x1b)[90m"
    $psro.ParameterColor="$([char]0x1b)[90m"
    $psro.SelectionColor="$([char]0x1b)[30;106m"
    $psro.StringColor="$([char]0x1b)[38;5;33m"
    $psro.TypeColor="$([char]0x1b)[37m"
    $psro.VariableColor="$([char]0x1b)[38;5;226m"
    $host.PrivateData.ErrorForegroundColor='Red'
}
remove-variable psro
cd "$env:homedrive\scripts"
clear
}
# Windows PowerShell https://learn.microsoft.com/en-us/powershell/scripting/learn/shell/creating-profiles?view=powershell-5.1
$wpwshpath="$env:userprofile\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1"
if((test-path $wpwshpath) -eq $false){
    new-item -path $wpwshpath -itemtype file
}
set-content -path $wpwshpath -value $csb -nonewline

# Powershell
$pwshpath="$env:userprofile\Documents\PowerShell\Microsoft.PowerShell_profile.ps1"
if((test-path "$env:userprofile\Documents\PowerShell") -eq $false){
    new-item "$env:userprofile\Documents\PowerShell" -itemtype directory
}
if((test-path $pwshpath) -eq $false){
    new-item -path $pwshpath -itemtype file
}
set-content -path $pwshpath -value $csb -nonewline

# SHORTCUTS

if(
    (test-path -path ([System.IO.Path]::Combine([System.Environment]::GetFolderPath('Desktop'),'Recycle Bin.lnk'))) -eq $false
){
    $ShortcutLocation = [System.IO.Path]::Combine([System.Environment]::GetFolderPath('Desktop'),'Recycle Bin.lnk')
    $TargetPath='explorer.exe'
    $Arguments='shell:RecycleBinFolder'
    $Shell=New-Object -ComObject WScript.Shell
    $Shortcut=$Shell.CreateShortcut($ShortcutLocation)
    $Shortcut.TargetPath=$TargetPath
    $Shortcut.Arguments=$Arguments
    $Shortcut.IconLocation='shell32.dll,31'
    $Shortcut.Save()
}

#POWERSHELLGET START

# Avoid first run error "powershellget requires nuget provider version 2.8.5.201"
$gpp=get-packageprovider
$gppv=[Version]($gpp | where-object {$_.name -eq 'nuget'}).Version
if(
$null -eq $gppv -or
    $gppv -lt '2.8.5.201'
){
    install-packageprovider -name nuget -minimumversion 2.8.5.201 -force
}

# Prep for psreadline to avoid https://github.com/PowerShell/PSReadLine/issues/3902

$gmpo=(get-module -name powershellget).version
if($null -eq $gmpo){
    # PowerShellGet 1.x does not have -acceptlicense https://learn.microsoft.com/en-us/powershell/module/powershellget/install-module?view=powershellget-1.x
    $gpmv=[Version](get-module -name 'PackageManagement').Version
    if($null -ne $gpmv -and $gpmv -ge '1.4.8.1'){
        Install-Module -Name PowerShellGet
        <# This avoids log noise:
        WARNING: The version '1.4.8.1' of module 'PackageManagement' is currently in use. Retry the operation after closing the applications.
        #>
    }else{
        Install-Module -Name PowerShellGet -allowclobber -Force
    }
}

# POWERSHELLGET END

# PSREADLINE START

$gmps=(get-module -name psreadline).version
if($null -eq $gmps){
    install-module -name psreadline -allowclobber -Force
}

# PSREADLINE END

# WINGET START

# winget native installation busted Windows 10 https://github.com/MicrosoftDocs/windows-dev-docs/issues/4787
<# WINGET doesn't work with SYSTEM by default https://github.com/microsoft/winget-cli/tree/master/doc/troubleshooting#common-issues get error as SYSTEM:
TerminatingError(): "Program 'winget.exe' failed to run: The file cannot be accessed by the systemAt c:\startup.ps1
#>
$wingetchk=[version]((winget -v).substring(1))
if(
$wingetchk -lt '1.9.25200'
){
    # winget not installed or old version install latest from github as one built into Windows 10 too old
    $wingetfile=-join("$env:windir\Temp\",'Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle')
    $wingeturi='https://github.com/microsoft/winget-cli/releases/download/v1.9.25200/Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle'
    invoke-webrequest -uri $wingeturi -outfile $wingetfile
    <#
    add-appxpackage -path $wingetfile
    add-appxpackage : Deployment failed with HRESULT: 0x80073CF9, Install failed. Please contact your software vendor. 
    (Exception from HRESULT: 0x80073CF9)
    Deployment Add operation rejected on package Microsoft.DesktopAppInstaller_2024.506.2113.0_neutral_~_8wekyb3d8bbwe 
    from: Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle install request because the Local System account is not 
    allowed to perform this operation.
    #>
    add-appxpackage -path $wingetfile -ForceApplicationShutdown
    remove-item -path $wingetfile
    #DEBUG remove-appxpackage -Name 'Microsoft.DesktopAppInstaller'
}

# check Powershell LTS installed
$pwshchk=winget list --id 'Microsoft.PowerShell' --accept-source-agreements
if($pwshchk.item($pwshchk.count-1) -match '^No installed package found'){
    winget search 'Microsoft.PowerShell' --accept-source-agreements
    winget install --id 'Microsoft.PowerShell' --accept-package-agreements --accept-source-agreements --silent --source winget
}

# Check Powershell Preview installed
$pwshpchk=winget list --id 'Microsoft.PowerShell.Preview' --accept-source-agreements
if($pwshpchk.item($pwshpchk.count-1) -match '^No installed package found'){
    winget search 'Microsoft.PowerShell.Preview' --accept-source-agreements
    winget install --id 'Microsoft.PowerShell.Preview' --accept-package-agreements --accept-source-agreements --silent --source winget
}

# update Powershell
winget upgrade --accept-package-agreements --accept-source-agreements --id 'Microsoft.PowerShell' --silent
winget upgrade --accept-package-agreements --accept-source-agreements --id 'Microsoft.PowerShell.Preview' --silent

# Intel Graphics Command Center
$gcwv=(Get-CimInstance -ClassName Win32_VideoController).Name
foreach($wvc in $gcwv){
    if($wvc -match '^Intel'){
        $ichk=winget list --id '9PLFNLNT3G5G' --accept-source-agreements
        if($ichk.item($ichk.count-1) -match '^No installed package found'){
            winget install --id '9PLFNLNT3G5G' --accept-package-agreements --accept-source-agreements --silent --source msstore
        }
        winget upgrade --accept-package-agreements --accept-source-agreements --id '9PLFNLNT3G5G' --silent
        break
    }
}

# WINGET END

# UPDATE WP HELP START
update-help -force -uiculture en-us -ea 0
<#
update-help : Failed to update Help for the module(s) 'ConfigDefender, ConfigDefenderPerformance' with UI culture(s) 
{en-US} : Unable to retrieve the HelpInfo XML file for UI culture en-US. Make sure the HelpInfoUri property in the 
module manifest is valid or check your network connection and then try the command again.
+ update-help -force -uiculture en-us
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : ResourceUnavailable: (:) [Update-Help], Exception
    + FullyQualifiedErrorId : UnableToRetrieveHelpInfoXml,Microsoft.PowerShell.Commands.UpdateHelpCommand
update-help : Failed to update Help for the module(s) 'ConfigDefender, ConfigDefenderPerformance' with
UI culture(s) {en-US} : Unable to retrieve the HelpInfo XML file for UI culture en-US. Make sure the
HelpInfoUri property in the module manifest is valid or check your network connection and then try the
command again.
#>
# UPDATE WP HELP END

Set-MpPreference -DisableRealtimeMonitoring $false
stop-transcript