<#
Windows PowerShell

$tt=new-scheduledtasktrigger -atstartup
$ta=new-scheduledtaskaction -execute 'powershell.exe' -argument '-nologo -windowstyle hidden -ExecutionPolicy Bypass -command ". $env:homedrive\scripts\startup.ps1; exit $LASTEXITCODE"' -workingdirectory "$env:homedrive\scripts"
$principal=New-ScheduledTaskPrincipal -UserID "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount -RunLevel Highest
$tle=new-timespan -minutes 15
$tlr=new-timespan -minutes 1
# https://learn.microsoft.com/en-us/powershell/module/scheduledtasks/new-scheduledtasksettingsset?view=windowsserver2022-ps
$ts=new-scheduledtasksettingsSet -AllowStartIfOnBatteries -compatibility 'Win8' -DontStopIfGoingOnBatteries -DontStopOnIdleEnd -executionTimelimit $tle -hidden -restartcount 1 -restartinterval $tlr
register-scheduledtask 'custom\startup.ps1' -action $ta -settings $ts -trigger $tt -principal $principal
wevtutil set-log Microsoft-Windows-TaskScheduler/Operational /enabled:true

WINDOWS DEFENDER
WINDOWS FIREWALL
ENVIRONMENT VARIABLES MACHINE
REMOTE DESKTOP CONNECTION
POWER SETTINGS
REGISTRY HKLM
HIBERNATE
TIME SETTINGS
SERVICES DISABLE
SERVICES ENABLE
WINDOWSCAPABILITY
WINDOWSOPTIONALFEATURE DISABLE
WINDOWSOPTIONALFEATURE ENABLE
STARTUP APPS
SCHEDULED TASK
MSIEXEC UNINSTALL
NVIDIA
JUNKWARE
APPXPACKAGE
APPXPROVISIONEDPACKAGE
USERCHOICE PROTECTION DRIVER
CHOCOLATEY
PIP
MARIADB
HARDWARE DIAGNOSTICS
MODULES RESTORE
#>

$dt=get-date -f 'yyyyMMddHHmmss'
$ScriptInvocation = (Get-Variable MyInvocation -Scope Script).Value
$ScriptName = $ScriptInvocation.MyCommand.Name
$fn="$env:homedrive\scripts\LOGS\$dt-$ScriptName.txt"
start-transcript -path $fn -IncludeInvocationHeader

# https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_preference_variables
$ErrorActionPreference='Continue'
$ProgressPreference='Continue'
$warningPreference='Continue'
<# DEBUG
$ErrorActionPreference='Stop'
#>

# MODULES START
$inp=(get-psrepository | where-object {$_.Name -eq 'PSGallery'}).InstallationPolicy
set-psrepository -name 'PSGallery' -installationpolicy Trusted

# WINDOWS DEFENDER START

# Windows 11 only https://learn.microsoft.com/en-us/powershell/module/defender/set-mppreference?view=windowsserver2022-ps
Set-MpPreference -DisableRealtimeMonitoring $false
Set-MpPreference -AllowDatagramProcessingOnWinServer $true
Set-MpPreference -AllowNetworkProtectionDownLevel $false
Set-MpPreference -AllowNetworkProtectionOnWinServer $true
set-mppreference -AllowSwitchToAsyncInspection $true
# AttackSurfaceReductionOnlyExclusions
# AttackSurfaceReductionRules_Actions
# AttackSurfaceReductionRules_Ids
Set-MpPreference -CheckForSignaturesBeforeRunningScan $true
Set-MpPreference -CloudBlockLevel 0
# CloudExtendedTimeout
# ControlledFolderAccessAllowedApplications
# ControlledFolderAccessProtectedFolders
Set-MpPreference -DisableArchiveScanning $true
Set-MpPreference -DisableAutoExclusions $false
set-MpPreference -DisableBehaviorMonitoring $false
set-MpPreference -DisableBlockAtFirstSeen $false
set-MpPreference -DisableCacheMaintenance $false
set-MpPreference -DisableCatchupFullScan $true
set-MpPreference -DisableCatchupQuickScan $true
set-MpPreference -DisableCpuThrottleOnIdleScans $false
Set-MpPreference -DisableDatagramProcessing $false
Set-MpPreference -DisableDnsOverTcpParsing $false
Set-MpPreference -DisableDnsParsing $false
set-MpPreference -DisableEmailScanning $false
Set-MpPreference -DisableFtpParsing $false
Set-MpPreference -DisableHttpParsing $false
Set-MpPreference -DisableInboundConnectionFiltering $false
set-MpPreference -DisableIOAVProtection $false
Set-MpPreference -DisableNetworkProtectionPerfTelemetry 1
Set-MpPreference -DisableRdpParsing $false
set-MpPreference -DisableRemovableDriveScanning $false
set-MpPreference -DisableRestorePoint $false
Set-MpPreference -DisableScanningMappedNetworkDrivesForFullScan $true
Set-MpPreference -DisableScanningNetworkFiles $false
Set-MpPreference -DisableScriptScanning $false
Set-MpPreference -DisableSmtpParsing 0
Set-MpPreference -DisableSshParsing $false
Set-MpPreference -DisableTlsParsing $false
Set-MpPreference -EnableControlledFolderAccess 0
Set-MpPreference -EnableConvertWarnToBlock 1
Set-MpPreference -EnableDnsSinkhole $false
Set-MpPreference -EnableFileHashComputation $true
Set-MpPreference -EnableFullScanOnBatteryPower $true
Set-MpPreference -EnableLowCpuPriority $true
Set-MpPreference -EngineUpdatesChannel 'Broad'
# ExclusionExtension
# ExclusionIpAddress
# ForceUseProxyOnly
Set-MpPreference -HighThreatDefaultAction 'Block'
Set-MpPreference -IntelTDTEnabled 0
Set-MpPreference -LowThreatDefaultAction 'Block'
Set-MpPreference -MAPSReporting 0
Set-MpPreference -ModerateThreatDefaultAction 'Block'
Set-MpPreference -OobeEnableRtpAndSigUpdate 0
# Dev Drive protection https://learn.microsoft.com/en-us/defender-endpoint/microsoft-defender-endpoint-antivirus-performance-mode#powershell
set-mppreference -performancemodestatus Disabled
Set-MpPreference -PlatformUpdatesChannel 'Broad'
# ProxyBypass
# ProxyPacUrl
# ProxyServer
Set-MpPreference -PUAProtection 2
Set-MpPreference -QuarantinePurgeItemsAfterDelay 0
Set-MpPreference -RandomizeScheduleTaskTimes $false
Set-MpPreference -RealTimeScanDirection 0
Set-MpPreference -RemediationScheduleDay 0
Set-MpPreference -RemediationScheduleTime '21:00:00'
Set-MpPreference -ScanAvgCPULoadFactor 5
Set-MpPreference -ScanParameters 1
set-MpPreference -ScanPurgeItemsAfterDelay 15
set-MpPreference -ScanScheduleDay 8
set-MpPreference -ScanScheduleOffset 1260
set-MpPreference -ScanScheduleQuickScanTime 1260
set-MpPreference -ScanScheduleTime 1260
set-MpPreference -SchedulerRandomizationTime 0
Set-MpPreference -ServiceHealthReportInterval 0
Set-MpPreference -SevereThreatDefaultAction 'Quarantine'
# SharedSignaturesPath
Set-MpPreference -SignatureAuGracePeriod 120
# SignatureBlobFileSharesSources
# SignatureBlobUpdateInternval
# SignatureDefinitionUpdateFileSharesSources
Set-MpPreference -SignatureDisableUpdateOnStartupWithoutEngine $false
# SignatureFallbackOrder
Set-MpPreference -SignatureScheduleDay 0
Set-MpPreference -SignatureScheduleTime '21:00:00'
Set-MpPreference -SignatureUpdateInterval 0
Set-MpPreference -SubmitSamplesConsent 2
Set-MpPreference -ThrottleForScheduledScanOnly 1
Set-MpPreference -UILockdown $false
Set-MpPreference -UnknownThreatDefaultAction 'Block'

add-mppreference -exclusionpath "$env:ProgramFiles\AutoHotkey"
add-mppreference -exclusionprocess "$env:ProgramFiles\AutoHotkey\AutoHotkey.exe"

add-mppreference -exclusionpath "$env:ProgramFiles\AutoHotkey"
add-mppreference -exclusionprocess "$env:ProgramFiles\AutoHotkey\AutoHotkeyU64.exe"

add-mppreference -exclusionpath "${env:ProgramFiles(x86)}\KeePass Password Safe 2"
add-mppreference -exclusionprocess "${env:ProgramFiles(x86)}\KeePass Password Safe 2\KeePass.exe"

add-mppreference -exclusionpath "$env:systemdrive\Traders Elite Pro CMEG_x64"
add-mppreference -exclusionprocess "$env:systemdrive\Traders Elite Pro CMEG_x64\AutoUpdateClient.exe"
add-mppreference -exclusionprocess "$env:systemdrive\Traders Elite Pro CMEG_x64\DasTrader64.exe"
add-mppreference -exclusionprocess "$env:systemdrive\Traders Elite Pro CMEG_x64\UpdateClient.exe"

add-mppreference -exclusionpath "$env:windir\System32\dwm.exe"
add-mppreference -exclusionprocess "$env:windir\System32\dwm.exe"

$nvdc=Get-ChildItem -Path "$env:windir\System32\DriverStore" -Recurse -Filter 'NVDisplay.Container.exe'
if($null -ne $nvdc){
    $nvdcpath=$nvdc.DirectoryName
    add-mppreference -exclusionpath $nvdcpath
    $nvdcpro=-join($nvdcpath,'\NVDisplay.Container.exe')
    add-mppreference -exclusionprocess $nvdcpro
}

$nvxd=Get-ChildItem -Path "$env:windir\System32\DriverStore" -Recurse -Filter 'nvxdapix.dll'
if($null -ne $nvxd){
    $nvxdpath=$nvxd.DirectoryName
    add-mppreference -exclusionpath $nvxdpath
}

$nvwmi=Get-ChildItem -Path "$env:windir\System32\DriverStore" -Recurse -Filter 'nvWmi64.exe'
if($null -ne $nvwmi){
    foreach($entry in $nvwmi){
        $nvwmipath=$entry.DirectoryName
        add-mppreference -exclusionpath $nvwmipath
        $nvwmipro=-join($nvwmipath,'\nvWmi64.exe')
        add-mppreference -exclusionprocess $nvwmipro
    }
}

$nvd3=Get-ChildItem -Path "$env:windir\System32\DriverStore" -Recurse -Filter 'nvd3dumx_cfg.dll'
if($null -ne $nvd3){
    foreach($entry in $nvd3){
        $nvwmipath=$entry.DirectoryName
        add-mppreference -exclusionpath $nvwmipath
        $nvwmipro=-join($nvwmipath,'\nvd3dumx_cfg.dll')
        add-mppreference -exclusionprocess $nvwmipro
    }
}

add-mppreference -exclusionprocess "$env:systemroot\regedit.exe"

add-mppreference -exclusionpath "$env:ProgramFiles\Trade-Ideas\Trade-Ideas Pro AI"
add-mppreference -exclusionprocess "$env:ProgramFiles\Trade-Ideas\Trade-Ideas Pro AI\TIPro.exe"
add-mppreference -exclusionprocess "$env:ProgramFiles\Trade-Ideas\Trade-Ideas Pro AI\CefSharp.BrowserSubprocess.exe"

<# Disable after otherwise will hit error:
Set-MpPreference : Operation failed with the following error: 0x800106ba
#>
Set-MpPreference -DisableRealtimeMonitoring $true

# WINDOWS DEFENDER END

# WINDOWS FIREWALL START

$fwrules=get-netfirewallrule

# BLOCKS
foreach($rule in $fwrules){
    if(
    $rule.DisplayName -match 'ClipChamp\.Clipchamp' -or
    $rule.DisplayName -match 'Microsoft\.BingNews' -or
    $rule.DisplayName -match 'Microsoft\.BingWeather' -or
    $rule.DisplayName -match 'Microsoft\.MicrosoftStickyNotes' -or
    $rule.DisplayName -match 'Microsoft\.Todos' -or
    $rule.DisplayName -match 'Microsoft\.WindowsAlarms' -or
    $rule.DisplayName -match 'Microsoft\.WindowsCalculator' -or
    $rule.DisplayName -match 'Microsoft\.WindowsMaps' -or
    $rule.DisplayName -match 'Microsoft\.WindowsSoundRecorder' -or
    $rule.DisplayName -match 'Microsoft\.XboxIdentityProvider' -or
    $rule.DisplayName -match 'Microsoft\.ZuneMusic' -or
    $rule.DisplayName -match 'Microsoft\.ZuneVideo' -or
    $rule.DisplayName -eq 'Connected User Experiences and Telemetry' -or
    $rule.DisplayName -eq 'Game Bar' -or
    $rule.DisplayName -eq 'Solitaire & Casual Games' -or
    $rule.DisplayName -eq 'Windows Default Lock Screen' -or
    $rule.DisplayName -eq 'Xbox' -or
    $rule.DisplayName -eq 'Xbox Game Bar Plugin' -or
    $rule.DisplayName -eq 'Xbox Game UI' -or
    $rule.DisplayName -eq 'Xbox TCUI'
    ){
        $rule | set-netfirewallrule -action block
    }
}

# CMEG+DAS
$rulechk=$fwrules | where-object {$_.DisplayName -eq 'CMEGDAS64'}
if($null -eq $rulechk){
    new-netfirewallrule -action allow -displayname 'CMEGDAS64' -direction outbound -group 'TRADING' -profile Any -program '%SYSTEMDRIVE%\Traders Elite Pro CMEG_x64\DasTrader64.exe'
}

# Cortana https://learn.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services#21-cortana-and-search-group-policies
if($null -eq ($fwrules | where-object {$_.DisplayName -eq 'Cortana firewall configuration'})){
    new-netfirewallrule -action block -displayname 'Cortana firewall configuration' -direction outbound -group 'CUSTOM' -localport Any -profile Any -program '%SystemRoot%\SystemApps\MicrosoftWindows.Client.CBS_cw5n1h2txyewy\SearchHost.exe' -protocol TCP -RemotePort Any
}

# TI
$rulechk=$fwrules | where-object {$_.DisplayName -eq 'TI'}
if($null -eq $rulechk){
    new-netfirewallrule -action allow -displayname 'TI' -direction outbound -group 'TRADING' -localport Any -profile Any -program '%ProgramFiles%\Trade-Ideas\Trade-Ideas Pro AI\TIPro.exe'
    new-netfirewallrule -action allow -displayname 'TI' -direction outbound -group 'TRADING' -localport Any -profile Any -program '%ProgramFiles%\Trade-Ideas\Trade-Ideas Pro AI\CefSharp.BrowserSubprocess.exe'
}

# Microsoft Compatibility Telemetry
if($null -eq ($fwrules | where-object {$_.DisplayName -eq 'Microsoft Compatibility Telemetry'})){
    new-netfirewallrule -action block -displayname 'Microsoft Compatibility Telemetry' -direction outbound -group 'CUSTOM' -localport Any -profile Any -program '%SystemRoot%\system32\compattelrunner.exe' -protocol Any -RemotePort Any
    new-netfirewallrule -action block -displayname 'Microsoft Compatibility Telemetry' -direction inbound -group 'CUSTOM' -localport Any -profile Any -program '%SystemRoot%\system32\compattelrunner.exe' -protocol Any -RemotePort Any
}

# WINDOWS FIREWALL END

# ENVIRONMENT VARIABLES MACHINE START

# Powershell Telemetry Disable (this should be designed as opt-in not opt-out) https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_telemetry
[Environment]::SetEnvironmentVariable('POWERSHELL_TELEMETRY_OPTOUT','1','Machine')
# .NET Core Telemetry Disable (this should be designed as opt-in not opt-out) https://docs.microsoft.com/en-us/dotnet/core/tools/telemetry#collected-options
[Environment]::SetEnvironmentVariable('DOTNET_CLI_TELEMETRY_OPTOUT','1','Machine')

# ENVIRONMENT VARIABLES MACHINE END

# REMOTE DESKTOP CONNECTION START

$rdchk=(Get-WindowsEdition -online).edition
if($rdchk -ne 'Home'){
    Set-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server' -Name fDenyTSConnections -Value 0
    # Namespace Win32_TSGeneralSetting requires at least Windows 11 Pro
    $instance=get-ciminstance -classname Win32_TSGeneralSetting -Namespace root\cimv2\terminalservices -filter "TerminalName='RDP-tcp'"
    invoke-cimmethod -inputobject $instance -methodname setuserauthenticationrequired -arguments @{UserAuthenticationRequired=1}
    get-netfirewallrule -DisplayGroup 'Remote Desktop' | Enable-NetFirewallRule
    set-netfirewallrule -displayGroup 'Remote Desktop' -action allow
}else{
    write-host 'USING WINDOWS HOME UPGRADE'
}

# REMOTE DESKTOP CONNECTION END

# POWER SETTINGS START

<# This used to work but was removed?!
$p_s=(get-ciminstance -namespace root\cimv2\power -Class win32_PowerPlan -Filter "ElementName='High Performance'")
$null=Invoke-CimMethod -InputObject $p_s -method Activate
#>
powercfg.exe /setactive 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c
$plan_s=get-ciminstance -Class win32_powerplan -Namespace root\cimv2\power -Filter "isActive='true'"
$regex=[regex]"{(.*?)}$"
$planGuid_s=$regex.Match($plan_s.instanceID.Tostring()).groups[1].value
# Processor performance core parking utility distribution disable
Powercfg -setacvalueindex $planGuid_s sub_processor DISTRIBUTEUTIL 0
# Processor performance core parking max cores All
Powercfg -setacvalueindex $planGuid_s sub_processor CPMAXCORES 100
# Processor performance core parking min cores Disable
Powercfg -setacvalueindex $planGuid_s sub_processor CPMINCORES 100
# Maximum Processor State on AC 100%
powercfg /setacvalueindex $planGuid_s SUB_PROCESSOR PROCTHROTTLEMAX 100
# Minimum Processor State 100%
powercfg /setacvalueindex $planGuid_s SUB_PROCESSOR PROCTHROTTLEMIN 100
# Adaptive Brightness Disable
powercfg /setacvalueindex $planGuid_s SUB_VIDEO ADAPTBRIGHT 0
# Allow Wake Timers on AC Disable
powercfg /setacvalueindex $planGuid_s SUB_SLEEP RTCWAKE 0
# Allow Wake Timers on battery Disable
powercfg /setdcvalueindex $planGuid_s SUB_SLEEP RTCWAKE 0
# Critical battery action when on AC Disable 
powercfg /setacvalueindex $planGuid_s SUB_BATTERY BATACTIONCRIT 0
# Hard Disk timeout on AC Disable
powercfg /change disk-timeout-ac 0
# Hibernate on AC Disable
powercfg /change hibernate-timeout-ac 0
# PCIe Link state Power Management on AC Disable
powercfg /setacvalueindex $planGuid_s SUB_PCIEXPRESS ASPM 0
# Suspend on AC Disable
powercfg /change standby-timeout-ac 0
# USB Selective Suspend on AC Disable
powercfg /setacvalueindex $planGuid_s 2a737441-1930-4402-8d77-b2bebba308a3 48e6b7a6-50f5-4782-a5d4-53bb8f07e226 0
# Wireless Adapter Settings power management Disable
powercfg /setacvalueindex $planGuid_s 19cbb8fa-5279-450e-9fac-8a3d5fedd0c1 12bbebe6-58d6-4636-95bb-3217ef867c1a 0
# Critical battery action when on DC/battery Enable
powercfg /setdcvalueindex $planGuid_s SUB_BATTERY BATACTIONCRIT 2
# System Cooling Policy on AC Enable
powercfg /setacvalueindex $planGuid_s SUB_PROCESSOR SYSCOOLPOL 1
# Video Playback Performance bias Max
powercfg /setacvalueindex $planGuid_s 9596fb26-9850-41fd-ac3e-f7c3c00afd4b 10778347-1370-4ee0-8bbd-33bdacaade49 1
# AC Turn Off Display after 10 Minutes
powercfg /change monitor-timeout-ac 10
# DC Battery Turn Off Display after 10 Minutes
powercfg /change monitor-timeout-dc 10
# When playing video
powercfg /setacvalueindex $planGuid_s 9596fb26-9850-41fd-ac3e-f7c3c00afd4b 34c7b99f-9a6d-4b3c-8dc7-b6693b78cef4 0
powercfg /setacvalueindex $planGuid_s SUB_BATTERY BATACTIONLOW 0
powercfg /setacvalueindex $planGuid_s SUB_BATTERY BATFLAGSLOW 1
powercfg /setacvalueindex $planGuid_s SUB_BATTERY BATLEVELCRIT 5
powercfg /setacvalueindex $planGuid_s SUB_BATTERY BATLEVELLOW 10
powercfg /setacvalueindex $planGuid_s SUB_BATTERY f3c5027d-cd16-4930-aa6b-90db844a8f00 7
powercfg /setdcvalueindex $planGuid_s SUB_BATTERY BATACTIONLOW 0
powercfg /setdcvalueindex $planGuid_s SUB_BATTERY BATFLAGSLOW 1
powercfg /setdcvalueindex $planGuid_s SUB_BATTERY BATLEVELCRIT 5
powercfg /setdcvalueindex $planGuid_s SUB_BATTERY BATLEVELLOW 10
powercfg /setdcvalueindex $planGuid_s SUB_BATTERY f3c5027d-cd16-4930-aa6b-90db844a8f00 7

# POWER SETTINGS END

# REGISTRY HKLM START

<# 
# Camera breaks
New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy'
Set-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy" -Name LetAppsAccessCamera -Value 2

# Microphone breaks
New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy'
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsAccessMicrophone -Value 2

# Microsoft Account supposedly breaks getting feature updates
set-itemproperty -path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System' -Name "NoConnectedUser" -Value 3
set-itemproperty -path 'HKLM:\System\CurrentControlSet\Services\wlidsvc' -Name "Start" -Value 4

# Lockscreen Customization Windows 10 https://learn.microsoft.com/en-us/troubleshoot/windows-client/shell-experience/manage-lock-screen-image

$wls=1920
$hls=1200
$pathls="$env:windir\web\screen\lockscreen.png"
$bmp=new-object System.Drawing.Bitmap($wls,$hls)
$cls=[System.Drawing.Color]::Black

for($x=0;$x -lt $width;$x++){
    for($y=0;$y -lt $hls;$y++){
        $bmp.setpixel($x,$y,$cls)
    }
}
$bmp.save($pathls,[System.Drawing.Imaging.Imageformat]::Png)
$bmp.Dispose()

# TESTING FAILED

copy-item -path "$env:windir\web\screen\img103.png" -destination "$env:windir\web\screen\img103.png.bak" -force
$acl=get-acl -path "$env:windir\web\screen\img103.png"
# DEBUG get-acl
set-acl -path "$env:windir\web\screen\lockscreen.png" -aclobject $acl

psexec.exe -i -s cmd.exe /c copy "$env:windir\web\screen\lockscreen.png" "$env:windir\web\screen\img103.png"
    # Nice try but exit code 1

copy-item -path "$env:windir\web\screen\lockscreen.png" -destination "$env:windir\web\screen\img103.png" -force

# TESTING FAILED Windows 10 Pro set these keys

New-Item -Path 'HKLM:\SOFTWARE\WOW6432Node\Policies\Microsoft\Windows\Personalization'
Set-ItemProperty -Path 'HKLM:\SOFTWARE\WOW6432Node\Policies\Microsoft\Windows\Personalization' -Name LockScreenImage -Value "$env:windir\web\screen\lockscreen.png"
Set-ItemProperty -Path 'HKLM:\SOFTWARE\WOW6432Node\Policies\Microsoft\Windows\Personalization' -Name NoLockScreen -Value 0

# TESTING FAILED

New-Item -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\PersonalizationCSP'
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\PersonalizationCSP' -Name LockScreenImage -Value "$env:windir\web\screen\lockscreen.png"
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\PersonalizationCSP' -Name LockScreenImageUrl -Value "$env:windir\web\screen\lockscreen.png"
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\PersonalizationCSP' -Name LockScreenImageStatus -Value 1

# FAILED
New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Personalization'
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Personalization' -Name LockScreenImage -Value "$env:windir\web\screen\lockscreen.png"
# Only greys out UI. 1 = enable lock screen
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Personalization' -Name NoLockScreen -Value 1
# https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-admx-controlpaneldisplay
#Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Personalization' -Name LockScreenOverlaysDisabled -Value 1

# TESTING Windows Defender Tamper Protection
New-Item -Path 'HKLM:\SOFTWARE\Microsoft\Windows Defender\Features'
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows Defender\Features' -Name TamperProtection -Value 0
$gawd=Get-Acl -Path 'HKLM:\SOFTWARE\Microsoft\Windows Defender\Features'
# Set folder permissions then set TamperProtection value

#>

# TODO Review rest of these options
# https://learn.microsoft.com/en-us/windows-server/administration/performance-tuning/hardware/power/power-performance-tuning
# Control Panel > Power Options > Change Plan Settings > Change advanced power settings > Shows options > Processor Power Management > "Processor performance core parking min cores" shows up
Set-ItemProperty -Path 'HKLM:\SYSTEM\ControlSet001\Control\Power\PowerSettings\54533251-82be-4824-96c1-47b60b740d00\0cc5b647-c1df-4637-891a-dec35c318583' -Name Attributes -Value 0

# https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-applicationmanagement#allowautomaticapparchiving
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\Appx' -Name AllowAutomaticAppArchiving -Value 0

# Control Panel > Sound > Sounds tab > Play Windows Startup sound uncheck
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI\BootAnimation' -Name DisableStartupSound -Value 1
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\EditionOverrides' -Name 'UserSetting_DisableStartupSound' -Value 1

# Windows Update Limit https://www.tenforums.com/tutorials/88607-limit-bandwidth-windows-update-store-app-updates-windows-10-a.html#option2

# Widgets disable https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-newsandinterests
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Dsh') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Dsh'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Dsh' -Name AllowNewsAndInterests -Value 0

# Active hours https://learn.microsoft.com/en-us/windows/deployment/update/waas-restart#registry-keys-used-to-manage-restart
if((test-path 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate') -eq $false){
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate'
}
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate' -Name SetActiveHours -Value 1
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate' -Name ActiveHoursEnd -Value 21
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate' -Name ActiveHoursStart -Value 3
if((test-path 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU') -eq $false){
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
}
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU' -Name NoAutoRebootWithLoggedOnUsers -Value 1

# Activity History https://learn.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services#bkmk-act-history
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\System' -Name EnableActivityFeed -Value 0
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\System' -Name PublishUserActivities -Value 0
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\System' -Name UploadUserActivities -Value 0

# Visual Studio Customer Experience Improvement Program https://learn.microsoft.com/en-us/visualstudio/ide/visual-studio-experience-improvement-program?view=vs-2022
if((test-path 'HKLM:\Software\Wow6432Node\Microsoft\VSCommon\17.0\SQM') -eq $false){
    New-Item -Path 'HKLM:\Software\Wow6432Node\Microsoft\VSCommon\17.0\SQM'
}
Set-ItemProperty -Path 'HKLM:\Software\Wow6432Node\Microsoft\VSCommon\17.0\SQM' -Name OptIn -Value 0
if((test-path 'HKLM:\Software\Policies\Microsoft\VisualStudio') -eq $false){
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\VisualStudio'
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\VisualStudio\SQM'
}
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\VisualStudio\SQM' -Name OptIn -Value 0

<# Windows 11 Start Menu https://learn.microsoft.com/en-us/windows/configuration/customize-start-menu-layout-windows-11
# NOT ACTUALLY WORKING
if($osv_1.major -eq 10 -and $osv_1.Build -gt 22000){
    $layout='{"pinnedList":[{"desktopAppLink":"%ALLUSERSPROFILE%\\Microsoft\\Windows\\Start Menu\\Programs\\Microsoft Edge.lnk"},{"packagedAppId":"Microsoft.WindowsStore_8wekyb3d8bbwe!App"},{"packagedAppId":"Microsoft.Windows.Photos_8wekyb3d8bbwe!App"},{"packagedAppId":"windows.immersivecontrolpanel_cw5n1h2txyewy!microsoft.windows.immersivecontrolpanel"},{"packagedAppId":"Microsoft.WindowsCalculator_8wekyb3d8bbwe!App"},{"packagedAppId":"Microsoft.WindowsNotepad_8wekyb3d8bbwe!App"},{"packagedAppId":"Microsoft.Paint_8wekyb3d8bbwe!App"},{"desktopAppLink":"%APPDATA%\\Microsoft\\Windows\\Start Menu\\Programs\\File Explorer.lnk"}]}'
    if((test-path 'HKLM:\SOFTWARE\Microsoft\PolicyManager\current\device\Start') -eq $false){
        New-Item -Path 'HKLM:\SOFTWARE\Microsoft\PolicyManager\current\device\Start'
    }
    Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\PolicyManager\current\device\Start' -Name ConfigureStartPins -Value $layout
}
#>

<# BITS https://docs.microsoft.com/en-us/windows/win32/bits/group-policies
New-Item -Path 'HKLM:\Software\Policies\Microsoft\Windows\BITS'
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\BITS' -Name EnableBITSMaxBandwidth -Value 1
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\BITS' -Name MaxBandwidthValidFrom -Value 4
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\BITS' -Name MaxBandwidthValidTo -Value 20
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\BITS' -Name MaxTransferRateOffSchedule -Value 10000
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\BITS' -Name MaxTransferRateOnSchedule -Value 0
#>

<# DEFAULTS
get-netoffloadglobalsetting
ReceiveSideScaling           : Enabled
ReceiveSegmentCoalescing     : Enabled
Chimney                      : Disabled
TaskOffload                  : Enabled
NetworkDirect                : Enabled
NetworkDirectAcrossIPSubnets : Blocked
PacketCoalescingFilter       : Enabled
#>

set-netoffloadglobalsetting -chimney disabled -receivesegmentcoalescing disabled -receivesidescaling disabled -packetcoalescingfilter disabled -TaskOffload disabled

<# DEFAULTS
get-nettcpsetting -settingname 'Internet'
SettingName                     : Internet
MinRto(ms)                      : 300
InitialCongestionWindow(MSS)    : 10
CongestionProvider              : CUBIC
CwndRestart                     : False
DelayedAckTimeout(ms)           : 40
DelayedAckFrequency             : 2
MemoryPressureProtection        : Enabled
AutoTuningLevelLocal            : Normal
AutoTuningLevelGroupPolicy      : NotConfigured
AutoTuningLevelEffective        : Local
EcnCapability                   : Disabled
Timestamps                      : Allowed
InitialRto(ms)                  : 1000
ScalingHeuristics               : Disabled
DynamicPortRangeStartPort       : 49152
DynamicPortRangeNumberOfPorts   : 16384
AutomaticUseCustom              : Disabled
NonSackRttResiliency            : Disabled
ForceWS                         : Enabled
MaxSynRetransmissions           : 4
AutoReusePortRangeStartPort     : 0
AutoReusePortRangeNumberOfPorts : 0
#>
set-nettcpsetting -settingname 'Internet' -autotuninglevellocal Normal -ecncapability Disabled -memorypressureprotection Disabled -nonsackrttresiliency Disabled -timestamps Disabled

<#
$gntf=get-nettransportfilter
if($gntf.settingName -ne 'InternetCustom'){
    # Server SKUs only
    # new-nettransportfilter -settingname 'CUSTOM' -protocol 'TCP' -LocalPortStart '0' -LocalPortEnd '65535' -RemotePortEnd '0' -RemotePortstart '65535' -destinationprefix '*'
}
#>

<# DEFAULTS
get-netipv4protocol
DefaultHopLimit             : 128
NeighborCacheLimit(Entries) : 256
RouteCacheLimit(Entries)    : 4096
ReassemblyLimit(Bytes)      : 534320992
IcmpRedirects               : Enabled
SourceRoutingBehavior       : DontForward
DhcpMediaSense              : Enabled
MediaSenseEventLog          : Disabled
IGMPLevel                   : All
IGMPVersion                 : Version3
MulticastForwarding         : Disabled
GroupForwardedFragments     : Disabled
RandomizeIdentifiers        : Enabled
AddressMaskReply            : Disabled
MinimumMtu(Bytes)           : 576
DeadGatewayDetection        : Enabled
MultipleArpAnnouncements    : Enabled
#>
set-netipv4protocol -igmpversion 'Version3'

# Pacing Profile https://techcommunity.microsoft.com/blog/networkingblog/algorithmic-improvements-boost-tcp-performance-on-the-internet/2347061

# LLIPorts
$dcports=('5094','9510')
$mdbport=('3306')
$pgsport=('5432')
# RDP https://learn.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/change-listening-port
$rdpports=('3389')
# Steam https://help.steampowered.com/en/faqs/view/2EA8-4D75-DA21-31EB
$steamports=('80','443','3478','4379','4380','27000-27100')
$tiports=('8844')
$lliports=@()
$lliports+=('5016','7001','7002')
$lliports+=$dcports
$lliports+=$mdbport
$lliports+=$pgsport
$lliports+=$rdpports
$lliports+=$steamports
$lliports+=$tiports

$gna=(get-netadapter).name
if($null -ne $gna){
    foreach($naitem in $gna){
        # Microsoft LLDP Protocol Driver
        # Link-Layer Topology Discovery Mapper I/O Driver
        disable-netadapterbinding -name $naitem -componentid ms_lltdio
        # QoS Packet Scheduler https://learn.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2012-R2-and-2012/jj735303(v=ws.11)
        disable-netadapterbinding -name $naitem -componentid ms_pacer
        # Link-Layer Topology Discovery Responder
        disable-netadapterbinding -name $naitem -componentid ms_rspndr
        # Internet Protocol Version 6 (TCP/IPv6)
        disable-netadapterbinding -name $naitem -componentid ms_tcpip6
    }
}

# Power Management disable all devices
# PnPCapabilities Decimal 24=off not safe assumption for all devices. Do below instead.
$pmchk=Get-CimInstance -Namespace 'root\WMI' -ClassName 'MSPower_DeviceEnable' | Where-Object {$_.Enable -eq $true}
if($null -ne $pmchk){
    foreach($pmitem in $pmchk){
        Set-CimInstance -CimInstance $pmitem -Property @{Enable = $false} -Verbose
    }
}

# Intel(R) Ethernet Server Adapter I210-T1 configure
# https://docs.microsoft.com/en-us/windows-hardware/drivers/network/task-offload
# https://www.intel.com/content/www/us/en/support/articles/000005593/ethernet-products.html
$PhysicalAdapter=get-ciminstance -query "select DeviceID,Name from Win32_NetworkAdapter where name='Intel(R) Ethernet Server Adapter I210-T1'"
if($null -ne $PhysicalAdapter){
    $netadname=(get-netadapter -InterfaceDescription $PhysicalAdapter.Name).Name
    $DeviceID=$PhysicalAdapter.DeviceID
    if($DeviceID.length -eq 1){
        $AdapterDeviceNumber="000"+$DeviceID
    }elseif($DeviceID.length -eq 2){
        $AdapterDeviceNumber="00"+$DeviceID
    }
    $KeyPath="HKLM:\SYSTEM\CurrentControlSet\Control\Class\{4d36e972-e325-11ce-bfc1-08002be10318}\$AdapterDeviceNumber"
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'ARP Offload' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'DMA Coalescing' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Enable PME' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -RegistryKeyword 'EnableLLI' -allproperties -registryvalue 1
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Energy Efficient Ethernet' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Flow Control' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Gigabit Master Slave Mode' -registryvalue 1
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Gigabit PHY Mode' -registryvalue 1
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Interrupt Moderation' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Interrupt Moderation Rate' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'IPv4 Checksum Offload' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Jumbo Packet' -registryvalue 1514
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Large Send Offload V2 (IPv4)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Large Send Offload V2 (IPv6)' -registryvalue 0
    Set-itemproperty -Path $KeyPath -Name 'LLIPorts' -Value $lliports
    # set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Locally Administered Address' -registryvalue '--'
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Log Link State Event' -registryvalue 16
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Maximum Number of RSS Queues' -registryvalue 4
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'NS Offload' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Packet Priority & VLAN' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'PTP Hardware Timestamp' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Receive Buffers' -registryvalue 2048
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Receive Side Scaling' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Reduce Speed On Power Down' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Software Timestamp' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Speed & Duplex' -registryvalue 6
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'TCP Checksum offload (IPv4)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'TCP Checksum offload (IPv6)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Transmit Buffers' -registryvalue 2048
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'UDP Checksum offload (IPv4)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'UDP Checksum offload (IPv6)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Wait for Link' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Wake on link Settings' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Wake on Magic Packet' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Wake on Pattern Match' -registryvalue 0
    restart-netadapter -name $netadname -asjob
}

# Intel(R) Wi-Fi 6 AX200 160MHz
$PhysicalAdapter=get-ciminstance -classname Win32_NetworkAdapter | where-object {$_.Name -eq 'Intel(R) Wi-Fi 6 AX200 160MHz'}
if($null -ne $PhysicalAdapter){
    $netadname=(get-netadapter -InterfaceDescription $PhysicalAdapter.Name).Name
    set-netadapteradvancedproperty -displayname '802.11a/b/g Wireless Mode' -name $netadname -norestart -registryvalue 34
    set-netadapteradvancedproperty -displayname '802.11n/ac/ax Wireless Mode' -name $netadname -norestart -registryvalue 3
    set-netadapteradvancedproperty -displayname 'ARP offload for WoWLAN' -name $netadname -norestart -registryvalue 0
    set-netadapteradvancedproperty -displayname 'Channel Width for 2.4GHz' -name $netadname -norestart -registryvalue 1
    set-netadapteradvancedproperty -displayname 'Channel Width for 5GHz' -name $netadname -norestart -registryvalue 1
    set-netadapteradvancedproperty -displayname 'Fat Channel Intolerant' -name $netadname -norestart -registryvalue 0
    set-netadapteradvancedproperty -displayname 'GTK rekeying for WoWLAN' -name $netadname -norestart -registryvalue 0
    set-netadapteradvancedproperty -displayname 'MIMO Power Save Mode' -name $netadname -norestart -registryvalue 3
    set-netadapteradvancedproperty -displayname 'Mixed Mode Protection' -name $netadname -norestart -registryvalue 0
    set-netadapteradvancedproperty -displayname 'NS offload for WoWLAN' -name $netadname -norestart -registryvalue 0
    set-netadapteradvancedproperty -displayname 'Packet Coalescing' -name $netadname -norestart -registryvalue 0
    set-netadapteradvancedproperty -displayname 'Preferred Band' -name $netadname -norestart -registryvalue 2
    set-netadapteradvancedproperty -displayname 'Roaming Aggressiveness' -name $netadname -norestart -registryvalue 2
    set-netadapteradvancedproperty -displayname 'Sleep on WoWLAN Disconnect' -name $netadname -norestart -registryvalue 0
    set-netadapteradvancedproperty -displayname 'Throughput Booster' -name $netadname -norestart -registryvalue 0
    set-netadapteradvancedproperty -displayname 'Transmit Power' -name $netadname -norestart -registryvalue 100
    set-netadapteradvancedproperty -displayname 'U-APSD support' -name $netadname -norestart -registryvalue 0
    set-netadapteradvancedproperty -displayname 'Wake on Magic Packet' -name $netadname -norestart -registryvalue 0
    set-netadapteradvancedproperty -displayname 'Wake on Pattern Match' -name $netadname -norestart -registryvalue 0
    restart-netadapter -name $netadname -asjob
}

# Myricom
$PhysicalAdapter=get-ciminstance -query "select Name from Win32_NetworkAdapter where name like 'Myricom%'"
if($null -ne $PhysicalAdapter){
    $netadname=(get-netadapter -InterfaceDescription $PhysicalAdapter.Name).Name
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Adaptive Interrupt Moderation (AIM)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'AIM: Max Idle Time (msec)' -registryvalue 10
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'AIM: Max Period (msec)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Custom RSS Hash' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Degraded PCI Express Link' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Flow Control' -registryvalue 0
    # 0-1000
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Interrupt Coalescing Delay' -registryvalue 2
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Large Send Offload v2 (IPv4)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Large Send Offload v2 (IPv6)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Log Link State Event' -registryvalue 1
    #2-16
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Maximum Number of RSS Queues' -registryvalue 4
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'MTU' -registryvalue 9000
    #1024-32768
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Receive Buffers' -registryvalue 2048
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Receive Side Scaling' -registryvalue 1
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Strip VLAN Tags' -registryvalue 1
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'TCP Checksum Offload (IPv4)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'TCP Checksum Offload (IPv6)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'UDP Checksum Offload (IPv4)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'UDP Checksum Offload (IPv6)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'VLAN ID' -registryvalue 0
    restart-netadapter -name $netadname -asjob
}

<# Realtek PCIe Family Controller
Has Advanced EEE
PCI\VEN_10EC&DEV_8168&REV_10

Does not have Advanced EEE
PCI\VEN_10EC&DEV_8168&REV_11
PCI\VEN_10EC&DEV_8168&REV_0C
#>

$PhysicalAdapter=get-ciminstance -classname Win32_NetworkAdapter | where-object {$_.name -eq 'Realtek PCIe GbE Family Controller'}
if($null -ne $PhysicalAdapter){
    $netadname=(get-netadapter -InterfaceDescription $PhysicalAdapter.Name).Name
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Advanced EEE' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'ARP Offload' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Auto Disable Gigabit' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Energy-Efficient Ethernet' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Flow Control' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname "Green Ethernet" -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Gigabit Lite' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Interrupt Moderation' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'IPv4 Checksum Offload' -registryvalue 0
    #set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Jumbo Frame' -registryvalue 1514
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Large Send Offload v2 (IPv4)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Large Send Offload v2 (IPv6)' -registryvalue 0
    # Does not support LLIPorts
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Maximum Number of RSS Queues' -registryvalue 4
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Network Address' -registryvalue '--'
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'NS Offload' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Power Saving Mode' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Priority & VLAN' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Receive Buffers' -registryvalue 512
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Receive Side Scaling' -registryvalue 1
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Shutdown Wake-On-Lan' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Speed & Duplex' -registryvalue 6
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'TCP Checksum offload (IPv4)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'TCP Checksum offload (IPv6)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Transmit Buffers' -registryvalue 128
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'UDP Checksum offload (IPv4)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'UDP Checksum offload (IPv6)' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'VLAN ID' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Wake on Magic Packet' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Wake on pattern match' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'WOL & Shutdown Link Speed' -registryvalue 2
    restart-netadapter -name $netadname -asjob
}

$PhysicalAdapter=get-ciminstance -classname Win32_NetworkAdapter | where-object {$_.name -eq 'Realtek RTL8852BE WiFi 6 802.11ax PCIe Adapter'}
if($null -ne $PhysicalAdapter){
    $netadname=(get-netadapter -InterfaceDescription $PhysicalAdapter.Name).Name
    set-netadapteradvancedproperty -norestart -name $netadname -displayname '2.4G Wireless mode' -registryvalue 45
    set-netadapteradvancedproperty -norestart -name $netadname -displayname '5G Wireless Mode' -registryvalue 58
    set-netadapteradvancedproperty -norestart -name $netadname -displayname '802.11d' -registryvalue 1
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Beacon Interval' -registryvalue 50
    # Does not support LLIPorts
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Mac Randomization' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Multi-Channel Concurrent' -registryvalue 1
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Preamble Mode' -registryvalue 2
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Preferred Band' -registryvalue 2
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Roaming Aggressiveness' -registryvalue 3
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Wake on Magic Packet' -registryvalue 0
    set-netadapteradvancedproperty -norestart -name $netadname -displayname 'Wake on Pattern Match' -registryvalue 0
    restart-netadapter -name $netadname -asjob
}

# Location
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\LocationAndSensors') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\LocationAndSensors'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\LocationAndSensors' -Name DisableLocation -Value 1
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsAccessLocation -Value 2

# Settings > Privacy > Radios > Access to control radios on this device > Off
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\radios' -Name Value -Value Deny

# Account Info
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsAccessAccountInfo -Value 2

# Account Information
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\userAccountInformation' -Name Value -Value Deny

# Account History https://docs.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services#bkmk-priv-ink
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\System' -Name EnableActivityFeed -Value 2
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\System' -Name PublishUserActivities -Value 2
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\System' -Name UploadUserActivities -Value 2

# App diagnostic info access for this device disable
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\appDiagnostics' -Name Value -Value Deny

# App Diagnostics
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsGetDiagnosticInfo -Value 2

# Appointments disable
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\appointments' -Name Value -Value Deny

# Apps for websites
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\System' -Name EnableAppUriHandlers -Value 0

# Calendar
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsAccessCalendar -Value 2

# Call history
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsAccessCallHistory -Value 2

# Chat
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\chat' -Name Value -Value Deny

# Contacts
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsAccessContacts -Value 2

# Defender sending file samples to Microsoft disable
if((test-path 'HKLM:\Software\Policies\Microsoft\Windows Defender\Spynet') -eq $false){
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\Windows Defender\Spynet'
}
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows Defender\Spynet' -Name SubmitSamplesConsent -Value 2

# Device metadata retrieval disable  https://learn.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services#4-device-metadata-retrieval
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Device Metadata') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Device Metadata'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Device Metadata' -Name PreventDeviceMetadataFromNetwork -Value 1

# Location disable
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\location' -Name Value -Value Deny

# Speech, inking, and typing / Disable updates to speech recognition and speech synthesis models https://learn.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\InputPersonalization' -Name RestrictImplicitInkCollection -Value 1
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\InputPersonalization' -Name RestrictImplicitTextCollection -Value 1
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Speech_OneCore\Preferences' -Name ModelDownloadAllowed -Value 0

<# Cortana and Search
https://learn.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services
#>
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search' -Name AllowCortana -Value 0
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search' -Name AllowSearchToUseLocation -Value 0
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search' -Name DisableWebSearch -Value 1
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search' -Name AllowCloudSearch -Value 0
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search' -Name ConnectedSearchUseWeb -Value 0
#https://learn.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services#21-cortana-and-search-group-policies
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search' -Name ConnectedSearchPrivacy -Value 3

<# Search
DisableSearch 0 and DoNotUseWebResults 0 allows Start menu type searching without web apps. Restart Explorer to switch value https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-search#disablesearch
#>
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search' -Name DisableSearch -Value 0
# https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-search#donotusewebresults
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search' -Name DoNotUseWebResults -Value 0

# ?
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\userDataTasks' -Name Value -Value Deny

# Document library access for this device disable
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\contacts' -Name Value -Value Deny

# Edge https://learn.microsoft.com/en-us/deployedge/configure-microsoft-edge#1-download-and-install-the-microsoft-edge-administrative-template

# Disable address bar drop-down list suggestions https://docs.microsoft.com/en-us/microsoft-edge/deploy/group-policies/address-bar-settings-gp

#Edge Books
# Disable Allow a shared books folder
# Disable Allow configuration updates for the Books Library
# Disable full diagnostic data
if((test-path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\BooksLibrary') -eq $false){
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\BooksLibrary'
}
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\BooksLibrary' -name 'AllowConfigurationUpdateForBooksLibrary' -Type DWORD -Value 0
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\BooksLibrary' -name 'EnableExtendedBooksTelemetry' -Type DWORD -Value 0
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\BooksLibrary' -name 'UseSharedFolderForBooks' -Type DWORD -Value 0

# Edge UI
# Disable Allow web content on New Tab page https://docs.microsoft.com/en-us/microsoft-edge/deploy/group-policies/new-tab-page-settings-gp
if((test-path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\ServiceUI') -eq $false){
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\ServiceUI'
}
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\ServiceUI' -name "AllowWebContentOnNewTabPage" -Type DWORD -Value 0
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\ServiceUI' -name "ShowOneBox" -Type DWORD -Value 0

#Edge Disable Configure search suggestions in Address bar
if((test-path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\SearchScopes') -eq $false){
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\SearchScopes'
}

set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\SearchScopes' -name 'ShowSearchSuggestionsGlobal' -Type DWORD -Value 0

#Edge Disable Prelaunch
if((test-path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge') -eq $false){
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge'
}
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge' -name 'AllowPrelaunch' -Type DWORD -Value 0

#Edge Disable show books library regardless of support
#Edge Disable browsing history
#Edge Disable Autofill
#Edge Disable Popups
#Edge Disable Password Manager
#Edge Disable "Do Not Track"
#Edge Enable Prevent Microsoft Edge from gathering Live Tile information when pinning a site to Start
if((test-path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\Main') -eq $false){
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\Main'
}
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\Main' -name 'AllowPopups' -Type String -Value 1
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\Main' -name 'AllowSavingHistory' -Type DWORD -Value 0
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\Main' -name 'AlwaysEnableBooksLibrary' -Type DWORD -Value 0
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\Main' -name 'DoNotTrack' -Type DWORD -Value 0
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\Main' -name 'FormSuggest Passwords' -Type String -Value 0
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\Main' -name 'PreventLiveTileDataCollection' -Type DWORD -Value 1
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\Main' -name 'Use FormSuggest' -Type String -Value 0

#Tab Preloading
if((test-path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\TabPreloader') -eq $false){
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\TabPreloader'
}
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\MicrosoftEdge\TabPreloader' -name 'AllowTabPreloading' -Type DWORD -Value 0

# Do not sync
set-itemproperty -path 'HKLM:\Software\Policies\Microsoft\Windows\SettingSync' -name 'DisableSettingSync' -Type DWORD -Value 2

# Email
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsAccessEmail -Value 2

# Email access for this device disable
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\email' -Name Value -Value Deny

# Find My Device disable https://learn.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services#5-find-my-device
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\FindMyDevice') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\FindMyDevice'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\FindMyDevice' -Name AllowFindMyDevice -Value 0

# Font streaming https://learn.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services#6-font-streaming
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\System' -Name EnableFont -Value 0

# GameDVR and Broadcast User Service disable https://docs.microsoft.com/en-us/windows-hardware/drivers/install/hklm-system-currentcontrolset-services-registry-tree
# https://docs.microsoft.com/en-us/windows/application-management/per-user-services-in-windows
$gp=get-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\BcastDVRUserServic*"
$gpp=$gp.pspath
foreach($i in $gpp){
    Set-ItemProperty -Path $i -Name Start -Value 4
}

<# Stop Event Viewer error 10010 Windows.Gaming.GameBar.Internal.PresenceWriterServer The server Windows.Gaming.GameBar.PresenceServer.Internal.PresenceWriter did not register with DCOM within the required timeout.
WORKS
1) Setting owner and fullcontrol of key:
'HKLM:\SOFTWARE\Microsoft\WindowsRuntime\ActivatableClassId\Windows.Gaming.GameBar.PresenceServer.Internal.PresenceWriter'
and subkeys to something else temporarily via regedit, making change and restoring to original configuration.
FAILED 
1)
As admin "Requested registry access is not allowed"
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\WindowsRuntime\ActivatableClassId\Windows.Gaming.GameBar.PresenceServer.Internal.PresenceWriter' -name ActivationType -value 0 -force
2)
invoke-psexec -computername "$Env:Computername" -command "Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\WindowsRuntime\ActivatableClassId\Windows.Gaming.GameBar.PresenceServer.Internal.PresenceWriter' -name ActivationType -value 0 -force" -CustomPsExecParameters "-i -s" -IsLongPSCommand -continueonpingfail
3)
install-module -name ntobjectmanager
start-service -name TrustedInstaller
$parent = Get-NtProcess -ServiceName TrustedInstaller
$proc = New-Win32Process powershell.exe -CreationFlags NewConsole -ParentProcess $parent
$acl = Get-Acl -Path 'HKLM:\SOFTWARE\Microsoft\WindowsRuntime\ActivatableClassId\Windows.Gaming.GameBar.PresenceServer.Internal.PresenceWriter'
$temprule= New-Object System.Security.AccessControl.RegistryAccessRule(
    "SYSTEM", 
    "FullControl", 
    "Allow"
)
$acl.SetAccessRule($temprule)
Set-Acl -Path $regKeyPath -AclObject $acl
4)
https://www.tiraniddo.dev/2017/08/the-art-of-becoming-trustedinstaller.html
$regKeyPath='HKLM:\SOFTWARE\Microsoft\WindowsRuntime\ActivatableClassId\Windows.Gaming.GameBar.PresenceServer.Internal.PresenceWriter'
$newOwner ='NT AUTHORITY\SYSTEM'
$acl = Get-Acl -Path $regKeyPath
$owner = New-Object System.Security.Principal.NTAccount($newOwner)
$acl.SetOwner($owner)
Set-Acl -Path $regKeyPath -AclObject $acl
#>

# Windows Update > Advanced options > Delivery Optimization
set-DODownloadmode -downloadmode cdnonly

# Let apps run in the background
# https://learn.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsRunInBackground -Value 2

# Let apps use advertising ID to make ads more interesting to you based on your app usage disable
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AdvertisingInfo') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AdvertisingInfo'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AdvertisingInfo' -Name DisabledByGroupPolicy -Value 1
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AdvertisingInfo' -Name Enabled -Value 0

# Login background disable
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\System' -Name DisableLogonBackgroundImage -Value 1

# Mail synchronization disable
if((test-path 'HKLM:\Software\Policies\Microsoft\Windows Mail') -eq $false){
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\Windows Mail'
}
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows Mail' -Name ManualLaunchAllowed -Value 0

# Malicious Software Reporting Tool diagnostic data disable https://support.microsoft.com/en-us/help/891716/deploy-windows-malicious-software-removal-tool-in-an-enterprise-enviro
if((test-path 'HKLM:\Software\Policies\Microsoft\MRT') -eq $false){
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\MRT'
}
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\MRT' -Name DontReportInfectionInformation -Value 1

# Message Sync
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Messaging') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Messaging'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Messaging' -Name AllowMessageSync -Value 0

# Messaging
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsAccessMessaging -Value 2

# Microsoft Update Health Tools stop installing
if((test-path 'HKLM:\SOFTWARE\Microsoft\PCHC') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Microsoft\PCHC'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\PCHC' -name PreviousUninstall -value 1

# Motion
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsAccessMotion -Value 2

# Microsoft Store stop auto install of junkware apps without consent https://learn.microsoft.com/en-us/windows/msix/group-policy-msix
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\WindowsStore') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\WindowsStore'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\WindowsStore' -Name AutoDownload -Value 2

# Network Connection Status Indicator disable
# Set-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\NetworkConnectivityStatusIndicator" -Name NoActiveProbe -Value 1

# Notification https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-privacy
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsAccessNotifications -Value 2

# Notifications disable https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-notifications
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications' -Name NoCloudApplicationNotification -Value 1

# Offline maps disable
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Maps') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Maps'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Maps' -Name AllowUntriggeredNetworkTrafficOnSettingsPage -Value 0
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Maps' -Name AutoDownloadAndUpdateMapData -Value 0

# OneDrive disable
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\OneDrive') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\OneDrive'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\OneDrive' -Name DisableFileSyncNGSC -Value 1
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\OneDrive' -Name PreventNetworkTrafficPreUserSignIn -Value 1

# Other devices
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsSyncWithDevices -Value 2

# Phone call
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsAccessPhone -Value 2

# Pictures library access for this device disable
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\picturesLibrary' -Name Value -Value Deny

# Radios
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsAccessRadio -Value 2

# Sync your settings
if((test-path 'HKLM:\Software\Policies\Microsoft\Windows\SettingSync') -eq $false){
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\Windows\SettingSync'
}
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\SettingSync' -Name DisableSettingSync -Value 2
Set-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Windows\SettingSync' -Name DisableSettingSyncUserOverride -Value 1

# Policy CSP - Experience https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-experience
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent'
}
# https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-experience#allowcortana
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent' -Name AllowCortana -Value 0
# https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-experience#allowfindmydevice
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent' -Name AllowFindMyDevice -Value 0

# https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-experience#disablecloudoptimizedcontent
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent' -Name DisableCloudOptimizedContent -Value 1
# https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-experience#disableconsumeraccountstatecontent
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent' -Name  	DisableConsumerAccountStateContent -Value 1
# https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-experience#allowwindowstips
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent' -Name DisableSoftLanding -Value 0
# https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-experience#allowwindowsconsumerfeatures
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent' -Name DisableWindowsConsumerFeatures -Value 0


# Storage Health https://learn.microsoft.com/en-us/windows/client-management/mdm/policy-csp-storage#storage-allowdiskhealthmodelupdates
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\StorageHealth') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\StorageHealth'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\StorageHealth' -Name 'AllowDiskHealthModelUpdates' -Value 0

# Tasks
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsAccessTasks -Value 2

# Telemetry disable
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\DataCollection' -Name AllowTelemetry -Value 0

#Telemetry found pre-existing disable
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\DataCollection' -Name AllowTelemetry -Value 0

# Videos Library access disable
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\videosLibrary' -Name Value -Value Deny

# Voice Activation
# https://learn.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services#1823-voice-activation
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsActivateWithVoice -Value 2
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\AppPrivacy' -Name LetAppsActivateWithVoiceAboveLock -Value 2

# Windows Defender
# SmartScreen Disable
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\System' -Name EnableSmartScreen -Value 0
if((test-path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender\SmartScreen') -eq $false){
    New-Item -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender\SmartScreen'
}
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender\SmartScreen' -Name ConfigureAppInstallControl -Value Anywhere
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender\SmartScreen' -Name ConfigureAppInstallControlEnabled -Value 1

# Memory Isolation https://learn.microsoft.com/en-us/windows/security/hardware-security/enable-virtualization-based-protection-of-code-integrity#use-registry-keys-to-enable-memory-integrity
Set-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\DeviceGuard\Scenarios\HypervisorEnforcedCodeIntegrity' -Name Enabled -Value 1
# To gray out the memory integrity UI and display the message "This setting is managed by your administrator"
#remove-item -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\DeviceGuard\Scenarios\HypervisorEnforcedCodeIntegrity' -Name Enabled -Value 1

# Recent activity and scan results disable
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows Defender Security Center\Virus and threat protection\' -Name SummaryNotificationDisabled -Value 1

# Windows should ask for my feedback disable
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\DataCollection' -Name DoNotShowFeedbackNotifications -Value 1

# Audio Power Management test disable to avoid first second of audio not being heard
$aregpath='HKLM:\SYSTEM\CurrentControlSet\Control\Class\'
$akeys=Get-ChildItem -Path $aregpath
$apbytes=[byte[]](0x00, 0x00, 0x00, 0x00)
foreach($akey in $akeys){
    $aclass=(get-itemproperty $akey.pspath).Class
    if($aclass -eq 'MEDIA'){
        # This will error on Properties despite being Administrator but will still add all other subkeys
        $asubkeys=get-childitem $akey.pspath -ea 0
        foreach($asubkey in $asubkeys){
            $pschk=-join('Registry::',$asubkey.Name,'\PowerSettings')
            <# NVIDIA High Definition Audio defaults
            04 00 00 00
            03 00 00 00
            04 00 00 00
            Cirrus Logic Superior High Definition Audio defaults
            1e 00 00 00
            03 00 00 00
            00 00 00 00
            #>
            if((test-path -path $pschk) -eq $true){
                $citchk=$null
                $citchk=(get-itemproperty -path $pschk).ConservationIdleTime
                if($null -ne $citchk){
                    set-itemproperty -path $pschk -name ConservationIdleTime -value $apbytes
                }
                $ipschk=$null
                $ipschk=(get-itemproperty -path $pschk).IdlePowerState
                if($null -ne $ipschk){
                    # Default 03 00 00 00
                    set-itemproperty -path $pschk -name IdlePowerState -value $apbytes
                }
                $pitchk=$null
                $pitchk=(get-itemproperty -path $pschk).PerformanceIdleTime
                if($null -ne $pitchk){
                    # Default 04 00 00 00
                    set-itemproperty -path $pschk -name PerformanceIdleTime -value $apbytes
                }
            }
        }
    }
}

<#
TODO
https://www.elevenforum.com/t/add-edit-or-run-with-to-ps1-file-context-menu-in-windows-11.20366/
Run with As Admin
[HKEY_CLASSES_ROOT\SystemFileAssociations\.ps1\Shell\Edit-Run-with\shell\006flyout\Command]
@="PowerShell -windowstyle hidden -Command \"Start-Process cmd -ArgumentList '/s,/c,start PowerShell_ISE.exe \"\"%1\"\"'  -Verb RunAs\""


[HKEY_CLASSES_ROOT\SystemFileAssociations\.ps1\Shell\Edit-Run-with\shell\007flyout]
"MUIVerb"="Edit with PowerShell ISE (x86)"
"Icon"="powershell_ise.exe"
"CommandFlags"=dword:00000020

[HKEY_CLASSES_ROOT\SystemFileAssociations\.ps1\Shell\Edit-Run-with\shell\007flyout\Command]
@="\"C:\\WINDOWS\\syswow64\\WindowsPowerShell\\v1.0\\powershell_ise.exe\" \"%1\""


[HKEY_CLASSES_ROOT\SystemFileAssociations\.ps1\Shell\Edit-Run-with\shell\008flyout]
"MUIVerb"="Edit with PowerShell ISE (x86) as administrator"
"HasLUAShield"=""
"Icon"="powershell_ise.exe"

[HKEY_CLASSES_ROOT\SystemFileAssociations\.ps1\Shell\Edit-Run-with\shell\008flyout\Command]
@="PowerShell -windowstyle hidden -Command \"Start-Process cmd -ArgumentList '/s,/c,start C:\\WINDOWS\\syswow64\\WindowsPowerShell\\v1.0\\powershell_ise.exe \"\"%1\"\"'  -Verb RunAs\""


[HKEY_CLASSES_ROOT\SystemFileAssociations\.ps1\Shell\Edit-Run-with\shell\009flyout]
"MUIVerb"="Edit with Notepad"
"Icon"="notepad.exe"
"CommandFlags"=dword:00000020

[HKEY_CLASSES_ROOT\SystemFileAssociations\.ps1\Shell\Edit-Run-with\shell\009flyout\Command]
@="\"C:\\Windows\\System32\\notepad.exe\" \"%1\""

[HKEY_CLASSES_ROOT\SystemFileAssociations\.ps1\Shell\Edit-Run-with\shell\010flyout]
"MUIVerb"="Edit with Notepad as administrator"
"HasLUAShield"=""
"Icon"="notepad.exe"
#>
# REGISTRY HKLM END

# HIBERNATE START
$hchk=Get-ItemProperty HKLM:\SYSTEM\CurrentControlSet\Control\Power -name HibernateEnabled -ea SilentlyContinue
if($null -eq $hchk -or $hchk.HibernateEnabled -eq 0){
    powercfg /hibernate on
    powercfg /hibernate /type full
    $hguichk=get-itemproperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FlyoutMenuSettings' -name ShowHibernateOption -ea SilentlyContinue
    if($null -eq $hguichk -or $hguichk.ShowHibernateOption -eq 0){
        # Folder didn't exist on new install.
        new-item -path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FlyoutMenuSettings'
        set-itemproperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FlyoutMenuSettings' -name ShowHibernateOption -value 1
    }
}
# HIBERNATE END

# TIME SETTINGS START

$gtz=(get-timezone).id
if($gtz -ne 'Eastern Standard Time'){
    set-timezone -id 'Eastern Standard Time'
}

$gipn=(get-itemproperty 'HKLM:\SYSTEM\CurrentControlSet\Services\W32Time\Parameters').NtpServer
if($gipn -ne 'time.windows.com time.nist.gov pool.ntp.org'){
    # https://learn.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2012-R2-and-2012/ff799054(v=ws.11)
    # This command shows error if w32time isn't started with:
    # The following error occurred: The service has not been started. (0x80070426)
    w32tm /config /manualpeerlist:"time.windows.com time.nist.gov pool.ntp.org" /reliable:YES /syncfromflags:MANUAL /update
    w32tm /resync
}
$gipt=(get-itemproperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\DateTime\Servers').3
if($null -eq $gipt){
    set-itemproperty -path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\DateTime\Servers' -name '3' -value 'pool.ntp.org'
}

# TIME SETTINGS END

# SERVICES DISABLE START

$serv_1=new-object 'System.Collections.Generic.List[String]'
# Connected User Experiences and Telemetry disable https://learn.microsoft.com/en-us/windows/privacy/configure-windows-diagnostic-data-in-your-organization
$serv_1.add('diagtrack')
<#
https://learn.microsoft.com/en-us/bingmaps/v8-web-control/modules/spatial-data-service-module/

#Diagnostic Execution Service disable
$serv_1.add('diagsvc')
# Diagnostic Policy Service disable https://docs.microsoft.com/en-us/previous-versions/aa905076(v=msdn.10)
start-process powershell.exe -argumentlist 'set-service -name dps -startuptype disabled ; stop-service -name dps -force' -windowstyle hidden -verb runas
# Diagnostic Service Host disable
start-process powershell.exe -argumentlist 'set-service -name WdiServiceHost -startuptype disabled ; stop-service -name WdiServiceHost -force' -windowstyle hidden -verb runas
#set-service -name WdiServiceHost -startuptype disabled ; stop-service -name WdiServiceHost -force
# Diagnostic System Host disable
start-process powershell.exe -argumentlist 'set-service -name WdiSystemHost -startuptype disabled ; stop-service -name WdiSystemHost -force' -windowstyle hidden -verb runas
#set-service -name WdiSystemHost -startuptype disabled ; stop-service -name WdiSystemHost -force
#>
# GameInput Service
$serv_1.add('GameInputSvc')
# Downloaded Maps Manager
$serv_1.add('MapsBroker')
# Geolocation
$serv_1.add('lfsvc')
# Spatial Data Service disable
$serv_1.add('SharedRealitySvc')
# Windows Perception Service disable
$serv_1.add('spectrum')
<# Windows Perception Simulation Service disable
https://learn.microsoft.com/en-us/windows/iot/iot-enterprise/optimize/services#system-services
#>
$serv_1.add('perceptionsimulation')
<# Auto Time Zone Updater
https://learn.microsoft.com/en-us/windows/iot/iot-enterprise/optimize/services#system-services
#>
$serv_1.add('tzautoupdate')
<# Windows Error Reporting Service disable
https://learn.microsoft.com/en-us/windows/win32/wer/wer-settings
https://learn.microsoft.com/en-us/troubleshoot/windows-client/system-management-components/windows-error-reporting-diagnostics-enablement-guidance
#>
$serv_1.add('WerSvc')
# Report Problems Off / Disable Windows Error Reporting
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\Windows Error Reporting' -Name Disabled -Value 1
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\Windows Error Reporting' -Name DontShowUI -Value 1
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\Windows Error Reporting\Consent' -Name DefaultConsent -Value 1
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\Windows Error Reporting\Consent' -Name DontSendAdditionalData -Value 1
<# Temporarily enable Windows Error Reporting DEBUG
# Does not give user option via UI to send or not send report using below in Windows 11 Pro 23H2 22631.3007 WORKGROUP. Hence, presently it's fully disable WER or enable and auto send all reports.
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\Windows Error Reporting' -Name Disabled -Value 0
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\Windows Error Reporting' -Name DontShowUI -Value 0
set-service -name wercplsupport -StartupType automatic
start-service -name wercplsupport
set-service -name wersvc -StartupType automatic
start-service -name wersvc
#>
# Xbox Accessory Management Service disable
$serv_1.add('XboxGipSvc')
# Xbox Live Auth Manager disable
$serv_1.add('XblAuthManager')
# Xbox Live Game Save disable
$serv_1.add('XblGameSave')
# Xbox Live Networking Service disable
$serv_1.add('XboxNetApiSvc')
foreach($srv in $serv_1){
    set-service -name $srv -startuptype disabled -ea 0
    stop-service -name $srv -force -ea 0
}

# SERVICES DISABLE END

# SERVICES ENABLE START

$servon_1=new-object 'System.Collections.Generic.List[String]'
foreach($srvon in $servon_1){
    set-service -name $srvon -startuptype Automatic
}
# SERVICES ENABLE END

# WINDOWSCAPABILITY START

# Add-WindowsCapability https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse
add-WindowsCapability -online -Name 'OpenSSH.Client~~~~0.0.1.0'

# Remove-WindowsCapability

# https://learn.microsoft.com/en-us/windows-hardware/manufacture/desktop/features-on-demand-language-fod?view=windows-11
$rwc_1=new-object 'System.Collections.Generic.List[String]'
$rwc_1.add('App.StepsRecorder~~~~0.0.1.0')
$rwc_1.add('Browser.InternetExplorer~~~~0.0.11.0')
# Premanent package cannot be removed
# $rwc_1.add('Language.Basic~~~en-US~0.0.1.0')
$rwc_1.add('Hello.Face.20134~~~~0.0.1.0')
$rwc_1.add('Language.Handwriting~~~en-US~0.0.1.0')
$rwc_1.add('Language.OCR~~~en-US~0.0.1.0')
$rwc_1.add('Language.Speech~~~en-US~0.0.1.0')
$rwc_1.add('Language.TextToSpeech~~~en-US~0.0.1.0')
$rwc_1.add('MathRecognizer~~~~0.0.1.0')
$rwc_1.add('Media.WindowsMediaPlayer~~~~0.0.12.0')
$rwc_1.add('Microsoft.Wallpapers.Extended~~~~0.0.1.0')
$rwc_1.add('Microsoft.Windows.MSPaint~~~~0.0.1.0')
$rwc_1.add('Microsoft.Windows.Notepad.System~~~~0.0.1.0')
<# Leave for troubleshooting. Reinstall:
DISM /Online /Add-Capability /CapabilityName:Microsoft.Windows.PowerShell.ISE~~~~0.0.1.0
$rwc_1.add('Microsoft.Windows.PowerShell.ISE~~~~0.0.1.0')
#>
<# Not uninstallable https://learn.microsoft.com/en-us/defender-endpoint/windows-whatsnew
remove-windowscapability : Remove-WindowsCapability failed. Error code = 0x800f0825
At line:1 char:1
+ remove-windowscapability -online -Name 'Microsoft.Windows.Sense.Clien ...
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : NotSpecified: (:) [Remove-WindowsCapability], COMException
    + FullyQualifiedErrorId : Microsoft.Dism.Commands.RemoveWindowsCapabilityCommand
#>
<# https://learn.microsoft.com/en-us/windows-hardware/manufacture/desktop/features-on-demand-non-language-fod?view=windows-11#sense-client-for-microsoft-defender-for-endpoint-mde
$rwc_1.add('Microsoft.Windows.Sense.Client~~~~')
Remove-WindowsCapability failed. Error code = 0x800f0825

DISM /Online /Remove-Capability /CapabilityName:Microsoft.Windows.Sense.Client~~~~

Deployment Image Servicing and Management tool
Version: 10.0.26100.1150

Image Version: 10.0.26100.3194


Error: 0x800f0825

DISM failed. No operation was performed.
For more information, review the log file.

The DISM log file can be found at C:\WINDOWS\Logs\DISM\dism.log
#>
$rwc_1.add('Microsoft.Windows.WordPad~~~~0.0.1.0')
$rwc_1.add('OneCoreUAP.OneSync~~~~0.0.1.0')
$rwc_1.add('Print.Fax.Scan~~~~0.0.1.0')
$rwc_1.add('VBSCRIPT~~~~')
$rwc_1.add('XPS.Viewer~~~~0.0.1.0')

foreach($wincap in $rwc_1){
    Get-windowscapability -online | where-object name -eq $wincap | Remove-WindowsCapability -online
}

# WINDOWSCAPABILITY END

# WINDOWSOPTIONALFEATURE DISABLE START

<# Legacy / W10 / Home only?!
FaxServicesClientPackage
Internet-Explorer-Optional-amd64
Microsoft-Windows-Printing-XPSServices-Package
Xps-Foundation-Xps-Viewer
#>

# Disable-WindowsOptionalFeature
$wofchk_1=new-object 'System.Collections.Generic.List[String]'
$wofchk_1.add('FaxServicesClientPackage')
$wofchk_1.add('Internet-Explorer-Optional-amd64')
$wofchk_1.add('Microsoft-SnippingTool')
$wofchk_1.add('Microsoft-Windows-Printing-XPSServices-Package')
$wofchk_1.add('MicrosoftWindowsPowerShellV2')
$wofchk_1.add('MicrosoftWindowsPowerShellV2Root')
$wofchk_1.add('NetFx3')
$wofchk_1.add('Printing-Foundation-InternetPrinting-Client')
$wofchk_1.add('Printing-XPSServices-Features')
$wofchk_1.add('Recall')
$wofchk_1.add('SearchEngine-Client-Package')
$wofchk_1.add('SMB1Protocol')
$wofchk_1.add('SMB1Protocol-Client')
$wofchk_1.add('SMB1Protocol-Deprecation')
$wofchk_1.add('SMB1Protocol-Server')
$wofchk_1.add('TFTP')
$wofchk_1.add('WorkFolders-Client')
$wofchk_1.add('Xps-Foundation-Xps-Viewer')

foreach($wof in $wofchk_1){
    $woftemp=$null
    $woftemp=(get-windowsoptionalfeature -online -FeatureName $wof | Where-Object {$_.State -eq 'Enabled'}).FeatureName
    if($null -ne $woftemp){
        Disable-WindowsOptionalFeature -Online -Featurename $woftemp -NoRestart
    }
}

# WINDOWSOPTIONALFEATURE DISABLE END

# WINDOWSOPTIONALFEATURE ENABLE START

$ewofchk_1=new-object 'System.Collections.Generic.List[String]'
$ewofchk_1.add('Microsoft-RemoteDesktopConnection')
$ewofchk_1.add('Printing-PrintToPDFServices-Features')
#$ewofchk_1.add('VirtualMachinePlatform')

foreach($item in $ewofchk_1){
    $tempewof=$null
    $tempewof=(get-windowsoptionalfeature -online -featurename $item).state
    if($tempewof -eq 'Disabled'){
        enable-windowsoptionalfeature -online -Featurename $item -norestart
    }
}

<#
https://learn.microsoft.com/en-us/windows/wsl/install
wsl --install
#>

# WINDOWSOPTIONALFEATURE ENABLE END

# STARTUP APPS START
# # Task Manager > Startup apps
<#
Removed item but it showed back up anyways in registry and Task Manager
remove-itemproperty -path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run' -name 'KeePass 2 PreLoad'
DID NOT WORK
remove-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\RunNotification' -name 'StartupTNotiKeePass 2 PreLoad'
#>

remove-itemproperty -path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Run' -name 'KeePass 2 PreLoad' -ea 0
# https://learn.microsoft.com/en-us/windows-server/identity/ad-ds/manage/understand-security-identifiers
# Windows Defender Application Guard removed from Windows 11 24H2 but account found in fresh install anyways https://learn.microsoft.com/en-us/windows/security/application-security/application-isolation/microsoft-defender-application-guard/faq-md-app-guard
$sidlist=get-ciminstance -classname win32_useraccount
foreach($sid in $sidlist){
    if(
    $sid.Name -ne 'Administrator' -and
    $sid.Name -ne 'DefaultAccount' -and
    $sid.Name -ne 'Guest' -and
    $sid.Name -ne 'WDAGUtilityAccount'
    ){
        # Found user account SID
        $usrsid=$sid.SID
        $runpath=-join('Registry::HKEY_USERS\',$usrsid,'\Software\Microsoft\Windows\CurrentVersion\Run')
        $runitems=get-item -path $runpath
        foreach($ritem in $runitems.property){
            if(
            $ritem -match '^BraveSoftware Update' -or
            $ritem -match '^MicrosoftEdgeAutoLaunch' -or
            $ritem -match '^Steam'
            ){
                remove-itemproperty -path $runitems.PSPath -Name $ritem
            }
        }
    }
}
# STARTUP APPS END

# SCHEDULED TASK START

# History was disabled by Windows doing in place update from 23H2 > 24H2 (Why?!). Enforce this to on.
wevtutil set-log Microsoft-Windows-TaskScheduler/Operational /enabled:true

# Maps
get-scheduledtask -taskpath \Microsoft\Windows\Maps\ | disable-scheduledtask

# OneDrive
get-scheduledtask | where-object {$_.taskname -match '^OneDrive Reporting Task'} | unregister-scheduledtask -confirm:$false
get-scheduledtask | where-object {$_.taskname -match '^OneDrive Standalone Update Task'} | unregister-scheduledtask -confirm:$false

# Speech
get-scheduledtask -taskpath \Microsoft\Windows\Speech\ | disable-scheduledtask

# Xbox
get-scheduledtask -taskpath \Microsoft\XblGameSave\ | disable-scheduledtask

# SCHEDULED TASK END

# MSIEXEC UNINSTALL START

$appchk=@()
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*'
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*'

# Microsoft Update Health Tools
$muhtchk=($appchk | where-object{$_.DisplayName -eq 'Microsoft Update Health Tools'}).uninstallstring
if($muhtchk.count -gt 0){
    foreach($muht in $muhtchk){
        $us=$muht.replace('MsiExec.exe /X{','MsiExec.exe /qn /norestart /x {')
        cmd.exe /c $us
    }
}

<# # MyDell forces GUI prompt during uninstall https://www.dell.com/support/manuals/en-us/mydell/mydell_3_3_ug/uninstall-mydell?guid=guid-fbae5ce1-be78-46e1-a3da-eb765f6c434e&lang=en-us
$mdchk=($appchk | where-object{$_.DisplayName -eq 'MyDell'}).uninstallstring
if($mdchk.count -gt 0){
    cmd.exe /c $mdchk
}
#>

# Snipping Tool
<#
After initiating uninstall via Programs and Features subsequent attempts via PWSH show window popup that stays open until toggled:
Snipping Tool
Snipping Tool could not be uninstalled due to an error.
Error code: 0x800700002
#>
$snipchk=($appchk | where-object{$_.DisplayName -match 'SnippingTool'}).uninstallstring
if($snipchk.count -gt 0){
    cmd.exe /c $snipchk
}

# Windows PC Health Check
$wphcchk=($appchk | where-object{$_.DisplayName -eq 'Windows PC Health Check'}).uninstallstring
if($wphcchk.count -gt 0){
    $us=$wphcchk.replace('MsiExec.exe /X{','MsiExec.exe /qn /norestart /x {')
    # For whatever reason, doing &, invoke-command, or msiexec.exe via PS5.1 or PWSH7.2.1 does not work
    cmd.exe /c $us
}

# Windows 11 Installation Assistant
$weiachk=($appchk | where-object{$_.DisplayName -eq 'Windows 11 Installation Assitant'}).uninstallstring
if($weiachk.count -gt 0){
    $us=$weiachk.replace('MsiExec.exe /X{','MsiExec.exe /qn /norestart /x {')
    # For whatever reason, doing &, invoke-command, or msiexec.exe via PS5.1 or PWSH7.2.1 does not work
    cmd.exe /c $us
}

# MSIEXEC UNINSTALL END

# JUNKWARE REMOVE START

# GLANCE
remove-itemproperty -path 'Registry::HKEY_CLASSES_ROOT\Local Settings\Software\Microsoft\Windows\Shell\MuiCache' -name 'C:\Program Files (x86)\GlanceGuest\GProtocolHandler.exe.ApplicationCompany' -ea 0

remove-itemproperty -path 'Registry::HKEY_CLASSES_ROOT\Local Settings\Software\Microsoft\Windows\Shell\MuiCache' -name 'C:\Program Files (x86)\GlanceGuest\GProtocolHandler.exe.FriendlyAppName' -ea 0

# NVIDIA
$npath="$env:ProgramFiles\NVIDIA Corporation\Installer2\InstallerCore\NVI2.DLL"
if((test-path $npath) -eq $true){
    $ansel=-join('RunDll32 "',$npath,'",UninstallPackage Ansel')
    cmd.exe /c $ansel
    $nvt=-join('RunDll32 "',$npath,'",UninstallPackage NVTelemetryContainer')
    cmd.exe /c $nvt
}

# ONEDRIVE
$opath="$env:PROGRAMDATA\Microsoft OneDrive"
if((test-path $opath) -eq $true){
    remove-item -path $opath -force -recurse
}

# JUNKWARE REMOVE END

# NVIDIA START

# NVIDIA Control Panel
$nschk=Get-ciminstance -Namespace 'root\CIMv2\nv' -Class __Namespace
if($null -ne $nschk){
    # namespace installed proceed

    $gpuchk=Get-ciminstance -Namespace 'root\CIMv2\nv' -Class 'gpu'
    if($null -ne $gpuchk){
        # NVIDIA GPU recognized proceed
        
        $profilename=get-wmiobject -Namespace 'root\CIMv2\nv' -class ProfileManager
        # Set Vertical sync off for Global Settings
        $profilename.invokemethod('setVSync',1)

        <# FINISH https://developer.nvidia.com/nvwmi-sdk
        $qr = Get-ciminstance -namespace "root\cimv2\nv" -Query 'select * from SettingTable'
        
        # $qr.SettingIds list of IDs
        #>
    }
}

# NVIDIA END

# APPXPACKAGE START

# remove-appxpackage
# https://learn.microsoft.com/en-us/windows/application-management/remove-provisioned-apps-during-update
# https://docs.microsoft.com/en-us/windows/privacy/manage-connections-from-windows-operating-system-components-to-microsoft-services#bkmk-cortana
# Native import-module > remove-appxpackage broken https://github.com/PowerShell/PowerShell/issues/16652
$appx_1=new-object 'System.Collections.Generic.List[String]'
$appx_1.add('AD2F1837.myHP')
$appx_1.add('AD2F1837.OMENCoomandCenter')
$appx_1.add('AD2F1837.DropboxOEM')
$appx_1.add('AppUp.IntelGraphicsExperience')
$appx_1.add('Clipchamp.Clipchamp')
$appx_1.add('McAfeeWPSSparsePackage')
$appx_1.add('Microsoft.3DBuilder')
# Cortana for Windows https://support.microsoft.com/en-us/topic/end-of-support-for-cortana-d025b39f-ee5b-4836-a954-0ab646ee1efa
$appx_1.add('*Microsoft.549981C3F5F10*')
$appx_1.add('Microsoft.Advertising.Xaml')
$appx_1.add('Microsoft.BingFinance')
$appx_1.add('Microsoft.BingNews')
$appx_1.add('Microsoft.BingSearch')
$appx_1.add('Microsoft.BingSports')
$appx_1.add('Microsoft.BingWeather')
$appx_1.add('Microsoft.Copilot')
$appx_1.add('Microsoft.GamingApp')
# Required to run Troubleshooters https://support.microsoft.com/en-us/windows/deprecation-of-microsoft-support-diagnostic-tool-msdt-and-msdt-troubleshooters-0c5ac9a2-1600-4539-b9d0-069e71f9040a
# $appx_1.add('Microsoft.GetHelp')
$appx_1.add('Microsoft.GetStarted')
$appx_1.add('Microsoft.Messaging')
$appx_1.add('Microsoft.Microsoft3DViewer')
$appx_1.add('Microsoft.MicrosoftOfficeHub')
$appx_1.add('Microsoft.MicrosoftSolitaireCollection')
$appx_1.add('Microsoft.MicrosoftStickyNotes')
$appx_1.add('Microsoft.MixedReality.Portal')
$appx_1.add('Microsoft.mspaint')
$appx_1.add('Microsoft.Office.OneNote')
$appx_1.add('Microsoft.Office.Sway')
$appx_1.add('Microsoft.OneConnect')
$appx_1.add('Microsoft.OutlookForWindows')
$appx_1.add('Microsoft.Paint')
$appx_1.add('Microsoft.People')
$appx_1.add('Microsoft.PowerAutomateDesktop')
$appx_1.add('Microsoft.Print3D')
$appx_1.add('Microsoft.ScreenSketch')
$appx_1.add('Microsoft.SkypeApp')
$appx_1.add('Microsoft.Todos')
$appx_1.add('Microsoft.Wallet')
$appx_1.add('Microsoft.Whiteboard')
$appx_1.add('Microsoft.Windows.Ai.Copilot.Provider')
# Not uninstallable $appx_1.add('Microsoft.Windows.PeopleExperienceHost')
$appx_1.add('Microsoft.Windows.DevHome')
$appx_1.add('Microsoft.Windows.Photos')
$appx_1.add('Microsoft.WindowsAlarms')
$appx_1.add('Microsoft.WindowsCalculator')
$appx_1.add('Microsoft.WindowsCamera')
$appx_1.add('microsoft.windowscommunicationsapps')
$appx_1.add('Microsoft.WindowsFeedbackHub')
$appx_1.add('Microsoft.WindowsFeedbackHub')
$appx_1.add('Microsoft.WindowsMaps')
$appx_1.add('Microsoft.WindowsNotepad')
$appx_1.add('Microsoft.WindowsSoundRecorder')
$appx_1.add('Microsoft.WindowsTerminal')
$appx_1.add('Microsoft.Xbox.TCUI')
$appx_1.add('Microsoft.XboxApp')
$appx_1.add('Microsoft.XboxGameOverlay')
$appx_1.add('Microsoft.XboxGamingOverlay')
$appx_1.add('Microsoft.XboxIdentityProvider')
$appx_1.add('Microsoft.XboxSpeechToTextOverlay')
$appx_1.add('Microsoft.YourPhone')
# Not Uninstallable $appx_1.add('Microsoft.XboxGameCallableUI')
$appx_1.add('Microsoft.ZuneMusic')
$appx_1.add('Microsoft.ZuneVideo')
$appx_1.add('MicrosoftCorporationII.MicrosoftFamily')
$appx_1.add('MicrosoftTeams')
# Get Started app not uninstallable $appx_1.add('MicrosoftWindows.Client.CBS')
$appx_1.add('MicrosoftWindows.Client.WebExperience')

$appx_1.add('MSTeams')
$appx_1.add('SpotifyAB.SpotifyMusic')

$psv=$psversiontable.PSVersion.Major
if($psv -lt 6){
    foreach($app in $appx_1){
        get-appxpackage -name $app -allusers | remove-appxpackage -allusers
    }
}else{
    # If Powershell Core on Windows do this until PWSHC works natively
    Import-Module -Name Appx -UseWindowsPowerShell
    foreach($app in $appx_1){
        start-process powershell.exe -argumentlist "get-appxpackage -name $app -allusers | remove-appxpackage -allusers" -windowstyle hidden -verb runas
    }
}

# APPXPACKAGE END

# APPXPROVISIONEDPACKAGE START

# Remove-AppxProvisionedPackage
#https://docs.microsoft.com/en-us/windows/security/information-protection/windows-information-protection/enlightened-microsoft-apps-and-wip
$aprov_1=new-object 'System.Collections.Generic.List[String]'
$aprov_1.add('AD2F1837.myHP')
$aprov_1.add('AD2F1837.OMENCoomandCenter')
$aprov_1.add('AD2F1837.DropboxOEM')
$aprov_1.add('McAfeeWPSSparsePackage')
$aprov_1.add('Microsoft.3dbuilder')
$aprov_1.add('Microsoft.BingFinance')
$aprov_1.add('Microsoft.BingNews')
$aprov_1.add('Microsoft.BingSearch')
$aprov_1.add('Microsoft.BingSports')
$aprov_1.add('Microsoft.BingWeather')
$aprov_1.add('Microsoft.GamingApp')
# Required to run Troubleshooters https://support.microsoft.com/en-us/windows/deprecation-of-microsoft-support-diagnostic-tool-msdt-and-msdt-troubleshooters-0c5ac9a2-1600-4539-b9d0-069e71f9040a
# $aprov_1.add('Microsoft.GetHelp')
$aprov_1.add('Microsoft.GetStarted')
$aprov_1.add('Microsoft.Microsoft3Dviewer')
$aprov_1.add('Microsoft.MicrosoftOfficeHub')
$aprov_1.add('Microsoft.MicrosoftSolitaireCollection')
$aprov_1.add('Microsoft.MicrosoftStickyNotes')
$aprov_1.add('Microsoft.MixedReality.Portal')
$aprov_1.add('Microsoft.MSPaint')
$aprov_1.add('Microsoft.Office.OneNote')
$aprov_1.add('Microsoft.Office.Sway')
$aprov_1.add('Microsoft.OneDriveSync')
$aprov_1.add('Microsoft.OutlookForWindows')
$aprov_1.add('Microsoft.Paint')
$aprov_1.add('Microsoft.People')
$aprov_1.add('Microsoft.PowerAutomateDesktop')
$aprov_1.add('Microsoft.Print3d')
$aprov_1.add('Microsoft.ScreenSketch')
$aprov_1.add('Microsoft.Skypeapp')
$aprov_1.add('Microsoft.Wallet')
$aprov_1.add('Microsoft.Whiteboard')
$aprov_1.add('Microsoft.Windows.Ai.Copilot.Provider')
$aprov_1.add('Microsoft.Windows.Photos')
$aprov_1.add('Microsoft.WindowsAlarms')
$aprov_1.add('Microsoft.WindowsCalculator')
$aprov_1.add('Microsoft.WindowsCamera')
$aprov_1.add('Microsoft.Windowscommunicationsapps')
$aprov_1.add('Microsoft.Windowsfeedbackhub')
$aprov_1.add('Microsoft.WindowsMaps')
$aprov_1.add('Microsoft.WindowsNotepad')
$aprov_1.add('Microsoft.WindowsSoundRecorder')
$aprov_1.add('Microsoft.WindowsTerminal')
$aprov_1.add('Microsoft.Xbox.TCUI')
$aprov_1.add('Microsoft.Xboxapp')
$aprov_1.add('Microsoft.XboxGameOverlay')
$aprov_1.add('Microsoft.XboxGamingOverlay')
$aprov_1.add('Microsoft.XboxIdentityProvider')
$aprov_1.add('Microsoft.XboxSpeechtotextoverlay')
$aprov_1.add('Microsoft.YourPhone')
$aprov_1.add('Microsoft.ZuneMusic')
$aprov_1.add('Microsoft.ZuneVideo')
$aprov_1.add('MicrosoftCorporationII.MicrosoftFamily')
$aprov_1.add('MicrosoftCorporationII.QuickAssist')
$aprov_1.add('MicrosoftWindows.Client.WebExperience')
$aprov_1.add('MicrosoftWindows.CrossDevice')
$aprov_1.add('MSTeams')
$aprov_1.add('Twitter')

foreach($apx in $aprov_1){
    Get-AppxProvisionedPackage -Online | Where-Object {$_.DisplayName -eq $apx} | Remove-AppxProvisionedPackage -allusers -online
}

# APPXPROVISIONEDPACKAGE END



<#
Unable to disable Microsoft Store via GP due to licensing
https://learn.microsoft.com/en-us/troubleshoot/windows-client/group-policy/cannot-disable-microsoft-store
#>

# Chocolatey START https://chocolatey.org/install#individual
$chocochk_1=choco -v
if($chocochk_1.count -eq 0){
    $executionpolicy_1=get-executionpolicy -scope Process
    Set-ExecutionPolicy -scope Process Bypass
    $secprotcol_1=[System.Net.ServicePointManager]::SecurityProtocol
    [System.Net.ServicePointManager]::SecurityProtocol=[System.Net.ServicePointManager]::SecurityProtocol -bor 3072
    Invoke-Expression((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
    set-executionpolicy -scope Process $executionpolicy_1
    [System.Net.ServicePointManager]::SecurityProtocol=$secprotcol_1
    $chocochk_2=choco -v
    if($chocochk_2.count -eq 0){
        # Warning but not an error when partially uninstalled
        <#
        WARNING: Files from a previous installation of Chocolatey were found at 'C:\ProgramData\chocolatey'.
        WARNING: An existing Chocolatey installation was detected. Installation will not continue. This script will not overwrite installations.
        If there is no Cocolatey installation at '', delete the folder and attemtpt the installation again.

        Please use choco upgrade chcolatey to handle upgrades of Cocolatey itself.
        If the existing installation is not functional or a prior installation did not complete, follow these steps:
        - Backup the files at the path listed above so you can restore your previous installation if needed.
        - Remove the existing installation manually.
        - Rerun this installation script.
        - Reinstall any packages previously installed, if needed (refer to the lib folder in the backup).
        
        Once installation is completed, the backup folder is no longer needed and can be deleted.
        #>
        remove-item -path "$env:ProgramData\chocolatey" -recurse -force
        remove-item -path "$env:ProgramData\ChocolateyHttpCache" -recurse -force
    }
    # Exit script given just installed chocolatey for first time
    exit 1
}

<# If choco wasn't just installed, then install packages. Chocolatey prompts below after first install. But, installs worked anyways...
WARNING: It's very likely you will need to close and reopen your shell before you can use choco.
#>

# Required as some apps won't update (e.g. brave, firefox, git, libreoffice-fresh, python311, sysinternals, visualstudio2022community, etc.) if not set
choco feature enable -n allowGlobalConfirmation

choco upgrade chocolatey

choco upgrade all

$chocolist_1=choco list

# Adobe Reader https://community.chocolatey.org/packages/adobereader
$archk_1=$chocolist_1|where-object{$_ -match '^adobereader'}
if($archk_1.count -eq 0){
    choco install adobereader -y --no-progress -r
}

# Autohotkey https://community.chocolatey.org/packages/autohotkey
$auchk_1=$chocolist_1|where-object{$_ -match '^autohotkey'}
if($auchk_1.count -eq 0){
    choco install autohotkey -y --no-progress -r
}

# Brave https://community.chocolatey.org/packages/brave
$brchk_1=$chocolist_1|where-object{$_ -match '^brave'}
if($brchk_1.count -eq 0){
    choco install brave -y --no-progress -r
}

<# Chrome https://community.chocolatey.org/packages/GoogleChrome
# -y didn't appear to work as it was holding up an upgrade.
# Disabled ublock origin = no Chrome for any reason
$chromechk_1=$chocolist_1|where-object{$_ -match '^GoogleChrome'}
if($chromechk_1.count -eq 0){
    choco install googlechrome -y --no-progress -r --ignorechecksum
}
#>

# Firefox https://community.chocolatey.org/packages/firefox
$ffchk_1=$chocolist_1|where-object{$_ -match '^firefox'}
if($ffchk_1.count -eq 0){
    choco install firefox -y --no-progress -r
}

# Flameshot https://community.chocolatey.org/packages/flameshot
$fschk_1=$chocolist_1|where-object{$_ -match '^flameshot'}
if($fschk_1.count -eq 0){
    choco install flameshot -y --no-progress -r
}

# Git https://community.chocolatey.org/packages/git.install
$gitchk_1=$chocolist_1|where-object{$_ -match '^git\.install'}
if($gitchk_1.count -eq 0){
    choco install 'git.install' -y --no-progress -r
}

# Keepass https://community.chocolatey.org/packages/keepass
$kpchk_1=$chocolist_1|where-object{$_ -match '^keepass'}
if($kpchk_1.count -eq 0){
    choco install 'keepass' -y --no-progress -r
}
# Krita https://community.chocolatey.org/packages/krita
$kritachk_1=$chocolist_1|where-object{$_ -match '^krita'}
if($kritachk_1.count -eq 0){
    choco install krita -y --no-progress -r
}

# LibreOffice Fresh https://community.chocolatey.org/packages/libreoffice-fresh
$lochk_1=$chocolist_1|where-object{$_ -match '^libreoffice-fresh'}
if($lochk_1.count -eq 0){
    choco install libreoffice-fresh -y --no-progress -r
}

# Libre Hardware Monitor
$lhchk_1=$chocolist_1|where-object{$_ -match '^librehardwaremonitor'}
if($lhchk_1.count -eq 0){
    choco install librehardwaremonitor -y --no-progress -r
}

<# MySQL CLI Choco package not updated frequently https://community.chocolatey.org/packages/mysql-cli
$mysqlclichk_1=$chocolist_1 | where-object{$_ -match '^mysql-cli'}
if($mysqlclichk_1.count -eq 0){
    choco install mysql-cli -y --no-progress -r
}

# MySQL Connector Choco package not updated frequently https://community.chocolatey.org/packages/mysql-connector
$mysqlconnectorchk_1=$chocolist_1|where-object{$_ -match '^mysql-connector'}
if($mysqlconnectorchk_1.count -eq 0){
    choco install mysql-connector -y --no-progress -r
}
#>

# Node.js 
$nppchk_1=$chocolist_1|where-object{$_ -match '^nodejs'}
if($nppchk_1.count -eq 0){
    choco install nodejs -y --no-progress -r
}

# Notepad++ https://community.chocolatey.org/packages/notepadplusplus
$nppchk_1=$chocolist_1|where-object{$_ -match '^notepadplusplus'}
if($nppchk_1.count -eq 0){
    choco install notepadplusplus -y --no-progress -r
}

# OBS https://community.chocolatey.org/packages/obs-studio
$obschk_1=$chocolist_1|where-object{$_ -match '^obs-studio'}
if($obschk_1.count -eq 0){
    choco install obs-studio -y --no-progress -r
}

# PDFSAM https://community.chocolatey.org/packages/pdfsam
$pdfchk_1=$chocolist_1|where-object{$_ -match '^pdfsam'}
if($pdfchk_1.count -eq 0){
    choco install pdfsam -y --no-progress -r
}

# Postgresql https://community.chocolatey.org/packages/postgresql
<#
$pgschk_1=$chocolist_1|where-object{$_ -match '^postgresql'}
if($pgschk_1.count -eq 0){
    $dp=& "$env:systemdrive\scripts\get-mdbpass.ps1"
    $pgsparam=-join('/AllowRemote /Password:',$dp,' /Port:5432')
    choco install postgresql --params $pgsparam --ia '--enable-components commandlinetools,pgAdmin,server' -y --no-progress -r
}
#>

# Python N-1 https://community.chocolatey.org/packages/python311
$python3chk_1=$chocolist_1|where-object{$_ -match '^python311'}
if($python3chk_1.count -eq 0){
    choco install python311 -y --no-progress -r
}

# Sysinternals https://community.chocolatey.org/packages/sysinternals
$sichk=$chocolist_1|where-object{$_ -match '^sysinternals'}
if($sichk.count -eq 0){
    choco install sysinternals -y --no-progress -r
}

<# Visual Studio 2022 Community https://community.chocolatey.org/packages/visualstudio2022community
MySQL for Visual Studio appears will never be compatible with vs202X > https://bugs.mysql.com/bug.php?id=105536
Supposedly use MySQL Shell for VS Code or move off MySQL
#>
$vs20xychk_1=$chocolist_1|where-object{$_ -match '^visualstudio2022community'}
if($vs20xychk_1.count -eq 0){
    choco install visualstudio2022community -y --no-progress -r
}

# VLC https://community.chocolatey.org/packages/vlc
$vlcchk_1=$chocolist_1|where-object{$_ -match '^vlc'}
if($vlcchk_1.count -eq 0){
    choco install vlc -y --no-progress -r
}

# VSCodium https://community.chocolatey.org/packages/vscodium
$vscchk_1=$chocolist_1|where-object{$_ -match '^vscodium'}
if($vscchk_1.count -eq 0){
    choco install vscodium -y --no-progress -r
}

# Visual Studio Code https://community.chocolatey.org/packages/vscode
#$vscodechk_1=$chocolist_1|where-object{$_ -match '^vscode'}
#if($vscodechk_1.count -eq 0){
#    choco install vscode -y --no-progress -r
#}

# CHOCOLATEY END

# NPM START

npm install -g npm@latest
npm install -g npm-check-updates
if((test-path "$env:homedrive\scripts\nodejs") -eq $false){
    new-item -path "$env:homedrive\scripts\nodejs" -itemtype Directory
}
cd "$env:homedrive\scripts\nodejs"
if((test-path "$env:homedrive\scripts\nodejs\package.json") -eq $false){
    npm init -y
}
npx npm-check-updates --packageFile "$env:homedrive\scripts\nodejs\package.json"
npx npm-check-updates
npx npm-check-updates -u

# NPM END

<# PIP START 

# Install PIP https://pip.pypa.io/en/stable/installation/
$null=py -m ensurepip --upgrade
# Upgrade PIP
$null=py -m pip install --upgrade pip
# requests dependencies certifi, urllib3, idna, charset-normalizer, requests
$null=py -m pip install requests
$pipchk_s=py -m pip list --outdated
if($pipchk_s.count -gt 0){
    for($i=2;$i -le $pipchk_s.count-1;$i++){
        $pippkg=$pipchk_s.item($i).split(' ')[0]
        $null=py -m pip install --upgrade $pippkg
    }
}
#> # PIP END

# USERCHOICE PROTECTION DRIVER START
# "HIDDEN" service not in services.msc??????? https://hitco.at/blog/windows-userchoice-protection-driver-ucpd/
get-scheduledtask -taskpath \Microsoft\Windows\AppxDeploymentClient\ -taskname 'UCPD velocity' | disable-scheduledtask
set-service -name 'ucpd' -startuptype disabled
# USERCHOICE PROTECTION DRIVER END

# NUGET START
# https://learn.microsoft.com/en-us/nuget/reference/nuget-exe-cli-reference?tabs=windows
$nupath="${env:ProgramFiles(x86)}\NuGet"
if((test-path $nupath) -eq $false){
    new-item -path $nupath -type Directory
    $nugurl='https://dist.nuget.org/win-x86-commandline/latest/nuget.exe'
    $nuout=-join($nupath,'\nuget.exe')
    invoke-webrequest -uri $nugurl -outfile $nuout
    $pathcurrent=$env:PATH
    $pathchk=-join('*',$nupath,'*')
    if(($pathcurrent -like $pathchk) -eq $false){
        $nugetpath=-join($pathcurrent,"$nupath;")
        [System.Environment]::SetEnvironmentVariable("Path",$nugetpath,[System.EnvironmentVariableTarget]::Machine)
        $env:PATH=$nugetpath
    }
    # https://learn.microsoft.com/en-us/nuget/consume-packages/configuring-nuget-behavior#nugetdefaultsconfig-location
    $nusb=@'
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <packageSources>
    <add key="nuget" value="https://api.nuget.org/v3/index.json" />
  </packageSources>
</configuration>
'@
    $nusb | set-content -path "$nupath\NuGetDefaults.Config" -nonewline
}
nuget update -self
# NUGET END

<# MARIADB START
$apps=@()
$apps+=Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*'
$mdbv='11.6.2'
$mdbd=-join('MariaDB ',([version]$mdbv).major,'.',([version]$mdbv).minor,' (x64)')
$mdbichk=$apps | where-object {$_.DisplayName -eq $mdbd}
if($null -eq $mdbichk){
    $mdurl="https://downloads.mariadb.org/rest-api/mariadb/$mdbv/mariadb-$mdbv-winx64.msi"
    $mdbout="$env:WINDIR\TEMP\mariadb-$mdbv-winx64.msi"
    Invoke-webrequest -Uri $mdurl -outfile $mdbout
    $mdchksum="https://downloads.mariadb.org/rest-api/mariadb/$mdbv/mariadb-$mdbv-winx64.msi/checksum/"
    $mdirc=(invoke-restmethod -uri $mdchksum -method Get).response.checksum.sha256sum
    $mdgfh=(get-filehash -algorithm sha256 -path $mdbout).hash
    if($mdirc -eq $mdgfh){
        $dp=& "$env:systemdrive\scripts\get-mdbpass.ps1"
        # https://mariadb.com/kb/en/installing-mariadb-msi-packages-on-windows/#silent-installation
        $mdblog=-join("$env:WINDIR\TEMP\",(get-date -f 'yyyyMMddHHmmss'),'-mdbinstall.txt')
        $msic=@(
        "/i $mdbout"
        "DATADIR=`"$env:PROGRAMFILES\MariaDB $mdbv\data`""
        "DEFAULTUSER=`"`""
        "INSTALLDIR=`"$env:PROGRAMFILES\MariaDB $mdbv\`""
        "/log `"$mdblog`""
        "PASSWORD=`"$dp`""
        "PORT=3306"
        "SERVICENAME=MariaDB"
        "STDCONFIG=1"
        "UTF8=1"
        "/qn"
        )
        start-process -filepath "$env:windir\System32\msiexec.exe" -ArgumentList $msic -workingdirectory "$env:windir\System32" -wait -nonewwindow
        $pathcurrent=$env:PATH
        $pathchk=-join('*',"$env:PROGRAMFILES\MariaDB $mdbv\bin",'*')
        if(($pathcurrent -like $pathchk) -eq $false){
            $mdbpath=-join($pathcurrent,"$env:PROGRAMFILES\MariaDB $mdbv\bin;")
            [System.Environment]::SetEnvironmentVariable("Path",$mdbpath, [System.EnvironmentVariableTarget]::Machine)
            $env:PATH=-join($env:PATH,"$env:PROGRAMFILES\MariaDB $mdbv\bin;")
        }
        $mdbsg='SET GLOBAL max_allowed_packet=1073741824;set global net_read_timeout=1200;set session net_read_timeout=1200;SET GLOBAL wait_timeout=57600;set global interactive_timeout=57600;'
        $arglist2=-join('-u root -p',$dp,' -e "',$mdbsg,'"')
        start-process "$env:PROGRAMFILES\MariaDB $mdbv\bin\mysql.exe" -argumentlist $arglist2 -nonewwindow -wait
    }
}

# MySQL CONNECTOR .NET START
# https://dev.mysql.com/doc/connector-net/en/connector-net-installation-binary-windows-installer.html
$app=@()
$app+=get-itemproperty 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*'
$mscv='9.1.0'
$mscdn=-join('MySQL Connector/NET ',$mscv)
$mscchk=$app | where-object {$_.DisplayName -eq $mscdn}
if($null -eq $mscchk){
    $mscurl="https://dev.mysql.com/get/Downloads/Connector-Net/mysql-connector-net-$mscv.msi"
    $mscout="$env:WINDIR\TEMP\mysql-connector-net-$mscv.msi"
    Invoke-webrequest -Uri $mscurl -outfile $mscout
    $mscmd5='4e3aed92add170d0954b5004a30d7111'
    $gfhmcn=(get-filehash -algorithm md5 $mscout).hash
    if($gfhmcn -eq $mscmd5){
        $msiec=@(
        "/package $mscout"
        "/quiet"
        )
        start-process -filepath "$env:windir\System32\msiexec.exe" -ArgumentList $msiec -workingdirectory "$env:windir\System32" -wait -nonewwindow
    }
}
# MySQL CONNECTOR .NET END

# MYSQLCONNECTOR NUGET START

# https://www.nuget.org/packages/MySqlConnector
$mscndir="$env:systemdrive\scripts\nugets\mysqlconnector"
if((test-path -path $mscndir) -eq $false){
    nuget install mysqlconnector -outputdirectory $mscndir
}

# MYSQLCONNECTOR NUGET END

<# ED25519 PLUGIN START
# NOT READY YET
# https://www.nuget.org/packages/MySqlConnector.Authentication.Ed25519/
$maedir="$env:systemdrive\scripts\nugets\MySqlConnector.Authentication.Ed25519"
if((test-path -path $maedir) -eq $false){
    nuget install 'MySqlConnector.Authentication.Ed25519' -outputdirectory $maedir -version 2.4.0
}

# System.Runtime.CompilerServices.Unsafe LATEST
# $srcdir="$env:systemdrive\scripts\nugets\System.Runtime.CompilerServices.Unsafe"
# if((test-path -path $srcdir) -eq $false){
#     nuget install 'System.Runtime.CompilerServices.Unsafe' -outputdirectory $srcdir
# }

# System.Runtime.CompilerServices.Unsafe 4.5.3
$srcdir="$env:systemdrive\scripts\nugets\System.Runtime.CompilerServices.Unsafe.4.5.3"
if((test-path -path $srcdir) -eq $false){
    nuget install 'System.Runtime.CompilerServices.Unsafe' -outputdirectory $srcdir -version 4.5.3
}

$myini=get-content -path "$env:PROGRAMFILES\MariaDB $mdbv\data\my.ini" -raw
if(($myini -match 'plugin_load_add=auth_ed25519') -eq $false){
    $mindex=$myini.indexof('[mysqld]')
    $myininew=-join("[mysqld]`r`n","plugin_load_add=auth_ed25519`r`n",$myini.substring(10,($myini.length)-1-10))
    $myininew | set-content -path "$env:PROGRAMFILES\MariaDB $mdbv\data\my.ini"
    # Default install uses mysql_native_password https://mariadb.com/kb/en/authentication-plugin-mysql_native_password/
    restart-service -name 'MariaDB'
    $dp=& "$env:systemdrive\scripts\get-mdbpass.ps1"
    $scpc=-join("ALTER USER root@localhost IDENTIFIED VIA ed25519 USING PASSWORD('$dp');")
    $arglist1=-join('-u root -p',$dp,' -e "',$scpc,'"')
    start-process "$env:PROGRAMFILES\MariaDB $mdbv\bin\mysql.exe" -argumentlist $arglist1 -nonewwindow -wait
}
# ED25519 PLUGIN END
#>

# MARIADB END

# POSTGRESQL START
$pgv='17'
$apps=@()
$apps+=Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*'
$pgschk=$apps | where-object {$_.DisplayName -match "PostgreSQL $pgv"}
if($null -eq $pgschk){
    $dp=& "$env:systemdrive\scripts\get-mdbpass.ps1"
    $pgsout="$env:windir\TEMP\postgresql-17.2-2-windows-x64.exe"
    $pgsurl='https://sbp.enterprisedb.com/getfile.jsp?fileid=1259295'
    invoke-webrequest -uri $pgsurl -outfile $pgsout
    $pgshash='06CB160D5229C3F939CDF3B42E45F61847F09A0FA8C1674260B73D1F0A9855D6'
    $pgshashchk=(get-filehash -path $pgsout -algorithm sha256).hash
    if($pgshash -eq $pgshashchk){
        $pgdt=get-date -f 'yyyyMMddHHmmss'
        # https://www.enterprisedb.com/docs/supported-open-source/postgresql/installing/command_line_parameters/
        $pgargs=-join('--datadir ',"`"$env:ProgramFiles\PostgreSQL\$pgv\data`"",' --enable-components commandlinetools,pgAdmin,server,stackbuilder --mode unattended --prefix ',"`"$env:ProgramFiles\PostgreSQL\$pgv`"",' --serverport ',$pgsport,' --serviceaccount postgres --servicename postgres --superpassword ',$dp,' --unattendedmodeui none')
        # As of 17.2 .vbs files are executed on "$env:TEMP\install-postgresql.log"
        add-windowscapability -name 'VBSCRIPT~~~~' -online
        start-process -filepath $pgsout -argumentlist $pgargs -nonewwindow -wait
        remove-item "$env:TEMP\install-postgresql.log"
        remove-windowscapability -name 'VBSCRIPT~~~~' -online
        $pgchk=-join('*',"$env:ProgramFiles\PostgreSQL\$pgv",'*')
        $pathcurrent=$env:PATH
        if(($pathcurrent -like $pgchk) -eq $false){
            $pgpath=-join($pathcurrent,"$env:ProgramFiles\PostgreSQL\$pgv\bin;")
            [System.Environment]::SetEnvironmentVariable("Path",$pgpath, [System.EnvironmentVariableTarget]::Machine)
            $env:PATH=$pgpath
        }
    }
    # UNINSTALL
    # start-process -filepath "$env:PROGRAMFILES\PostgreSQL\$pgv\uninstall-postgresql.exe" -argumentlist '--mode unattended' -nonewwindow -wait
}

$npgsqldir="$env:systemdrive\scripts\nugets\Npgsql"
if((test-path -path $npgsqldir) -eq $false){
    nuget install 'Npgsql' -outputdirectory $npgsqldir
}

# POSTGRESQL END

# DELL BIOS START https://www.dell.com/support/kbdoc/en-us/000177240/dell-command-powershell-provider
$manuchk=(Get-ciminstance -Class Win32_ComputerSystem).Manufacturer
if($manuchk -match '^Dell'){
    $dbpchk=get-installedmodule | where-object {$_.Name -eq 'DellBIOSProvider'}
    if($null -eq $dbpchk){
        Install-Module -Name DellBIOSProvider -Force
    }
    update-module -name DELLBIOSProvider -acceptlicense -scope allusers
    import-module -name DELLBIOSProvider
    
    set-item dellsmbios:PreEnabled\TelemetryAccessLvl -Value Disabled
    # CStates disable https://learn.microsoft.com/en-us/windows-server/networking/technologies/network-subsystem/net-sub-performance-tuning-nics
    set-item dellsmbios:Performance\CStatesCtrl -Value Disabled

    $catlist=ls dellsmbios:
    $drawpath="$env:homedrive\scripts\LOGS\$dt-dellbiosraw.csv"
    $finalpath="$env:homedrive\scripts\LOGS\$dt-dellbios.csv"
    foreach($cat in $catlist){
        $data=-join('dellsmbios:',$cat.category)
        $list=ls $data -ea 0
        $list | export-csv -path $drawpath -notypeinformation -append
    }
    $csvraw=import-csv -path $drawpath
    for($i=0;$i -lt $csvraw.count;$i++){
        $csvraw.item($i).psobject.properties.remove('PossibleValues')
        $csvraw.item($i).psobject.properties.remove('PSDrive')
        $csvraw.item($i).psobject.properties.remove('PSIsContainer')
        $csvraw.item($i).psobject.properties.remove('PSParentPath')
        $csvraw.item($i).psobject.properties.remove('PSPath')
        $csvraw.item($i).psobject.properties.remove('PSProvider')
        $csvraw.item($i).psobject.properties.remove('UnsupportedPossibleValues')
        if(
        $csvraw.item($i).attribute -ne 'ExpressServiceCode' -and
        $csvraw.item($i).attribute -ne 'ManufactureDate' -and
        $csvraw.item($i).attribute -ne 'OwnershipDate' -and
        $csvraw.item($i).attribute -ne 'ProcessorId' -and
        $csvraw.item($i).attribute -ne 'SvcTag'){
            $csvraw.item($i) | export-csv -path $finalpath -notypeinformation -append
        }
    }
}
# DELL BIOS END

# HARDWARE DIAGNOSTICS START

# Drives
$drchk=Get-CimInstance -namespace root\wmi -class MSStorageDriver_FailurePredictStatus
foreach($drive in $drchk){
    if($drive.predictfailure -eq $true){
        write-host 'Drive failure predicted'
        write-host $drive.instancename
        write-host $drive.reason
    }
}

$dll="$env:ProgramData\chocolatey\lib\librehardwaremonitor\tools\LibreHardwareMonitorLib.dll"
Add-Type -LiteralPath $dll
$monitor = [LibreHardwareMonitor.Hardware.Computer]::new()
$monitor.IsCPUEnabled = $true
$monitor.Open()
foreach ($sensor in $monitor.Hardware.Sensors) {
    if ($sensor.SensorType -eq 'Temperature' -and $sensor.Name -eq 'CPU Package'){
        $tempc = $sensor.Value
        break
    }
}
$monitor.Close()
$tempf=$tempc*(9/5)+32
if($tempf -gt 180){
    write-host 'CPU temp hot'
}

# GPU Temperature

<# NVS 510 https://support.lenovo.com/us/en/solutions/pd026521-nvidia-nvs-510-2gb-graphics-card-overview-and-service-parts
Operating Environment: Temperature: 0°C to 55°C (32 deg to 131 deg (F))
# NVIDIA WMI https://www.nvidia.com/content/quadro_fx_product_literature/wp-06953-001-v02.pdf
#>
$ntempchk=(get-ciminstance -class 'thermalprobe' -namespace 'root\CIMV2\NV').temperature
if($null -ne $tempchk){
    if($ntempchk -gt 130){
        write-host 'GPU temp >130F may have temp issue'
    }
}
# HARDWARE DIAGNOSOTICS END

# MODULES RESTORE
set-psrepository -name 'PSGallery' -installationpolicy $inp

Set-MpPreference -DisableRealtimeMonitoring $false
stop-transcript