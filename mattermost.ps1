 # https://docs.mattermost.com/deploy/desktop-app.html

$null=start-transcript -path 'C:\temp\mattermostinstall.log'

# Prep config json that is get-filehash equivalent as if doing echo method via cmd
$content = @"
{
  "version": 2,
  "teams": [
    {
      "name": "core",
      "url": "https://community.mattermost.com",
      "order": 0
    }
  ],
  "showTrayIcon": true,
  "trayIconTheme": "light",
  "minimizeToTray": true,
  "notifications": {
    "flashWindow": 2,
    "bounceIcon": true,
    "bounceIconType": "informational"
  },
  "showUnreadBadge": true,
  "useSpellChecker": true,
  "enableHardwareAcceleration": true,
  "autostart": true,
  "spellCheckerLocale": "en-US",
  "darkMode": false
}
"@ + "`r`n";

if(!(test-path "$env:APPDATA\Mattermost")){
    # Path does not exist create it
    mkdir "$env:APPDATA\Mattermost"
}

$appchk=@()
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*'
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*'
$appchk+=Get-ItemProperty 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*'
<# .exe W11 5.6.0, 5.4.0, 5.1.0, 5.0.4 found in HKCU
C:\Users\USERNAME\AppData\Local\Programs\mattermost-desktop
.msi HKLM
C:\Program Files\Mattermost\Desktop
#>


$mmchk=@()
$mmchk+=$appchk | where-object{$_.DisplayName -like 'Mattermost *'}
<# DisplayName for .exe install:
Mattermost X.Y.Z
.msi install:
Mattermost Desktop
#>


# Uninstall scriptblock
$uninstallmm={
    $null=taskkill /f /im Mattermost.exe
    foreach($item in $mmchk){
        $qschk=$null
        $uns=$null
        # .exe uninstall check
        $qschk=$item.QuietUninstallString
        # .msi uninstall check
        $uns=$item.UninstallString.replace('MsiExec.exe /I{','MsiExec.exe /qn /norestart /x {')
        if($null -ne $qschk){
            # .exe install remove
            cmd /c $qschk
        }elseif($null -ne $uns){
            # .msi install found remove
            cmd /c $uns
        }else{
            write-host 'Uninstall scriptblock check failed. Fix me!'
        }
    }
}

# Install scriptblock
$installmm={
    # .exe
    invoke-webrequest -uri 'https://releases.mattermost.com/desktop/5.6.0/mattermost-desktop-setup-5.6.0-win.exe' -outfile "$env:HOMEDRIVE\temp\mattermost-desktop-setup-5.6.0-win.exe"
    start-process -filepath "$env:HOMEDRIVE\temp\mattermost-desktop-setup-5.6.0-win.exe" -argumentlist '--silent' -wait
    
    <# Did not prevent .exe installer window from displaying:
    1) -windowstyle Hidden
    2) -nonewwindow
    3) $null=
    4) -silent
    5) --silent
    For whatever reason, --silent isn't actually silent, event if run in cmd W11
    6) FAILS with "This installation package could not be opened..."
    $exemm='msiexec.exe /i "c:\temp\mattermost-desktop-setup-5.6.0-win.exe" /qn /norestart'
    cmd.exe /c $exemm

    DEBUG
    $null=start-process "$env:HOMEPATH\AppData\Local\Programs\mattermost-desktop\Uninstall Mattermost.exe" -argumentlist '/currentuser /S' -wait
    #>
    
    <# .msi
    https://docs.mattermost.com/install/desktop-msi-installer-and-group-policy-install.html
    .msi installer installs in C:\Program Files\Mattermost\Desktop
    invoke-webrequest -uri 'https://releases.mattermost.com/desktop/5.6.0/mattermost-desktop-5.6.0-x64.msi' -outfile "$env:HOMEDRIVE\temp\mattermost-desktop-5.6.0-x64.msi"
    $msimm='msiexec.exe /i "c:\temp\mattermost-desktop-5.6.0-x64.msi" /qn /norestart'
    cmd.exe /c $msimm

    DEBUG
    cmd.exe /c 'msiexec.exe /x "c:\temp\mattermost-desktop-5.6.0-x64.msi" /qn /norestart'
    #>
}

if($mmchk.count -eq 0){
    # Not installed install production version
    invoke-command -ScriptBlock $installmm
}elseif(
    $mmchk.count -gt 1 -or
    ($mmchk.count -eq 1 -and $mmchk.item(0).DisplayVersion -ne '5.6.0')
){
    invoke-command -ScriptBlock $uninstallmm
    invoke-command -ScriptBlock $installmm
}

<# Enforce config.json as installer creates or overwrites $env:APPDATA\Mattermost\config.json
As per W11 Notepad, doing cmd echo method defaults to:
UTF-8

Windows Powershell out-file defaults to UTF-16 LE. However, using switch -encoding -utf8 produces UTF8 with BOM hence use .NET instead
#>
$bytes = [System.Text.Encoding]::UTF8.GetBytes($content)
$null=[System.IO.File]::WriteAllBytes("$env:APPDATA\Mattermost\config.json", $bytes)
$null=stop-transcript
