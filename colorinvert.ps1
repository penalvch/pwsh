$dt=get-date -f 'yyyyMMddHHmmss'
$ScriptInvocation = (Get-Variable MyInvocation -Scope Script).Value
$ScriptName = $ScriptInvocation.MyCommand.Name
$fn="$env:homedrive\scripts\LOGS\$dt-$ScriptName.txt"
$null=start-transcript -path $fn -IncludeInvocationHeader

# Close Settings Window Scriptblock
$cssb={
Add-Type @"
using System;
using System.Runtime.InteropServices;
public class WindowHelper
{
    [DllImport("user32.dll")]
    public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
    public const uint WM_CLOSE = 0x0010;
    public static void CloseSettingsWindow()
    {
        IntPtr hWnd = FindWindow(null, "Settings");
        if (hWnd != IntPtr.Zero)
        {
            PostMessage(hWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
        }
    }
}
"@
[WindowHelper]::CloseSettingsWindow()
}

# SetSysColor Invert Scriptblock
$sisb={
    add-type -typedefinition "using System;`n using System.Runtime.InteropServices;`n public class PInvoke { [DllImport(`"user32.dll`")] public static extern bool SetSysColors(int cElements, int[] lpaElements, int[] lpaRgbValues); }"
    [PInvoke]::SetSysColors(1, @(1), @(0xFFFFFF))
}

# SetSysColor No Invert Scriptblock
$snsb={
    add-type -typedefinition "using System;`n using System.Runtime.InteropServices;`n public class PInvoke { [DllImport(`"user32.dll`")] public static extern bool SetSysColors(int cElements, int[] lpaElements, int[] lpaRgbValues); }"
    [PInvoke]::SetSysColors(1, @(1), @(0x000000))
}

# Wallpaper Update Scriptblock
$wusb={
Add-Type -TypeDefinition @"
using System;
using System.Runtime.InteropServices;
public class Wallpaper {
    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    public static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);
}
"@
# Define constants for SystemParametersInfo
$SPI_SETDESKWALLPAPER = 0x0014
$SPIF_UPDATEINIFILE = 0x01
$SPIF_SENDCHANGE = 0x02
# Apply the wallpaper by calling SystemParametersInfo
[Wallpaper]::SystemParametersInfo($SPI_SETDESKWALLPAPER, 0, $wallpaperPath, $SPIF_UPDATEINIFILE -bor $SPIF_SENDCHANGE)
}

<# REFERENCE Modify the .lnk colors then save to base64 string
$Link="$env:APPDATA\Microsoft\Windows\Start Menu\Programs\Windows PowerShell\Windows PowerShell - Copy.lnk"
$Stream=[IO.FileStream]::new($Link,'Open','Read')
$ByteContent=New-Object byte[] $Stream.Length
$Stream.Read($ByteContent, 0, $Stream.Length)
$Stream.Close()
$Base64Content = [Convert]::ToBase64String($ByteContent)
#>

$winpwshlnk="$env:APPDATA\Microsoft\Windows\Start Menu\Programs\Windows PowerShell\Windows PowerShell.lnk"

# Base64 string of modified Windows Powershell .lnk for color Invert
$invertbytes='TAAAAAEUAgAAAAAAwAAAAAAAAEbfAgAAIAAAAJuvlrfNas0Bm6+Wt81qzQGAHQKo3WrNAQDwBgAAAAAAAQAAAAAAAAAAAAAAAAAAAPEBFAAfUOBP0CDqOmkQotgIACswMJ0ZAC9DOlwAAAAAAAAAAAAAAAAAAAAAAAAAUgAxAAAAAADHQgKwMABXaW5kb3dzADwACAAEAO+++kDALMdCArAqAAAAHxAAAAAAAQAAAAAAAAAAAAAAAAAAAFcAaQBuAGQAbwB3AHMAAAAWAFYAMQAAAAAAx0JdBTAAU3lzdGVtMzIAAD4ACAAEAO+++kDBLMdCXQUqAAAAChgAAAAAAQAAAAAAAAAAAAAAAAAAAFMAeQBzAHQAZQBtADMAMgAAABgAaAAxAAAAAAD6QKBBEABXSU5ET1d+MQAAUAAIAAQA7776QKBB+kCgQSoAAACHHQAAAAABAAAAAAAAAAAAAAAAAAAAVwBpAG4AZABvAHcAcwBQAG8AdwBlAHIAUwBoAGUAbABsAAAAGABKADEAAAAAALhC660UAHYxLjAAADYACAAEAO+++kCgQbhC660qAAAAiB0AAAAAAQAAAAAAAAAAAAAAAAAAAHYAMQAuADAAAAAUAGgAMgAA8AYA+kCaGiAAcG93ZXJzaGVsbC5leGUAAEoACAAEAO+++kBXC/pAVwsqAAAA//kAAAAAAQAAAAAAAAAAAAAAAAAAAHAAbwB3AGUAcgBzAGgAZQBsAGwALgBlAHgAZQAAAB4AAABuAAAAHAAAAAEAAAAcAAAAMwAAAAAAAABtAAAAFwAAAAMAAABzLe50EAAAAE9TRGlzawBDOlxXaW5kb3dzXFN5c3RlbTMyXFdpbmRvd3NQb3dlclNoZWxsXHYxLjBccG93ZXJzaGVsbC5leGUAAC4AUABlAHIAZgBvAHIAbQBzACAAbwBiAGoAZQBjAHQALQBiAGEAcwBlAGQAIAAoAGMAbwBtAG0AYQBuAGQALQBsAGkAbgBlACkAIABmAHUAbgBjAHQAaQBvAG4AcwBRAC4ALgBcAC4ALgBcAC4ALgBcAC4ALgBcAC4ALgBcAC4ALgBcAC4ALgBcAC4ALgBcAC4ALgBcAFcAaQBuAGQAbwB3AHMAXABTAHkAcwB0AGUAbQAzADIAXABXAGkAbgBkAG8AdwBzAFAAbwB3AGUAcgBTAGgAZQBsAGwAXAB2ADEALgAwAFwAcABvAHcAZQByAHMAaABlAGwAbAAuAGUAeABlABUAJQBIAE8ATQBFAEQAUgBJAFYARQAlACUASABPAE0ARQBQAEEAVABIACUAOwAlAFMAeQBzAHQAZQBtAFIAbwBvAHQAJQBcAHMAeQBzAHQAZQBtADMAMgBcAFcAaQBuAGQAbwB3AHMAUABvAHcAZQByAFMAaABlAGwAbABcAHYAMQAuADAAXABwAG8AdwBlAHIAcwBoAGUAbABsAC4AZQB4AGUAFAMAAAEAAKAlU3lzdGVtUm9vdCVcc3lzdGVtMzJcV2luZG93c1Bvd2VyU2hlbGxcdjEuMFxwb3dlcnNoZWxsLmV4ZQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACUAUwB5AHMAdABlAG0AUgBvAG8AdAAlAFwAcwB5AHMAdABlAG0AMwAyAFwAVwBpAG4AZABvAHcAcwBQAG8AdwBlAHIAUwBoAGUAbABsAFwAdgAxAC4AMABcAHAAbwB3AGUAcgBzAGgAZQBsAGwALgBlAHgAZQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAABQAAoCUAAADVAAAAHAAAAAsAAKB3TsEa5wJdTrdELrGuUZi31QAAAGAAAAADAACgWAAAAAAAAABsZWVob2xtMTYAAAAAAAAAmpqyu7ZVLUqI6zLq13xnQNkHI1xpM+IRvnAAHMQt9AuamrK7tlUtSojrMurXfGdA2QcjXGkz4hG+cAAcxC30C8wAAAACAACgBwD1AHgAKSN4AEQAGgAaAAAAAAAAAAAACAAQADYAAACQAQAAQwBvAG4AcwBvAGwAYQBzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABkAAAAAAAAAAQAAAAEAAAABAAAAMgAAAAQAAAAAAAAA////AAA32gAToQ4AOpbdAMUPHwA6lt0AwZwAAAAAAAB2dnYAO3j/ABbGDABh1tYA50hWALQAngD58aUA8vLyALIBAAAJAACgkQAAADFTUFPiilhGvEw4Q7v8E5MmmG3OdQAAAAQAAAAAHwAAADIAAABTAC0AMQAtADUALQAyADEALQAyADEAMgA3ADUAMgAxADEAOAA0AC0AMQA2ADAANAAwADEAMgA5ADIAMAAtADEAOAA4ADcAOQAyADcANQAyADcALQAxADEAOAAwADYANAAzAAAAAAAAAOgAAAAxU1BTBwZXDJYD3kOdYeMh199QJhEAAAADAAAAAAsAAAD//wAAEQAAAAkAAAAAEwAAAP////8RAAAADAAAAAATAAAA/////xEAAAANAAAAAAsAAAAAAAAAEQAAAAEAAAAACwAAAP//AAARAAAAAgAAAAALAAAA//8AABEAAAAEAAAAAAsAAAAAAAAAEQAAAAYAAAAAAgAAAP8AAAARAAAACAAAAAATAAAAAAAAABEAAAALAAAAABMAAAD/////EQAAAAUAAAAACwAAAP//AAARAAAACgAAAAALAAAAAAAAAAAAAAAtAAAAMVNQU1UoTJ95nzlLqNDh1C3h1fMRAAAAEgAAAAATAAAAAQAAAAAAAAAAAAAAAAAAAA=='

$noinvertbytes='TAAAAAEUAgAAAAAAwAAAAAAAAEbfAgAAIAAAAJuvlrfNas0Bm6+Wt81qzQGAHQKo3WrNAQDwBgAAAAAAAQAAAAAAAAAAAAAAAAAAAPEBFAAfUOBP0CDqOmkQotgIACswMJ0ZAC9DOlwAAAAAAAAAAAAAAAAAAAAAAAAAUgAxAAAAAADHQgKwMABXaW5kb3dzADwACAAEAO+++kDALMdCArAqAAAAHxAAAAAAAQAAAAAAAAAAAAAAAAAAAFcAaQBuAGQAbwB3AHMAAAAWAFYAMQAAAAAAx0JdBTAAU3lzdGVtMzIAAD4ACAAEAO+++kDBLMdCXQUqAAAAChgAAAAAAQAAAAAAAAAAAAAAAAAAAFMAeQBzAHQAZQBtADMAMgAAABgAaAAxAAAAAAD6QKBBEABXSU5ET1d+MQAAUAAIAAQA7776QKBB+kCgQSoAAACHHQAAAAABAAAAAAAAAAAAAAAAAAAAVwBpAG4AZABvAHcAcwBQAG8AdwBlAHIAUwBoAGUAbABsAAAAGABKADEAAAAAALhC660UAHYxLjAAADYACAAEAO+++kCgQbhC660qAAAAiB0AAAAAAQAAAAAAAAAAAAAAAAAAAHYAMQAuADAAAAAUAGgAMgAA8AYA+kCaGiAAcG93ZXJzaGVsbC5leGUAAEoACAAEAO+++kBXC/pAVwsqAAAA//kAAAAAAQAAAAAAAAAAAAAAAAAAAHAAbwB3AGUAcgBzAGgAZQBsAGwALgBlAHgAZQAAAB4AAABuAAAAHAAAAAEAAAAcAAAAMwAAAAAAAABtAAAAFwAAAAMAAABzLe50EAAAAE9TRGlzawBDOlxXaW5kb3dzXFN5c3RlbTMyXFdpbmRvd3NQb3dlclNoZWxsXHYxLjBccG93ZXJzaGVsbC5leGUAAC4AUABlAHIAZgBvAHIAbQBzACAAbwBiAGoAZQBjAHQALQBiAGEAcwBlAGQAIAAoAGMAbwBtAG0AYQBuAGQALQBsAGkAbgBlACkAIABmAHUAbgBjAHQAaQBvAG4AcwBRAC4ALgBcAC4ALgBcAC4ALgBcAC4ALgBcAC4ALgBcAC4ALgBcAC4ALgBcAC4ALgBcAC4ALgBcAFcAaQBuAGQAbwB3AHMAXABTAHkAcwB0AGUAbQAzADIAXABXAGkAbgBkAG8AdwBzAFAAbwB3AGUAcgBTAGgAZQBsAGwAXAB2ADEALgAwAFwAcABvAHcAZQByAHMAaABlAGwAbAAuAGUAeABlABUAJQBIAE8ATQBFAEQAUgBJAFYARQAlACUASABPAE0ARQBQAEEAVABIACUAOwAlAFMAeQBzAHQAZQBtAFIAbwBvAHQAJQBcAHMAeQBzAHQAZQBtADMAMgBcAFcAaQBuAGQAbwB3AHMAUABvAHcAZQByAFMAaABlAGwAbABcAHYAMQAuADAAXABwAG8AdwBlAHIAcwBoAGUAbABsAC4AZQB4AGUAFAMAAAEAAKAlU3lzdGVtUm9vdCVcc3lzdGVtMzJcV2luZG93c1Bvd2VyU2hlbGxcdjEuMFxwb3dlcnNoZWxsLmV4ZQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACUAUwB5AHMAdABlAG0AUgBvAG8AdAAlAFwAcwB5AHMAdABlAG0AMwAyAFwAVwBpAG4AZABvAHcAcwBQAG8AdwBlAHIAUwBoAGUAbABsAFwAdgAxAC4AMABcAHAAbwB3AGUAcgBzAGgAZQBsAGwALgBlAHgAZQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAABQAAoCUAAADVAAAAHAAAAAsAAKB3TsEa5wJdTrdELrGuUZi31QAAAGAAAAADAACgWAAAAAAAAABsZWVob2xtMTYAAAAAAAAAmpqyu7ZVLUqI6zLq13xnQNkHI1xpM+IRvnAAHMQt9AuamrK7tlUtSojrMurXfGdA2QcjXGkz4hG+cAAcxC30C8wAAAACAACgBwD1AHgAKSN4AEQAVwCCAAAAAAAAAAAACAAQADYAAACQAQAAQwBvAG4AcwBvAGwAYQBzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABkAAAAAAAAAAQAAAAEAAAABAAAAMgAAAAQAAAAAAAAAAAAAAAA32gAToQ4AOpbdAMUPHwA6lt0AwZwAAP///wB2dnYAO3j/ABbGDABh1tYA50hWALQAngD58aUA8vLyALIBAAAJAACgkQAAADFTUFPiilhGvEw4Q7v8E5MmmG3OdQAAAAQAAAAAHwAAADIAAABTAC0AMQAtADUALQAyADEALQAyADEAMgA3ADUAMgAxADEAOAA0AC0AMQA2ADAANAAwADEAMgA5ADIAMAAtADEAOAA4ADcAOQAyADcANQAyADcALQAxADEAOAAwADYANAAzAAAAAAAAAOgAAAAxU1BTBwZXDJYD3kOdYeMh199QJhEAAAADAAAAAAsAAAD//wAAEQAAAAkAAAAAEwAAAP////8RAAAADAAAAAATAAAA/////xEAAAANAAAAAAsAAAAAAAAAEQAAAAEAAAAACwAAAP//AAARAAAAAgAAAAALAAAA//8AABEAAAAEAAAAAAsAAAAAAAAAEQAAAAYAAAAAAgAAAP8AAAARAAAACAAAAAATAAAAAAAAABEAAAALAAAAABMAAAD/////EQAAAAUAAAAACwAAAP//AAARAAAACgAAAAALAAAAAAAAAAAAAAAtAAAAMVNQU1UoTJ95nzlLqNDh1C3h1fMRAAAAEgAAAAATAAAAAQAAAAAAAAAAAAAAAAAAAA=='

# CHECKS START

$qwinstaOutput=qwinsta
# Convert array object to array string
$lines=$qwinstaOutput -split "`n"
# Skip the first line headers
$lines=$lines[1..($lines.Length -1)]
# Create array to hold session objects
$sessions=@()
foreach($line in $lines){
    # Create string array by removing extra spaces and split by space
    $columns=($line -replace '\s+', ',') -split ','
    # Create custom object for each session
    $session=[PSCustomObject]@{
        'SessionName'=$columns[0]
        'Username'=$columns[1]
        'Id'=$columns[2]
        'State'=$columns[3]
        'IdleTime'=$columns[4]
        'LogonTime'=$columns[5]
    }
    $sessions+=$session
}

$rdpchk=$null
foreach($item in $sessions){
    if($item.SessionName.Startswith('>rdp-tcp#') -and $item.State -eq 'Active'){
        write-host 'Found Active RDP session'
        $rdpchk=$item
        break
    }
}

# CHECKS START
if(
    $null -eq $rdpchk -and
    $null -ne (get-process -name magnify -ea 0) -and
    (get-itemproperty -path 'HKCU:\Software\Microsoft\ScreenMagnifier' -Name Invert).Invert -eq 1
){
    # Not RDP session, Magnify running and color inverted
    
    # Close Settings window
    $jobcssbi = start-job -scriptblock $cssb -name 'jobcssbi'
    $null=wait-job $jobcssbi
    $null=receive-job $jobcssbi
    
    # Accessibility > Contrast themes > Desert
    $invertpath="$env:systemroot\resources\Ease of Access Themes\hcwhite.theme"
    start-process -filepath $invertpath -wait

    # Change Desktop Background from Picture to Solid Color
    Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Wallpapers' -Name BackgroundType -Type DWORD -Value 1
    Set-ItemProperty -Path 'HKCU:\Control Panel\Desktop' -Name WallPaper -Value '' -type 'string'

    $jobwusbi=start-job -scriptblock $wusb -name 'jobwusbi'
    $null=wait-job $jobwusbi
    $null=receive-job $jobwusbi

    # Change Desktop Wallpaper color Invert
    $jobsisbi=start-job -scriptblock $sisb -name 'jobsisbi'
    $null=wait-job $jobsisbi
    $null=receive-job $jobsisbi

    set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize' -name AppsUseLightTheme -value 1 -type 'DWORD'
    set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize' -name SystemUsesLightTheme -value 1 -type 'DWORD'
        
    # VSCodium
    $vscpath_1="$env:APPDATA\VSCodium\User\settings.json"
    $sfinal_1=(get-content -path $vscpath_1 -raw)
    #"workbench.colorTheme": "Default High Contrast Light"
    if($sfinal_1 -notmatch '"workbench\.colorTheme":'){
        $sfinal_1=$sfinal_1.insert($sfinal_1.length-4,",`n    `"workbench.colorTheme`": `"Default High Contrast Light`"")
    }elseif($sfinal_1 -notmatch '"workbench\.colorTheme": "Default High Contrast Light"'){
        $sfinal_1=($sfinal_1 -ireplace '"workbench\.colorTheme": (.*)','"workbench.colorTheme": "Default High Contrast Light"')
    }
    set-content -path $vscpath_1 -value $sfinal_1 -nonewline

    # Windows PowerShell .lnk
    $invertbase64=[Convert]::FromBase64String($invertbytes)
    [IO.File]::WriteAllBytes($winpwshlnk,$invertbase64)

    # Close Settings window
    $jobcssbi = start-job -scriptblock $cssb -name 'jobcssbi'
    $null=wait-job $jobcssbi
    $null=receive-job $jobcssbi

    # write-host 'Magnify on and color inverted'
}else{
    # Not RDP session and NO INVERT, or RDP session
    
    # Close Settings window
    $jobcssbi = start-job -scriptblock $cssb -name 'jobcssbi'
    $null=wait-job $jobcssbi
    $null=receive-job $jobcssbi

    # Change to known good High Contrast theme
    # Night Sky
    $nitheme="$env:systemroot\resources\Ease of Access Themes\hc2.theme"
    start-process -filepath $nitheme -wait

    # Change Desktop from Picture to Solid Color
    Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Wallpapers' -Name BackgroundType -Type DWORD -Value 1
    Set-ItemProperty -Path 'HKCU:\Control Panel\Desktop' -Name WallPaper -Value '' -type 'string'

    $jobwusbn=start-job -scriptblock $wusb -name 'jobwusbn'
    $null=wait-job $jobwusbn
    $null=receive-job $jobwusbn

    $jobsnsb=start-job -scriptblock $snsb -name 'jobsnsb'
    $null=wait-job $jobsnsb
    $null=receive-job $jobsnsb

    set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize' -name AppsUseLightTheme -value 0 -type 'DWORD'
    set-itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize' -name SystemUsesLightTheme -value 0 -type 'DWORD'
    
    # VSCodium
    $vscpath_1="$env:APPDATA\VSCodium\User\settings.json"
    $sfinal_1=(get-content -path $vscpath_1 -raw)
    #"workbench.colorTheme": "Default High Contrast"
    if($sfinal_1 -notmatch '"workbench\.colorTheme":'){
        $sfinal_1=$sfinal_1.insert($sfinal_1.length-4,",`n    `"workbench.colorTheme`": `"Default High Contrast`"")
    }elseif($sfinal_1 -notmatch '"workbench\.colorTheme": "Default High Contrast"'){
        $sfinal_1=($sfinal_1 -ireplace '"workbench\.colorTheme": (.*)','"workbench.colorTheme": "Default High Contrast"')
    }
    set-content -path $vscpath_1 -value $sfinal_1 -nonewline

    # Windows PowerShell .lnk
    $noinvertbase64=[Convert]::FromBase64String($noinvertbytes)
    [IO.File]::WriteAllBytes($winpwshlnk,$noinvertbase64)

    # Close Settings window
    $jobcssbn = start-job -scriptblock $cssb -name 'jobcssbn'
    $null=wait-job $jobcssbn
    $null=receive-job $jobcssbn

    # write-host 'Magnify off,  Magnify On but color invert off, or RDP session'
}

if($null -ne (get-process taskmgr -ea 0)){
    stop-process -name taskmgr
    wait-process -name taskmgr -timeout 1
    start-process taskmgr
}

# CHECKS END
$null=stop-transcript