<# ThreatLocker silent removal
Tested on Windows 11 Windows Powershell
ThreatLocker Agent Version 9.0.0.2102
Tamper Protection Disabled https://threatlocker.kb.help/uninstalling-the-threatlocker-agent/
Endpoint will remain Online in Portal for about 15 minutes then flips to Offline and can be deleted
#>
$path=-join("$env:SYSTEMDRIVE\Windows\temp\",(Get-Date -Format "yyyyMMddHHmmss"),'-TLremove.txt')
start-transcript -path $path

stop-service -name 'HealthTLService' -force
sc.exe delete 'HealthTLService'

stop-service -name 'ThreatLockerService' -force
sc.exe delete 'ThreatLockerService'

stop-service -name 'ThreatLockerDriver' -force
sc.exe delete 'ThreatLockerDriver'

# This will delete file "$env:windir\System32\drivers\ThreatLockerDriver.sys"
$pnpchk=pnputil.exe /enum-drivers /class 'AntiVirus'
if($pnpchk.count -gt 0){
    $count=0
    foreach($item in $pnpchk){
        if($item.startswith('Published Name:')){
            $count=1
            $pubname=$item.substring(20)
        }
        if($item -eq 'Provider Name:      ThreatLockerDriver' -and $count -eq 3){
            write-host 'Found ThreatLocker driver remove'
            pnputil.exe /delete-driver $pubname
            break
        }
        $count=$count+1
    }
}

$tlpath="$env:windir\System32\drivers\etc"
$tlfr=get-childitem -path $tlpath -file -filter "ThreatLockerCoreFileRules*"
foreach($item in $tlfr){
    remove-item -path $item.fullname -force
}

# Do this at end to avoid "Cannot remove... being used by another process"
remove-item -path "$env:ProgramFiles\HealthTLService" -recurse -force
remove-item -path "$env:ProgramFiles\ThreatLocker" -recurse -force
remove-item -path "$env:ProgramData\ThreatLocker" -recurse -force
remove-item -path 'HKLM:\SOFTWARE\ThreatLocker' -force -recurse
stop-transcript