<# Workspace ONE Uninstall and Cleanup for Windows 10/11
+ Required to be run As Administrator
+ Removes "work or school" entry for "Connected to AirwatchMDM" as when one tries to "Disconnect" the entry, it is denied by policy despite user account being local Administrator.
https://kb.vmware.com/s/article/50122079?lang=en_us
https://communities.vmware.com/t5/Workspace-ONE-Discussions/How-to-remove-unenroll-from-WorkspaceOne-a-windows-laptop-that/td-p/2828788
#>
$path=-join("$env:SYSTEMDRIVE\temp\",(Get-Date -Format "yyyyMMddHHmmss"),'-ws1remove.txt')
$tpath=Test-Path -Path "$env:SYSTEMDRIVE\temp"
if($tpath -eq $false){
    New-Item -ItemType Directory -Path "$env:SYSTEMDRIVE\temp"
}

start-transcript -path $path

$appchk=@()
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*'
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*'

$awchk=@()
$awchk+=($appchk | where-object{$_.DisplayName -eq 'VMware SfdAgent'}).uninstallstring
$awchk+=($appchk | where-object{$_.DisplayName -eq 'Workspace ONE Intelligent Hub Installer'}).uninstallstring

if($awchk.count -gt 0){
    foreach($item in $awchk){
        $us=$item.replace('MsiExec.exe /X{','MsiExec.exe /qn /norestart /x {')
        $us=$item.replace('MsiExec.exe /I{','MsiExec.exe /qn /norestart /x {')
        cmd.exe /c $us
    }
}

# Event Viewer remove log folder
remove-eventlog -logname AirWatch

# Folder removals
Remove-Item -Path "$env:ProgramData\AirWatch" -Recurse -Force
Remove-Item -Path "$env:ProgramData\AirWatchMDM" -Recurse -Force
Remove-Item -Path "$env:ProgramData\VMware\SfdAgent" -Recurse -Force
Remove-Item -Path "$env:ProgramData\VMWOSQEXT" -Recurse -Force
Remove-Item -Path "$env:ProgramFiles\VMware\sfdAgent" -Recurse -Force
Remove-Item -path "$env:SYSTEMDRIVE\Program Files (x86)\AirWatch" -Recurse -Force

$rpath = Test-Path -Path "$env:SystemRoot\System32\AirWatchMDM.dll"
if($rpath -eq $true){
    cmd /c 'regsvr32.exe /u AirWatchMDM.dll'
    remove-item -path "$env:SystemRoot\System32\AirWatchMDM.dll" -Force
}

# User folder IntelligentHub removals 
$usrchk=@()
$usrchk+=(get-childitem -path "$env:SYSTEMDRIVE\Users").name
foreach($usr in $usrchk){
    $usrpath="$env:SYSTEMDRIVE\Users\$usr\AppData\Local\VMware\IntelligentHub"
    remove-item -path $usrpath -force -recurse
}

# HKCU - if computer has multiple user logins and all other things don't work, then may need to expand this into a user logon script / load logged off user hives method https://www.pdq.com/blog/modifying-the-registry-users-powershell/
remove-item 'HKCU:\Software\Microsoft\SCEP\AirWatchMDM' -recurse -force

$hkcuchk=@()
$hkcuchk+=get-item -path 'HKCU:\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppModel\PolicyCache\*'
foreach($item in $hkcuchk){
    if($item.name -like '*airwatch*'){
        $path=-join('Registry::',$item.name)
        remove-item -literalpath $path -recurse -force
    }
}

# HKLM
remove-item 'HKLM:\Software\AirWatchMDM' -recurse -force
remove-item 'HKLM:\Software\AirWatch' -recurse -force
remove-item 'HKLM:\Software\Classes\AirWatchMDM.AirWatchMDMApi' -recurse -force
remove-item 'HKLM:\Software\Classes\AirWatchMDM.AirWatchMDMApi.1' -recurse -force

$guidchk=@()
$guidchk+=get-itemproperty 'HKLM:\Software\Classes\AppID\*' | where-object {$_ -like '*airwatch*'}
if($guidchk.count -gt 0){
    foreach($guid in $guidchk){
        remove-item $guid.pspath -recurse -force
    }
}

$guidchk=@()
$guidchk+=get-itemproperty 'HKLM:\Software\Classes\CLSID\*' | where-object {$_ -like '*airwatch*'}
if($guidchk.count -gt 0){
    foreach($guid in $guidchk){
        remove-item $guid.pspath -recurse -force
    }
}

$guidchk=@()
$guidchk+=get-itemproperty 'HKLM:\Software\Classes\Interface\*' | where-object {$_ -like '*airwatch*'}
if($guidchk.count -gt 0){
    foreach($guid in $guidchk){
        remove-item $guid.pspath -recurse -force
    }
}

# Folder Names
$folderchk=@()
$folderchk+=get-itemproperty 'HKLM:\Software\Classes\*' -ea SilentlyContinue | where-object {$_ -like '*airwatch*'}
if($folderchk.count -gt 0){
    foreach($folder in $folderchk){
        remove-item $folder.pspath -recurse -force
    }
}

$guidchk=@()
$guidchk+=get-itemproperty 'HKLM:\Software\Classes\WOW6432Node\Interface\*' | where-object {$_ -like '*airwatch*'}
if($guidchk.count -gt 0){
    foreach($guid in $guidchk){
        remove-item $guid.pspath -recurse -force
    }
}

$guidchk=@()
$guidchk+=get-itemproperty 'HKLM:\Software\Microsoft\EnterpriseDesktopAppManagement\*\MSI\*' | where-object {$_ -like '*.awmdm.com*'}
if($guidchk.count -gt 0){
    foreach($guid in $guidchk){
        remove-item $guid.pspath -recurse -force
    }
}

$guidchk=@()
$guidchk+=get-itemproperty 'HKLM:\Software\Microsoft\Enrollments\*' | where-object {$_ -like '*airwatch*'}
if($guidchk.count -gt 0){
    foreach($guid in $guidchk){
        remove-item $guid.pspath -recurse -force
    }
}


<# Confirm DMClient folder or sub-folder only w/ multiple MDMs, but so far, works without doing this
remove-item 'HKLM:\Software\Microsoft\Provisioning\Diagnostics\ConfigManager\DMClient' -recurse -force
#>

remove-item 'HKLM:\Software\Microsoft\Provisioning\NodeCache\CSP\Device\AirWatchMDM' -recurse -force

$guidchk=@()
$guidchk=get-itemproperty 'HKLM:\Software\Microsoft\Provisioning\OMADM\Accounts\*'
if($guidchk.count -gt 0){
    foreach($guid in $guidchk){
        $temp=$null
        $temp=get-itemproperty (-join($guid.pspath.tostring(),'\Protected')) | where-object {$_ -like '*airwatch*'}
        if($null -ne $temp){
            remove-item $temp.psparentpath -recurse -force
        }
    }
}

remove-item 'HKLM:\Software\Microsoft\SCEP\AirWatchMDM' -recurse -force

# Looking for item properties containing C:\Program Files (x86)\AirWatch
$iproppath='HKLM:\Software\Microsoft\Windows\CurrentVersion\Installer\Folders'
$ipropchk=@()
$ipropchk+=(get-item $iproppath).Property
if($iprochk.count -gt 0){
    foreach($iprop in $ipropchk){
        if($iprop.startswith("$env:SYSTEMDRIVE\Program Files (x86)\Airwatch")){
            remove-itemproperty -path $iproppath -name $iprop -force
        }
    }
}

# SYSTEM account 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Installer\Userdata\S-1-5-18\Components\*' > 'C:\Program Files (x86)\AirWatch\'
$folderpath='HKLM:\Software\Microsoft\Windows\CurrentVersion\Installer\Userdata\S-1-5-18\Components\*'
$folderchk=@()
$folderchk+=get-itemproperty $folderpath | where-object {$_ -like '*airwatch*'}
foreach($folder in $folderchk){
    remove-item $folder.pspath -recurse -force
}

remove-itemproperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\SharedDLLs' -name 'C:\Windows\System32\AirWatchMDM.dll' -force
remove-item 'HKLM:\Software\Microsoft\EnterpriseResourceManager\Tracked' -recurse -force
remove-item 'HKLM:\Software\Microsoft\Enrollments\*' -recurse -force

stop-transcript