$tpath=-join("$env:SYSTEMROOT\Temp\",(Get-Date -Format "yyyyMMddHHmmss"),'-',$env:computername,'-su.txt')
start-transcript -path $tpath
$appchk=@()
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*'
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*'
$suo=$appchk | where-object {$_.DisplayName -eq 'Lenovo System Update'}
if($suo.count -eq 1){
    $suv=[Version]$suo.DisplayVersion
    $sm=[Version]'5.08.02.25'
    if($suv -lt $sm){
        write-host 'Remove Lenovo System Update < 5.08.02.25'
        $uninstall="`"${env:ProgramFiles(x86)}\Lenovo\System Update\unins000.exe`" /verysilent /norestart"
        cmd.exe /c $uninstall
    }else{
        write-host 'Lenovo System Update >= 5.08.02.25'
    }
}elseif($suo.count -eq 0){
    # exe file https://support.lenovo.com/us/en/solutions/ht037099#tvsu
    # Deployment Guide https://download.lenovo.com/pccbbs/mobiles_pdf/tvsu5_mst_en.pdf
    # sha256 hash https://support.lenovo.com/us/en/downloads/ds012808-lenovo-system-update-for-windows-10-7-32-bit-64-bit-desktop-notebook-workstation
    write-host 'LSU not installed, download.'
    $link='https://download.lenovo.com/pccbbs/thinkvantage_en/system_update_5.08.02.25.exe'
    $filepath="$env:systemdrive\Windows\Temp\system_update_5.08.02.25.exe"
    invoke-webrequest -uri $link -OutFile $filepath
    $sha256='0d2e68c006e6a973bb09c85b3b90d8b5fe99f400dee8778de59adbaf5b84d48f'
    $hashchk=(get-filehash -Path $filepath -Algorithm SHA256).hash
    if($sha256 -eq $hashchk){
        write-host 'LSU hash passed. install.'
        $install=-join($filepath,' /verysilent /norestart')
        cmd.exe /c $install
    }else{
        write-host 'LSU hash failed. Try again later.'
        exit 1
    }
}

new-item -path 'HKLM:\SOFTWARE\Lenovo' -Name 'System Update' -ea SilentlyContinue
new-item -path 'HKLM:\SOFTWARE\Lenovo\System Update' -Name 'Preferences' -ea SilentlyContinue
new-item -path 'HKLM:\SOFTWARE\Lenovo\System Update\Preferences' -Name 'UserSettings' -ea SilentlyContinue
new-item -path 'HKLM:\SOFTWARE\Lenovo\System Update\Preferences\UserSettings' -Name 'General' -ea SilentlyContinue
set-itemproperty 'HKLM:\SOFTWARE\Lenovo\System Update\Preferences\UserSettings\General' -name 'MetricsEnabled' -Value 'NO'

$gpchk=get-process -Name Tvsukernel -ea SilentlyContinue
$batchk=(get-ciminstance -classname Win32_Battery).BatteryStatus
if($null -ne $gpchk){
    write-host 'System Update running. Try again later.'
    exit 1
}elseif($null -eq $gpchk -and ($null -eq $batchk -or $batchk -eq 2)){
    write-host 'System Update not running. Either desktop or laptop connected to power.'
    $qwinstaOutput=qwinsta
    # Convert array object to array string
    $lines=$qwinstaOutput -split "`n"

    # Skip the first line headers
    $lines=$lines[1..($lines.Length - 1)]

    # Create array to hold session objects
    $sessions=@()
    foreach($line in $lines){
        # Create string array by removing extra spaces and split by space
        $columns=($line -replace '\s+', ',') -split ','
        # Create a custom object for each session
        $session=[PSCustomObject]@{
            'SessionName'=$columns[0]
            'Username'=$columns[1]
            'Id'=$columns[2]
            'State'=$columns[3]
            'IdleTime'=$columns[4]
            'LogonTime'=$columns[5]
        }
        $sessions+=$session
    }

    $conchk=$null
    $rdpchk=$null
    $dischk=$null
    foreach($item in $sessions){
        if(($item.SessionName.startswith('>console') -or $item.SessionName.startswith('console')) -and $item.State -eq 'Active'){
            # console Active = Locked
            # >console Active = Unlocked
            write-host 'found active console session'
            $conchk=$true
            break
        }
        if($item.SessionName.startswith('>rdp-tcp#')){
            write-host 'found active rdp session'
            $rdpchk=$true
            break
        }
        if($item.SessionName -eq '>' -and $item.Username.length -ne 0 -and $item.State -eq 'Disc'){
            write-host 'found disconnected session'
            $dischk=$true
            break
        }
    }

    if($null -eq $conchk -and $null -eq $rdpchk -and $null -eq $dischk){
        write-host 'no active RDP / console session and no disconnected session, proceed with all updates'
        set-itemproperty 'HKLM:\SOFTWARE\Lenovo\System Update\Preferences\UserSettings\General' -name 'SearchMode' -Value 'ALL'
    }else{
        write-host 'Session detected, non-disruptive updates only'
        set-itemproperty 'HKLM:\SOFTWARE\Lenovo\System Update\Preferences\UserSettings\General' -name 'SearchMode' -Value 'CRITICAL'
    }
    $cmt="`"${env:ProgramFiles(x86)}\Lenovo\System Update\tvsu.exe`" /CM"
    # All this will do is bring up the GUI to wait for user to click button "Next >"
    cmd.exe /c $cmt
}
write-host 'End of script'
stop-transcript