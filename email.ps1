<#
PWSH 7.4.0
WARNING: The command 'Send-MailMessage' is obsolete. This cmdlet does not guarantee secure connections to SMTP servers. While there is no immediate replacement available in PowerShell, we recommend you do not use Send-MailMessage at this time. See https://aka.ms/SendMailMessage for more information.
#>

# https://github.com/EvotecIT/Mailozaurr
$modchk=get-module -name mailozaurr
if($null -eq $modchk){
	$null=Install-Module -Name Mailozaurr -AllowClobber -Force
	$null=Update-Module -Name Mailozaurr
}

$username=''
$securepass=(ConvertTo-SecureString -String '' -AsPlainText -Force)
$creds=New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $username,$securepass
Send-EmailMessage -credential $creds -from '' -port 465 -smtpserver '' -subject '' -to ''
