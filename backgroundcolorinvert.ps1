$job1=start-job -scriptblock {
add-type -typedefinition @"
    using System;
    using System.Runtime.InteropServices;
    public class PInvoke {
        [DllImport(`"user32.dll`")]
        public static extern int GetSysColor(int nIndex);
    }
"@
[PInvoke]::GetSysColor(1)
}
wait-job $job1
$backgroundcolor1=Receive-Job $job1

if($backgroundcolor1 -eq 0){
    # Black background set to white
    $job2=start-job -scriptblock {
    add-type -typedefinition @"
    using System;
    using System.Runtime.InteropServices;
    public class PInvoke {
        [DllImport(`"user32.dll`")]
        public static extern bool SetSysColors(int cElements, int[] lpaElements, int[] lpaRgbValues);
    }
"@
    [PInvoke]::SetSysColors(1, @(1), @(0xFFFFFF))
}
    wait-job $job2
    Receive-Job $job2
}else{
# Set to black background
$job3 = start-job -scriptblock {
        add-type -typedefinition "using System;`n using System.Runtime.InteropServices;`n public class PInvoke { [DllImport(`"user32.dll`")] public static extern bool SetSysColors(int cElements, int[] lpaElements, int[] lpaRgbValues); }"
        [PInvoke]::SetSysColors(1, @(1), @(0x000000))
    }
    wait-job $job3
    Receive-Job $job3
}