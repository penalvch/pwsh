# Crowdstrike Tech Alert High CPU 2024-06-27 Remediation Script
$path=-join("$env:SYSTEMDRIVE\Windows\Temp\",(get-date -format "yyyMMddHHmmss"),'-',$env:computername,'-cs.txt')
start-transcript -path $path

# CS VERSION CHECK START
# Falcon Sensor for Windows 7.15 or lower
$appchk=@()
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*'
$appchk+=Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*'
$cschk=@()
$cschk+=($appchk | where-object {$_.DisplayName -eq 'CrowdStrike Windows Sensor'})
if($cschk.count -eq 1 -and ($csver.major -gt 7 -or ($csver.major -eq 7 -and $csver.minor -gt 15))){
        write-host 'CS version later than 7.15'
        exit 0
}elseif($cschk.count -eq 1 -and ($csver.major -lt 7 -or ($csver.major -eq 7 -and $csver.minor -le 15))){
    write-host 'CS version <= 7.15'
}elseif($cschk.count -eq 0){
    write-host 'CS not installed. Remediate.'
    exit 1
}elseif($cschk.count -gt 1){
    write-host 'More than one CS installed. Remediate.'
    exit 1
}
# CS VERSION CHECK END

# REBOOT CHECK START
$eventlog='System'
# Event ID 6005 reboot of any kind (startup from shutdown or restart, clean or unclean)
$eventIDReboot=6005
# 1515 UTC 2024-06-27
$dateUtc=[DateTime]::SpecifyKind([DateTime]::new('2024','06','27','15','15','0','0'),[DateTimeKind]::Utc)
$tz=[System.TimeZoneInfo]::Local
$datelocal=[System.TimeZoneInfo]::ConvertTime($dateUtc,$tz)

# Query Event logs for reboot events
$rebootEvents=get-winevent -logname $eventlog -FilterXPath "<QueryList><Query Path='$eventlog'><Select Path='$eventlog'>*[System[(EventID=$eventIDREboot)]]</Select></Query></QueryList>" -MaxEvents 1 -ea SilentlyContinue
if($null -ne $rebootEvents){
    $rebootdate=$rebootEvents.TimeCreated
    if($rebootdate -ge $datelocal){
        write-host 'Most recent reboot occurred since event time.'
        exit 0
    }
}
# REBOOT CHECK END

# INTEL CPU CHECK START
$cpu=get-ciminstance -class win32_processor
if($cpu.name.contains('Intel') -eq $false){
    write-host 'CPU is not Intel'
    exit 0
}else{
    write-host 'CPU is Intel'
}
# INTEL CPU CHECK END

# INTEL GENERATION CHECK START
# https://www.intel.com/content/www/us/en/support/articles/000032203/processors/intel-core-processors.html
if(
    !$cpu.Name.contains('13th Gen') -and
    !$cpu.Name.contains('12th Gen') -and
    !$cpu.Name.contains('11th Gen')
){
    write-host 'Intel CPU either older than 6th Gen, or not in above table'
    exit 0
}
# INTEL GENERATION CHECK END

# ONLINE CHECK START
$startutc=[DateTime]::SpecifyKind([DateTime]::new('2024','06','27','12','27','0','0'),[DateTimeKind]::Utc)
$startlocal=[System.TimeZoneInfo]::ConvertTime($startutc,$tz)
$endutc=[DateTime]::SpecifyKind([DateTime]::new('2024','06','27','14','43','0','0'),[DateTimeKind]::Utc)
$endlocal=[System.TimeZoneInfo]::ConvertTime($endutc,$tz)
$events=get-winevent -ea SilentlyContinue -MaxEvents 1 -filterhashtable @{
    LogName='*'
    StartTime=$startlocal
    endTime=$endlocal
}
if($events.count -gt 0){
    $qwinstaOutput=qwinsta
    # Convert array object to array string
    $lines=$qwinstaOutput -split "`n"
    # Skip the first line headers
    $lines=$lines[1..($lines.Length -1)]
    # Create array to hold session objects
    $sessions=@()
    foreach($line in $lines){
        # Create string array by removing extra spaces and split by space
        $columns=($line -replace '\s+', ',') -split ','
        # Create custom object for each session
        $session=[PSCustomObject]@{
            'SessionName'=$columns[0]
            'Username'=$columns[1]
            'Id'=$columns[2]
            'State'=$columns[3]
            'IdleTime'=$columns[4]
            'LogonTime'=$columns[5]
        }
        $sessions+=$session
    }
    $conchk=$null
    $rdpchk=$null
    $dischk=$null
    # Given script scope Windows 10/11 endpoints, only looking for first active Console / RDP session
    foreach($item in $sessions){
        if(($item.SessionName.startswith('>console') -or $item.SessionName.startswith('console')) -and $item.State -eq 'Active'){
            # console Active = Locked
            # >console Active = Unlocked
            write-host 'Found active console session'
            $conchk=$item
            break
        }
        if($item.SessionName.Startswith('>rdp-tcp#') -and $item.State -eq 'Active'){
            write-host 'Found Active RDP session'
            $rdpchk=$item
            break
        }
        if($item.SessionName -eq '>' -and $item.Username.length -ne 0 -and $item.State -eq 'Disc'){
            write-host 'Found disconnected session'
            $dischk=$item
            break
        }
    }

    $text='Need to perform one-time restart in 5 minutes. Please save all work now.'
    if($null -eq $conchk -and $null -eq $rdpchk -and $null -eq $dischk){
        write-host 'No active console, active RDP session, or disconnected session. Restart.'
        # Need -force when running as SYSTEM to avoid 'System.InvalidOperationException: Failed to restart the computer HOSTNAME with the following error message: The system shutdown cannot be initiated becaused there are other users logged on to the computer.'
        restart-computer -force
    }elseif($null -ne $conchk -and $null -eq $rdpchk -and $null -eq $dischk){
        write-host 'Active console session. Notify and restart.'
        $uname=$conchk.Username
        msg $uname /W $text
        start-sleep -seconds 300
        restart-computer -force
    }elseif($null -eq $conchk -and $null -ne $rdpchk -and $null -eq $dischk){
        write-host 'Active RDP session. Notify and restart.'
        $uname=$rdpchk.Username
        msg $uname /W $text
        start-sleep -seconds 300
        restart-computer -force
    }elseif($null -eq $conchk -and $null -eq $rdpchk -and $null -ne $dischk){
        write-host 'Disconnected session. Try again later.'
        exit 1
    }
}else{
    write-host 'No events logged during time band. Presume machine was off.'
    exit 0
}
# ONLINE CHECK END
stop-transcript