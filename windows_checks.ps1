# Checks for various issues in Windows via Powershell

# Resources

# CPU %
$cpuperc=(get-counter '\Processor(_Total)\% Processor Time').CounterSamples.CookedValue
    if($cpuperc -gt 90){
        # Action
    }

# Memory %
$memperc=(get-counter '\Memory\% Committed Bytes In Use').CounterSamples.CookedValue
    if($memperc -gt 90){
        # Action
    }

# Drive %
$drives=get-psdrive -psprovider filesystem
$drives | foreach-object {
    if($null -ne $_.Used -and $null -ne $_.free -and $_.free -ne 0){
        $usedspace=($_.Used/$_.Free)*100
        if($usedspace -gt 90){
            # Action
        }
    }
}

# DHCP Service check Running and Startup Type Automatic
$dhcp=get-service -name dhcp
if($dhcp.Status -ne 'Running'){
    $null=start-service -name dhcp
}
$serviceStartup=(Get-ciminstance -Class Win32_Service -Filter "Name='dhcp'").StartMode
if($servicestartup -ne 'Auto'){
    $null=set-service -name dhcp -startuptype Automatic
}

# Windows updates pending
$updateregs=@(
'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update\RebootRequired',
'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Component Based Servicing\RebootPending'
)

foreach($path in $updateregs){
    if(test-path $path){
        # Action
        break
    }
}

# BitLocker enabled on all drives
$blkchk=get-bitlockervolume
foreach($vol in $blchk){
    if($vol.VolumeStatus -ne 'FullyEncrypted'){
        # Action
    }
}
